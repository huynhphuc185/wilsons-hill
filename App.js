/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Dimensions, StyleSheet, YellowBox } from "react-native";
import AppContainer from "screens";
import { NavigationService } from "./src/services";

export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
