import { NavigationActions, StackActions } from "react-navigation";
import Global from "../constants/Global";
let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}
function push(routeName, params) {
  _navigator.dispatch(
    StackActions.push({
      routeName,
      params
    })
  );
}
function pop() {
  _navigator.dispatch(StackActions.pop());
}
function dispatch(action) {
  _navigator.dispatch(action);
}

function reset(stackName) {
  const resetAction = StackActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({ routeName: stackName })]
  });
  _navigator.dispatch(resetAction);
}
function resetBackHome(stackName) {
  Global.previousRoute = "home";
  const resetAction = StackActions.reset({
    index: 1,
    key: null,
    actions: [
      NavigationActions.navigate({ routeName: "mainStack" }),
      NavigationActions.navigate({ routeName: stackName })
    ]
  });
  _navigator.dispatch(resetAction);
}

export default {
  navigate,
  push,
  pop,
  dispatch,
  reset,
  resetBackHome,
  setTopLevelNavigator
};
