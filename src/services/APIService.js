import AsyncStorage from "@react-native-community/async-storage";
import DeviceInfo from "react-native-device-info";
import { Platform } from "react-native";
import { NavigationService, DataService } from ".";
import AlertScreen from "../screens/common/AlertScreen";
import { KeyStorage, Event } from "../constants";
import EventEmitter from "react-native-eventemitter";
import { strings } from "../../assets";

class APIService {
  constructor() {
    if (!APIService.instance) {
      APIService.instance = this;
    }
    return APIService.instance;
  }

  setBaseURL = url => {
    this.BASE_URL = url;
  };

  BASE_URL = "";

  queryParams(params) {
    return Object.keys(params)
      .map(k => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
  }

  get = async (func, params = {}) => {
    var query = this.queryParams(params);
    var url = this.BASE_URL + func;

    if (query) {
      url += "?" + query;
    }
    var token = await AsyncStorage.getItem(KeyStorage.TOKEN);
    console.log("GET: " + JSON.stringify(url));
    console.log("PARAMS: " + JSON.stringify(params));
    return new Request(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        token: token
      }
    });
  };

  post = async (func, params = {}) => {
    var url = this.BASE_URL + func;
    console.log("POST: " + params);
    var token = await AsyncStorage.getItem(KeyStorage.TOKEN);
    console.log("POST: " + JSON.stringify(url));
    console.log("PARAMS: " + JSON.stringify(params));
    return new Request(url, {
      method: "POST",
      body: JSON.stringify(params),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        token: token
      }
    });
  };

  multipart = async (func, body) => {
    try {
      var url = this.BASE_URL + func;
      let formData = new FormData(); //formdata object

      //   formData.append("name", "ABC"); //append the values with key, value pair
      //   formData.append("age", 20);
      if (body) {
        Object.keys(body).forEach(item => {
          var obj = body[item];
          if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
              formData.append(item, obj[i]);
            }
          } else {
            formData.append(item, body[item]);
          }
        });
      }
      var token = await AsyncStorage.getItem(KeyStorage.TOKEN);
      console.log(token);
      var request = new Request(url, {
        method: "POST",
        body: formData,
        headers: {
          Authorization: "Bearer " + token
        }
      });
      return request;
    } catch (error) {
      //this.handleError(error.code, error.message);
      throw error;
    }
  };

  execute = async (request, isShowProgress = true, isShowAlert = true) => {
    try {
      isShowProgress ? NavigationService.navigate("IProgress") : null;
      const responseJson = await fetch(request).then(response => {
        console.log(response);

        return response.json();
      });
      isShowProgress ? NavigationService.pop() : null;

      if (responseJson.code == 1) {
        if (responseJson.data) {
          return responseJson.data;
        }
        return responseJson;
      }
      if (isShowAlert === true) {
        var message = responseJson.error_message;
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance(message, responseJson.code)
        );
      }
      // request.url
      throw responseJson;
    } catch (error) {
      throw error;
    }
  };

  getDefaultParams = async () => {
    var params = new Object();
    params.deviceid = await AsyncStorage.getItem(KeyStorage.DEVICEID);
    params.token = await AsyncStorage.getItem(KeyStorage.TOKEN);
    params.os = Platform.OS == "ios" ? "iOS" : "android";

    return params;
  };

  login = async (username, password) => {
    try {
      var params = new Object();
      params.username = username;
      params.password = password;
      params.os = Platform.OS;
      params.deviceid = await DeviceInfo.getUniqueId();
      let request = await this.post("/userservice/user/login", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {}
  };

  register = async (
    company_name,
    country,
    billing_address,
    person_contact,
    occupation,
    email,
    phone
  ) => {
    var params = new Object();
    params.company_name = company_name;
    params.country = country;
    params.billing_address = billing_address;
    params.person_contact = person_contact;
    params.occupation = occupation;
    params.email = email;
    params.phone = phone;
    params.deviceid = await DeviceInfo.getUniqueId(); // Using real device id when register
    params.os = Platform.OS == "ios" ? "iOS" : "android";
    let request = await this.post("/userservice/user/register", params);
    let data = await this.execute(request);
    return data;
  };

  updateProfile = async (
    company_name,
    country,
    billing_address,
    person_contact,
    occupation,
    email_contact,
    phone
  ) => {
    var params = new Object();
    params.company_name = company_name;
    params.country = country;
    params.billing_address = billing_address;
    params.person_contact = person_contact;
    params.occupation = occupation;
    params.email_contact = email_contact;
    params.phone = phone;
    params.deviceid = await DeviceInfo.getUniqueId();
    params.os = Platform.OS == "ios" ? "iOS" : "android";
    let request = await this.post("/userservice/user/update_profile", params);
    let data = await this.execute(request);
    return data;
  };

  changePassword = async (current_password, new_password, retype_password) => {
    var params = await this.getDefaultParams();
    params.current_password = current_password;
    params.new_password = new_password;
    params.retype_password = retype_password;
    let request = await this.post("/userservice/user/change_password", params);
    let data = await this.execute(request);
    return data;
  };

  getListProductCollection = async (collection_id, sort, page) => {
    try {
      var params = await this.getDefaultParams();
      params.collection_id = collection_id;
      params.sort = sort;
      params.page = page;
      let request = await this.get(
        "/productservice/collection/get_list_product",
        params
      );
      let data = await this.execute(request, false);
      return data;
    } catch (error) {}
  };

  getListColletion = async isShowProgress => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/main/get_list_collection",
      params
    );
    let data = await this.execute(request, isShowProgress);
    return data;
  };
  getListCategory = async isShowProgress => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/main/get_list_category",
      params
    );
    let data = await this.execute(request, isShowProgress);
    return data;
  };

  getMainBestSeller = async isShowProgress => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/main/get_main_best_seller",
      params
    );

    let data = await this.execute(request, isShowProgress);
    return data;
  };
  getListBestSeller = async () => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/main/get_list_best_seller",
      params
    );

    let data = await this.execute(request);
    return data;
  };
  getListSort = async isShowProgress => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/sort/get_list_sort_product_type",
      params
    );
    let data = await this.execute(request, isShowProgress);
    return data;
  };
  getListSortDetailCollection = async isShowProgress => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/sort/get_list_sort_collection_type",
      params
    );
    let data = await this.execute(request, isShowProgress);
    return data;
  };

  getListProductByCate = async (cate_id, sort, page, isShowProgress) => {
    var params = await this.getDefaultParams();
    params.cate_id = cate_id;
    params.sort = sort;
    params.page = page;
    let request = await this.get(
      "/productservice/main/get_list_product_by_cate_filter",
      params
    );
    let data = await this.execute(request, isShowProgress);
    return data;
  };
  getDetailProduct = async product_id => {
    var params = await this.getDefaultParams();
    params.product_id = product_id;
    let request = await this.get("/productservice/product/get_detail", params);
    let data = await this.execute(request);
    return data;
  };

  getListProductRelate = async product_id => {
    var params = await this.getDefaultParams();
    params.product_id = product_id;
    let request = await this.get(
      "/productservice/product/get_list_product_related",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };

  addToCart = async (product_id, number) => {
    var params = await this.getDefaultParams();
    params.product_id = product_id;
    params.number = number;
    let request = await this.post("/orderservice/cart/add_product", params);
    let data = await this.execute(request);
    return data;
  };

  addManyToCart = async products => {
    var params = await this.getDefaultParams();
    params.products = products;
    let request = await this.post("/orderservice/cart/add_products", params);
    let data = await this.execute(request);
    return data;
  };

  addCollectionToCart = async collection_ids => {
    var params = await this.getDefaultParams();
    params.collection_ids = collection_ids;
    let request = await this.post("/orderservice/cart/add_collection", params);
    let data = await this.execute(request);
    return data;
  };

  getCart = async isPro => {
    var params = await this.getDefaultParams();
    let request = await this.get("/orderservice/cart/get_info", params);
    let data = await this.execute(request, isPro);
    return data;
  };

  getTokenDefaut = async () => {
    var params = new Object();
    params.deviceid = await DeviceInfo.getUniqueId();
    params.os = Platform.OS == "ios" ? "iOS" : "android";
    let request = await this.post("/userservice/user/login_session", params);
    let data = await this.execute(request, false);
    return data;
  };

  getCartNumber = async () => {
    var params = await this.getDefaultParams();
    let request = await this.get("/orderservice/cart/get_cart_number", params);
    let data = await this.execute(request, false, false);
    return data;
  };
  updateProductCart = async (product_id, number) => {
    var params = await this.getDefaultParams();
    params.product_id = product_id;
    params.number = number;
    let request = await this.post("/orderservice/cart/update_product", params);
    let data = await this.execute(request, true);
    return data;
  };
  confirmCart = async (address_id, forwarder, port_of_discharge, note) => {
    var params = await this.getDefaultParams();
    params.address_id = address_id;
    params.forwarder = forwarder;
    params.port_of_discharge = port_of_discharge;
    params.note = note;
    let request = await this.post("/orderservice/order/confirm", params);
    let data = await this.execute(request, false);
    return data;
  };
  productLike = async listID => {
    var params = await this.getDefaultParams();
    params.product_id = listID;
    // params.like = like;
    let request = await this.post("/productservice/product/like", params);
    let data = await this.execute(request, false);
    return data;
  };
  getOrderHistory = async (page, isProgress = true) => {
    var params = await this.getDefaultParams();
    params.page = page;
    let request = await this.get("/orderservice/order/get_list_order", params);
    let data = await this.execute(request, isProgress);
    return data;
  };
  ß;
  getAllCollection = async (page, isProgress = true) => {
    var params = await this.getDefaultParams();
    params.page = page;
    let request = await this.get("/productservice/collection/get_all", params);
    let data = await this.execute(request, isProgress);
    return data;
  };

  getProfile = async (isProgress = true) => {
    var params = await this.getDefaultParams();
    let request = await this.get("/userservice/user/get_profile", params);
    let data = await this.execute(request, isProgress);
    return data;
  };

  getDetailCollection = async collection_id => {
    var params = await this.getDefaultParams();
    params.collection_id = collection_id;
    let request = await this.get(
      "/productservice/collection/get_detail",
      params
    );
    let data = await this.execute(request);
    return data;
  };

  updateFirebaseToken = async fbToken => {
    // var params = await this.getDefaultParams()
    // params.fbToken = fbToken
    // let request = await this.post('/productservice/product/like', params)
    // let data = await this.execute(request)
    // return data
  };

  getTopSearchCollection = async () => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/collection/get_top_search",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };

  getTopSearchProduct = async () => {
    var params = await this.getDefaultParams();
    let request = await this.get(
      "/productservice/product/get_top_search",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };
  getSearchSuggestion = async keyword => {
    var params = await this.getDefaultParams();
    params.key = keyword;
    let request = await this.get("/productservice/search/suggestion", params);
    let data = await this.execute(request, false);
    return data;
  };
  getSearchProductWithKey = async keyword => {
    var params = await this.getDefaultParams();
    params.key = keyword;
    let request = await this.get(
      "/productservice/search/search_product",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };

  getSearchCategoryWithKey = async keyword => {
    var params = await this.getDefaultParams();
    params.key = keyword;
    let request = await this.get("/productservice/search/search_cate", params);
    let data = await this.execute(request, false);
    return data;
  };
  getSearchCollectionWithKey = async keyword => {
    var params = await this.getDefaultParams();
    params.key = keyword;
    let request = await this.get(
      "/productservice/search/search_collection",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };
  getOrderDetail = async id => {
    var params = await this.getDefaultParams();
    params.id = id;
    let request = await this.get("/orderservice/order/detail", params);
    let data = await this.execute(request, false);
    return data;
  };

  getMyCatalogue = async () => {
    var params = await this.getDefaultParams();
    let request = await this.get("/productservice/product/catalogue", params);
    let data = await this.execute(request, false);
    return data;
  };

  getPreviousOrders = async (page = 1) => {
    var params = await this.getDefaultParams();
    params.page = page;
    let request = await this.get("/orderservice/order/catalogue", params);
    let data = await this.execute(request, false);
    return data;
  };
  getSaveCollections = async (page = 1) => {
    var params = await this.getDefaultParams();
    params.page = page;
    let request = await this.get(
      "/productservice/collection/get_list_collection_save",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };
  getSaveItems = async (page = 1) => {
    var params = await this.getDefaultParams();
    params.page = page;
    let request = await this.get(
      "/productservice/product/get_list_product_like",
      params
    );
    let data = await this.execute(request, false);
    return data;
  };
  reOrder = async id => {
    var params = await this.getDefaultParams();
    params.id = id;
    let request = await this.get("/orderservice/order/re_order", params);
    let data = await this.execute(request);
    return data;
  };

  saveCollection = async (collection_id, save) => {
    var params = await this.getDefaultParams();
    params.collection_id = collection_id;
    params.save = save;
    let request = await this.post("/productservice/collection/save", params);
    let data = await this.execute(request, false);
    console.log(data);

    return data;
  };
}
const instance = new APIService();
//Object.freeze(instance);
export default instance;
