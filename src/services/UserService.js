import AsyncStorage from "@react-native-community/async-storage";
import DeviceInfo from "react-native-device-info";
import { KeyStorage } from "../constants";
import { Platform } from "react-native";
import { APIService } from ".";
import Global from "../constants/Global";
import EventEmitter from "react-native-eventemitter";
class UserService {
  isLogin = async () => {
    try {
      const isLogin = await AsyncStorage.getItem(KeyStorage.ISLOGIN);
      if (isLogin === "true") {
        return true;
      }
      return false;
    } catch (err) {
      console.log(err);
    }
  };

  login = async user => {
    const deviceid = await DeviceInfo.getUniqueId();

    await AsyncStorage.multiSet([
      [KeyStorage.USERNAME, user.userName],
      [KeyStorage.TOKEN, user.token],
      [KeyStorage.DEVICEID, deviceid],
      [KeyStorage.OS, Platform.OS == "ios" ? "iOS" : "android"]
    ]);
  };

  reset = async () => {
    try {
      const data = await APIService.getTokenDefaut();
      await AsyncStorage.setItem(KeyStorage.TOKEN, data.token);
      await AsyncStorage.setItem(KeyStorage.USERNAME, "");
      await AsyncStorage.setItem(KeyStorage.ISLOGIN, "false");
      return true;
    } catch (error) {}
    return false;
  };

  save = async token => {
    try {
      await AsyncStorage.setItem(KeyStorage.TOKEN, token);
      return true;
    } catch (error) {}
    return false;
  };
}

const instance = new UserService();
Object.freeze(instance);
export default instance;
