import React from "react";
import { Platform, Alert } from "react-native";

import Permissions from "react-native-permissions";
import ImageCropPicker from "react-native-image-crop-picker";
import OpenAppSettings from "react-native-app-settings";
const requestPermission = async () => {
  var result;
  result = await Permissions.request("photo");
  if (result == "restricted") {
    Alert.alert(
      "Can we access your photos?",
      "We need access so you can upload image to server",
      [
        {
          text: "No way",
          onPress: () => console.log("Permission denied"),
          style: "cancel"
        },

        {
          text: "Open Settings",
          onPress: OpenAppSettings.open
        }
      ]
    );
    return;
  }

  Permissions.request("camera").then(response => {});
  ImageCropPicker.openCamera({
    width: 512,
    height: 512,
    compressImageMaxWidth: 512,
    compressImageMaxHeight: 512,
    cropping: false,
    compressImageQuality: 0.5
  }).then(image => {
    alert(JSON.stringify(image));
    // console.log(image);
    // this.state.images[index] = image;
    // this.setState({})
  });
  return result;
};

const openGallery = async callback => {
  if (Platform.OS === "android") {
    var result = await Permissions.request("photo");
    if (result == "restricted") {
      Alert.alert(
        "Can we access your photos?",
        "We need access so you can upload image to server",
        [
          {
            text: "No way",
            onPress: () => console.log("Permission denied"),
            style: "cancel"
          },

          {
            text: "Open Settings",
            onPress: OpenAppSettings.open
          }
        ]
      );
      return;
    }
  }

  ImageCropPicker.openPicker({
    width: 512,
    height: 512,
    compressImageMaxWidth: 512,
    compressImageMaxHeight: 512,
    cropping: false,
    compressImageQuality: 0.5,
    mediaType: "photo",
    includeBase64: true
  }).then(data => {
    if (!callback) {
      return;
    }
    callback(data);
  });
};

const openCamera = async callback => {
  var result = await Permissions.request("camera");
  if (result == "restricted") {
    Alert.alert(
      "Can we access your photos?",
      "We need access so you can upload image to server",
      [
        {
          text: "No way",
          onPress: () => console.log("Permission denied"),
          style: "cancel"
        },

        {
          text: "Open Settings",
          onPress: OpenAppSettings.open
        }
      ]
    );
    return;
  }
  ImageCropPicker.openCamera({
    width: 512,
    height: 512,
    compressImageMaxWidth: 512,
    compressImageMaxHeight: 512,
    cropping: false,
    compressImageQuality: 0.5,
    mediaType: "photo",
    includeBase64: true
  }).then(data => {
    if (!callback) {
      return;
    }
    callback(data);
  });
};

export { requestPermission, openGallery, openCamera };
