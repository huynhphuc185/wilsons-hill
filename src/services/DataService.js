import { APIService } from ".";

class DataService {
  constructor() {
    if (!DataService.instance) {
      DataService.instance = this;
    }
    return DataService.instance;
  }
  cart = [];
  _getNumberCart = async () => {
    try {
      //   if (DataService.country) {
      //     return DataService.country;
      //   }
      const data = await APIService._getNumberCart();
      this.cart = data.product_ids;
      return this.cart;
    } catch (error) {
      //throw error;
    }
    return null;
  };
}

const instance = new DataService();
//Object.freeze(instance);
export default instance;
