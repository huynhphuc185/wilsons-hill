import firebase from 'react-native-firebase'
import { APIService } from '.'

class FantasyPushNotificationFirebase {
	constructor() {
		this.prepareFirebase()
	}

	registerFirebase(
		{ handleDisplayNotify },
		{ handleOpenedNotify },
		{ handleDataNotify },
	) {
		this.notificationDisplayedListener(handleDisplayNotify)
		this.notificationListener()
		this.notificationOpenedListener(handleOpenedNotify)
		this.messageListener(handleDataNotify)
		this.notificationWhenCloseApp(handleOpenedNotify)
	}

	async checkPermission() {
		try {
			firebase.messaging().subscribeToTopic('dev_all')
			const enabled = await firebase.messaging().hasPermission()
			if (enabled) {
				this.getToken()
			} else {
				this.requestPermission()
			}
		} catch (error) {
			console.log(err)
		}
	}
	async getToken() {
		try {
			let fcmToken = await firebase.messaging().getToken()
			console.log(fcmToken)
			if (fcmToken) {
				APIService.updateFirebaseToken(fcmToken)
					.then(() => {
						console.log('send firebase token success')
					})
					.catch(err => {
						console.log('send firebase token failed')
						console.log(err)
					})
			}
		} catch (err) {
			console.log(err)
		}
	}
	//
	async requestPermission() {
		try {
			await firebase.messaging().requestPermission()
			// User has authorised
			await this.getToken()
			console.log('permission success')
		} catch (error) {
			// User has rejected permissions
			console.log('permission rejected')
		}
	}
	removeAllNotification() {
		firebase.notifications().removeAllDeliveredNotifications()
		//  firebase.notifications().setBadge(0);
		firebase.notifications().cancelAllNotifications()
	}
	notificationWhenCloseApp = async handleOpenedNotify => {
		try {
			let notification = await firebase
				.notifications()
				.getInitialNotification()

			if (notification) {
				handleOpenedNotify(notification)
			}
		} catch (err) {
			console.log(err)
		}
	}
	notificationDisplayedListener = handleDisplayNotify => {
		firebase.notifications().onNotificationDisplayed(notification => {
			// When user in app, and searching for love, emit event to open matching popup
			handleDisplayNotify(notification)
		})
	}
	notificationListener = () => {
		firebase.notifications().onNotification(notification => {
			// console.log('on notification');
			// Process your notification as required
			notification.android.setChannelId('test-channel')
			firebase.notifications().displayNotification(notification)
		})
	}
	notificationOpenedListener = handleOpenedNotify => {
		firebase.notifications().onNotificationOpened(notificationOpen => {
			handleOpenedNotify(notificationOpen)
		})
	}
	messageListener = handleDataNotify => {
		firebase.messaging().onMessage(notification => {
			handleDataNotify(notification)
		})
	}
	async prepareFirebase() {
		try {
			await this.checkPermission()
			const channel = new firebase.notifications.Android.Channel(
				'test-channel',
				'Test Channel',
				firebase.notifications.Android.Importance.Max,
			).setDescription('My apps test channel')
			// Create the channel

			await firebase.notifications().android.createChannel(channel)
		} catch (error) {
			console.log('====================')
			console.log(error)
		}
	}
}

export default FantasyPushNotificationFirebase
