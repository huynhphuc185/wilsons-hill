import { Linking } from "react-native";
Date.prototype.customFormat = function(formatString) {
  var YYYY,
    YY,
    MMMM,
    MMM,
    MM,
    M,
    DDDD,
    DDD,
    DD,
    D,
    hhhh,
    hhh,
    hh,
    h,
    mm,
    m,
    ss,
    s,
    ampm,
    AMPM,
    dMod,
    th;
  YY = ((YYYY = this.getFullYear()) + "").slice(-2);
  MM = (M = this.getMonth() + 1) < 10 ? "0" + M : M;
  DD = (D = this.getDate()) < 10 ? "0" + D : D;
  th =
    D >= 10 && D <= 20
      ? "th"
      : (dMod = D % 10) == 1
      ? "st"
      : dMod == 2
      ? "nd"
      : dMod == 3
      ? "rd"
      : "th";
  formatString = formatString
    .replace("#YYYY#", YYYY)
    .replace("#YY#", YY)
    .replace("#MMMM#", MMMM)
    .replace("#MM#", MM)
    .replace("#M#", M)
    .replace("#DDDD#", DDDD)
    .replace("#DD#", DD)
    .replace("#D#", D)
    .replace("#th#", th);
  h = hhh = this.getHours();
  //if (h == 0) h = 24;
  //if (h > 12) h -= 12;
  hh = h < 10 ? "0" + h : h;
  hhhh = hhh < 10 ? "0" + hhh : hhh;
  //AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
  AMPM = "";
  mm = (m = this.getMinutes()) < 10 ? "0" + m : m;
  ss = (s = this.getSeconds()) < 10 ? "0" + s : s;
  return formatString
    .replace("#hhhh#", hhhh)
    .replace("#hhh#", hhh)
    .replace("#hh#", hh)
    .replace("#h#", h)
    .replace("#mm#", mm)
    .replace("#m#", m)
    .replace("#ss#", ss)
    .replace("#s#", s);
  // .replace('#ampm#', ampm)
  // .replace('#AMPM#', AMPM);
};

const validateEmail = email => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const priceFormat = price => {
  // if (price == undefined) {
  //   return "";
  // } else {
  //   let str = typeof price == "string" ? price : price.toString();
  //   return str.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  // }
  if (price == undefined) {
    return "$";
  } else {
    const priceWith2Decimal = price.toFixed(2);
    const str = priceWith2Decimal.toString();
    return "$ " + str.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
};

const unPriceFormat = price => {
  try {
    if (!price) {
      return 0;
    }
    var data = price.replace(/\,/g, "");
    return data;
  } catch (error) {}
  return price;
};

const call = phoneNumber => {
  const urlToOpen = `tel:${phoneNumber}`;
  Linking.openURL(urlToOpen);
};

const dateFormat = (miliseconds, isHour) => {
  if (miliseconds == undefined) {
    return "";
  }
  let date = new Date(miliseconds);
  if (isHour) {
    return date.customFormat("#hh#:#mm#");
  }
  return date.customFormat("#DD# - #MM# - #YYYY#");
};
export { validateEmail, priceFormat, unPriceFormat, dateFormat, call };
