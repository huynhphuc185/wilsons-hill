export default class ConfigServer {
    constructor() {}
    static configObject = {};
  
    static setConfig(obj) {
      this.configObject = obj;
    }
    static getConfig() {
      return this.configObject;
    }
  }