
import AsyncStorage from '@react-native-community/async-storage'
export default class EnvService {
  static ENV = {
    REAL: 'real',
    DEV: 'dev',
    TEST: 'test',
  };

  async getCurrentEnv() {
    try {
      const env = await AsyncStorage.getItem('env');
      
      if (!env) return EnvService.ENV.REAL;
      return env;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async setCurrentEnv(env) {
    try {
      
      await AsyncStorage.setItem('env', env);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  
}




