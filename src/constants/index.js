import KeyStorage from "./KeyStorage";
import Event from "./Event";
import { Platform } from "react-native";
const VERSION_ANDROID = "1.0.0";
const VERSION_IOS = "1.0.0";
const VERSION = Platform.OS === "ios" ? VERSION_IOS : VERSION_ANDROID;
// var mili = new Date().getTime();
const CONFIG_ANDROID = "http://wilsonhill.ipicorp.co/wilson_hill/config/android/info.json";
const CONFIG_IOS = "http://wilsonhill.ipicorp.co/wilson_hill/config/ios/info.json";

const CONFIG = Platform.OS === "ios" ? CONFIG_IOS : CONFIG_ANDROID;

const API_KEY = "";

var BASE_URL = "";

export { KeyStorage, Event, API_KEY, CONFIG, VERSION, BASE_URL };




