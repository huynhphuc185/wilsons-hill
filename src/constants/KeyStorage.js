const KeyStorage = {
  USERNAME: "USERNAME",
  TOKEN: "TOKEN",
  DEVICEID: "DEVICEID",
  ISLOGIN: "ISLOGIN",
  OS: "OS",
  SERVER: "SERVER",
  LANGUAGE: "LANGUAGE"
};
export default KeyStorage;
