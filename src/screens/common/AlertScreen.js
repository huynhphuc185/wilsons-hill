import React, { Component } from "react";
import { Text, View } from "react-native";
import { IView, IText, ITouch } from "../../components";
import { colors, strings } from "../../../assets";
import { NavigationService, UserService, APIService } from "../../services";
import AsyncStorage from "@react-native-community/async-storage";

class AlertScreen extends Component {
  static newInstance = (message, code, callBack,title='Alert') => ({
    title:title,
    message: message,
    code: code,
    callBack: callBack
   
  });
  constructor(props) {
    super(props);
    this.message = this.props.navigation.getParam("message");
    this.code = this.props.navigation.getParam("code");
    this.callBack = this.props.navigation.getParam("callBack");
    this.title = this.props.navigation.getParam("title");
    this.isCancel = false;
  }
  render() {
    var submitTitle = "Confirm";
    if (this.code === -1) {
      submitTitle = "Login";
    }
    if (this.code === -2 || this.code === 777) {
      this.isCancel = true;
    }
    return (
      <IView
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <IView
          style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: colors.black,
            opacity: 0.5
          }}
        ></IView>
        <IView
          style={{
            position: "absolute",
            width: "90%",
            padding: 20,
            backgroundColor: colors.white,
            borderRadius: 12,
            elevation: 12,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <IText
            style={{
              fontSize: 20,
              color: colors.text.black,
              fontWeight: "900"
            }}
          >
            {this.title}
          </IText>
          <IText
            style={{
              fontSize: 16,
              color: colors.text.black,
              marginVertical: 20,
              textAlign: "center"
            }}
          >
            {this.message}
          </IText>
          {this.isCancel ? (
            <IView style={{ flexDirection: "row" }}>
              <ITouch
                style={{
                  height: 44,
                  width: "50%",
                  borderRadius: 100,
                  marginTop: 8,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: colors.black,
                  elevation: 4,
                  flex: 1
                }}
                onPress={() => {
                  NavigationService.pop();
                }}
              >
                <IText
                  style={{
                    fontSize: 16,
                    color: colors.white,
                    fontWeight: "900"
                  }}
                >
                  Skip
                </IText>
              </ITouch>
              <IView style={{ width: 15 }}></IView>
              <ITouch
                style={{
                  height: 44,
                  width: "50%",
                  borderRadius: 100,
                  marginTop: 8,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: colors.black,
                  elevation: 4,
                  flex: 1
                }}
                onPress={this._submit}
              >
                <IText
                  style={{
                    fontSize: 16,
                    color: colors.white,
                    fontWeight: "900"
                  }}
                >
                  Login
                </IText>
              </ITouch>
            </IView>
          ) : (
            <ITouch
              style={{
                height: 44,
                width: "50%",
                borderRadius: 100,
                marginTop: 8,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: colors.black,
                elevation: 4
              }}
              onPress={this._submit}
            >
              <IText
                style={{
                  fontSize: 16,
                  color: colors.white,
                  fontWeight: "900"
                }}
              >
                {submitTitle}
              </IText>
            </ITouch>
          )}
        </IView>
      </IView>
    );
  }

  _submit = async () => {
    try {
      if (this.code === -1 || this.code === -2) {
        await UserService.reset();
        NavigationService.resetBackHome("oauthStack");
        return;
      }

      this.callBack ? this.callBack() : NavigationService.pop();
    } catch (err) {
      console.log(err);
    }
  };
}

export default AlertScreen;
