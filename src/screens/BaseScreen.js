import React, { Component } from "react";
import { Text, View, Dimensions, SafeAreaView } from "react-native";
import { IToolbar, IProgress } from "../components";
import { NavigationService } from "../services";
import EventEmitter from "react-native-eventemitter";
class BaseScreen extends Component {
  height = Dimensions.get("window").height;
  width = Dimensions.get("window").width;
  constructor(props) {
    super(props);
  }
  onEventUpdateWhenLikeProduct(listID){
    EventEmitter.emit('update_like', listID)
    EventEmitter.emit('update_catalogue')
  }
}

export default BaseScreen;
