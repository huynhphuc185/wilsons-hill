import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
	ITextInput,
} from '../../components'
import ITabbar from '../../components/ITabbar'
import { images } from '../../../assets'
import BaseScreen from '../BaseScreen'
class MyNotificationScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			tabs: {
				all: {
					screen: props => (
						<ChildScreen {...props} type={ChildScreen.TYPE.all} />
					),
					navigationOptions: {
						tabBarLabel: 'All',
					},
				},
				current_oder: {
					screen: props => (
						<ChildScreen
							{...props}
							type={ChildScreen.TYPE.current_oder}
						/>
					),
					navigationOptions: {
						tabBarLabel: 'Current Order',
					},
				},
			},
		}
	}
	render() {
		return (
			<View style={{ flex: 1, backgroundColor: '#f2f1f1' }}>
				<IToolbar title={'Notification'} notBack='true' />
				<IView style={{ flex: 1 }}>
					<ITabbar tabs={this.state.tabs} />
				</IView>
			</View>
		)
	}
}

class ChildScreen extends BaseScreen {
	static TYPE = {
		all: 1,
		current_oder: 2,
	}
	constructor(props) {
		super(props)
		this.state = {
			isLoading: false,
			notifilist: [
				{
					date: '12 - 11 - 2019',
					time: '15:00',
					content: 'Your order is placed and being processed',
					notSeen: true,
				},
				{
					date: '13 - 11 - 2019',
					time: '16:00',
					content: 'Your order is placed and being processed',
					notSeen: true,
				},
				{
					date: '13 - 11 - 2019',
					time: '16:30',
					content: 'Your order is placed and being processed',
					notSeen: false,
				},
				{
					date: '14 - 11 - 2019',
					time: '14:50',
					content: 'Your order is placed and being processed',
					notSeen: false,
				},
				{
					date: '15 - 11 - 2019',
					time: '02:40',
					content: 'Your order is placed and being processed',
					notSeen: true,
				},
				{
					date: '16- 11 - 2019',
					time: '19:00',
					content: 'Your order is placed and being processed',
					notSeen: true,
				},
			],
			current_oderList: [
				{
					type: 1,
					code: 'GF12337677',
				},
				{
					type: 2,
					code: 'GF12337677',
				},
				{
					type: 1,
					code: 'GF12337677',
				},
				{
					type: 2,
					code: 'GF12337677',
				},
			],
		}
		this.type = this.props.type
		this.page = 1
	}
	renderForwarder = data => {
		return (
			<IView
				style={{
					backgroundColor: 'white',
					height: 333,
					marginTop: 18,
					borderRadius: 8,
					paddingTop: 10,
					paddingHorizontal: 12,
				}}>
				<IText
					style={{
						fontSize: 12,
						textAlign: 'justify',
						letterSpacing: 1.6,
						color: 'black',
						lineHeight: 20,
					}}>
					Your order number #GF12337677 has been received and now in
					process. Please provide us shipping details such as
					forwarder information & Port of discharge. Thank you!
				</IText>
				<IText
					style={{
						fontSize: 14,
						letterSpacing: 1.6,
						marginTop: 13,
						color: 'black',
					}}>
					Forwarder:
				</IText>
				<IView
					style={{
						height: 67,
						borderWidth: 0.5,
						backgroundColor: 'white',
						marginTop: 10,
						paddingVertical: 7,
						paddingHorizontal: 12,
						borderColor: '#1C1C1C',
					}}>
					<ITextInput
						style={{
							fontSize: 14,
							color: 'black',
						}}
						placeholder={'Please fill in forwarder’s infomation'}
						onChangeText={value =>
							this.setState({ forwarder: value })
						}
					/>
				</IView>
				<IText
					style={{
						fontSize: 14,
						letterSpacing: 1.6,
						marginTop: 13,
						color: 'black',
					}}>
					Port of discharge
				</IText>
				<IView
					style={{
						height: 38,
						borderWidth: 0.5,
						backgroundColor: 'white',
						marginTop: 12,
						borderColor: '#1C1C1C',
						justifyContent: 'center',
						paddingHorizontal: 12,
					}}>
					<ITextInput
						style={{
							fontSize: 14,
							color: 'black',
						}}
						placeholder={''}
						onChangeText={value =>
							this.setState({ port_of_discharge: value })
						}
					/>
				</IView>
				<IView
					style={{
						marginTop: 10,
						alignItems: 'flex-end',
					}}>
					<ITouch
						style={{
							height: 26,
							width: 95,
							alignItems: 'center',
							justifyContent: 'center',
							backgroundColor: 'black',
						}}>
						<IText
							style={{
								color: 'white',
								fontSize: 12,
								letterSpacing: 1.6,
							}}>
							Confirm
						</IText>
					</ITouch>
				</IView>
			</IView>
		)
	}
	renderCurrent() {
		return (
			<IView
				style={{
					backgroundColor: 'white',
					height: 109,
					marginTop: 18,
					borderRadius: 8,
					paddingTop: 10,
					paddingHorizontal: 12,
				}}>
				<IText
					style={{
						fontSize: 12,
						textAlign: 'justify',
						letterSpacing: 1,
						color: 'black',
						lineHeight: 18,
					}}>
					Your custom request for order number #GF12337677 has been
					accepted/denied. Please review the order details and confirm
					it.
				</IText>
				<IView
					style={{
						marginTop: 10,
						alignItems: 'flex-end',
					}}>
					<ITouch
						style={{
							width: 134,
							borderBottomWidth: 1,
							justifyContent: 'space-between',
							alignItems: 'center',
							paddingVertical: 3,
							flexDirection: 'row',
						}}>
						<IText
							style={{
								fontSize: 12,
							}}>
							View order details
						</IText>
						<IFastImage
							source={images.vector}
							style={{ width: 16, height: 9 }}></IFastImage>
					</ITouch>
				</IView>
			</IView>
		)
	}
	renderItem_CurrentOrder = data => {
		console.log(data)

		switch (data.item.type) {
			case 1:
				return (
					<IView style={{ flex: 1 }}>{this.renderForwarder()}</IView>
				)
				break
			case 2:
				return <IView style={{ flex: 1 }}>{this.renderCurrent()}</IView>
				break
			// default:
			//   this.renderCurrent();
			//   break;
		}
	}

	_renderItem = data => {
		return (
			<ITouch
				style={{
					backgroundColor: 'white',
					height: 99,
					marginTop: 18,
					borderRadius: 8,
					flexDirection: 'row',
					paddingVertical: 13,
				}}>
				{data.item.notSeen ? (
					<IFastImage
						style={{
							width: 8,
							height: 8,
							position: 'absolute',
							right: 9,
							bottom: 9,
						}}
						source={images.dot}
						resizeMode='contain'
					/>
				) : null}

				<IView
					style={{
						paddingHorizontal: 10,
						justifyContent: 'center',
						alignItems: 'center',
						borderRightWidth: 1,
					}}>
					<IFastImage
						style={{ width: 64, height: 64 }}
						source={images.ic_noti}
						resizeMode='contain'
					/>
				</IView>
				<IView
					style={{
						flex: 1,
						paddingLeft: 15,
						paddingRight: 10,
					}}>
					<IView
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
						}}>
						<IText
							style={{
								fontSize: 12,
								lineHeight: 20,
							}}
							isSo={true}>
							{data.item.date}
						</IText>
						<IText
							style={{
								fontSize: 12,
								lineHeight: 20,
							}}
							isSo={true}>
							{data.item.time}
						</IText>
					</IView>
					<IView style={{ marginTop: 12 }}>
						<IText
							style={{
								fontSize: 12,
								fontFamily: 'normal',
								letterSpacing: 1.6,
							}}>
							{data.item.content}
						</IText>
					</IView>
				</IView>
			</ITouch>
		)
	}

	render() {
		switch (this.type) {
			case 1:
				// tab All
				return (
					<IView style={{ flex: 1, backgroundColor: '#f2f1f1' }}>
						<IView style={{ flex: 1 }}>
							<IFLatList
								style={{ flex: 1 }}
								contentContainerStyle={{
									paddingHorizontal: 18,
								}}
								data={this.state.notifilist}
								extraData={this.state.notifilist}
								keyExtractor={(item, index) => index + ''}
								renderItem={this._renderItem}
								showsVerticalScrollIndicator={false}
								// refreshing={this.state.isLoading}
							/>
						</IView>
					</IView>
				)
				break
			case 2:
				// tab Current Order
				return (
					<IView style={{ flex: 1, backgroundColor: '#f2f1f1' }}>
						<IView style={{ flex: 1 }}>
							<IFLatList
								style={{ flex: 1 }}
								contentContainerStyle={{
									paddingHorizontal: 18,
								}}
								data={this.state.current_oderList}
								extraData={this.state.current_oderList}
								keyExtractor={(item, index) => index + ''}
								renderItem={this.renderItem_CurrentOrder}
								showsVerticalScrollIndicator={false}
								// refreshing={this.state.isLoading}
							/>
						</IView>
					</IView>
				)
				break
			default:
				return (
					<IView
						style={{ flex: 1, backgroundColor: 'white' }}></IView>
				)
				break
		}
	}

	componentDidMount() {
		// this._get();
	}
}

export default MyNotificationScreen
