import React, { Component } from 'react'
import { View, Text, StyleSheet, Platform } from 'react-native'
import BaseScreen from '../BaseScreen'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
	ITextInput,
} from '../../components'
import { APIService, UserService, NavigationService } from '../../services'
import { colors, images } from '../../../assets'
import { KeyStorage } from '../../constants'
import AsyncStorage from '@react-native-community/async-storage'
import EnvService from '../../utils/EnvService'
import { validateEmail } from '../../utils'
class LoginScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = { email: '', password: '', errEmail: '', errPassword: '' }
	}

	login = async () => {
		if (!validateEmail(this.state.email)) {
      this.setState({errEmail:'Please enter a valid email address.'})
			return
		}else{
      this.setState({errEmail:''})
    }
		const data = await APIService.login(
			this.state.email,
			this.state.password,
		)

		if (data != null) {
			UserService.login({
				userName: this.state.email,
				token: data.token,
			})
			await AsyncStorage.setItem(KeyStorage.ISLOGIN, 'true')
			NavigationService.reset('mainStack')
		} else {
			console.log('that bai')
		}
	}

	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar title={''} notSearch='true' />
				<IView style={{ paddingHorizontal: 18, marginTop: 4 }}>
					<IFastImage
						source={images.login_logo}
						style={{ width: 227, height: 18 }}></IFastImage>
					<IView style={{ marginTop: 20 }}>
						<IText style={{ fontSize: 14 }}>Email</IText>
						<IView
							style={{
								height: 40,
								borderWidth: 0.5,
								justifyContent: 'center',
								marginVertical: 7,
								backgroundColor: 'white',
							}}>
							<ITextInput
								style={{
									fontSize: 14,
									marginLeft: 10,
									color: 'black',
								}}
								placeholder={'Type your email here'}
								onChangeText={value =>
									this.setState({ email: value })
								}
							/>
						</IView>
						{this.state.errEmail.length === 0 ? null : (
							<IText
								style={{ fontSize: 12, color: '#E11C1C' }}
								isItalic={true}>
								{this.state.errEmail}
							</IText>
						)}
					</IView>
					<IView style={{ marginTop: 7 }}>
						<IText style={{ fontSize: 14 }}>Password</IText>
						<IView
							style={{
								height: 40,
								borderWidth: 0.5,
								justifyContent: 'center',
								marginTop: 7,
								backgroundColor: 'white',
							}}>
							<ITextInput
								style={{
									fontSize: 14,
									marginLeft: 10,
									color: 'black',
								}}
								secureTextEntry={true}
								placeholder={'Type your password here'}
								onChangeText={value =>
									this.setState({ password: value })
								}>
								{/* <IText
                  style={{
                    fontSize: 14,
                    fontWeight: "500"
                  }}
                >
                  {this.state.password}
                </IText> */}
							</ITextInput>
						</IView>
					</IView>
					<IText
						style={{
							alignSelf: 'flex-end',
							marginTop: 15,
							fontSize: 14,
						}}>
						Forgot your password?
					</IText>
					<ITouch
						disabled={
							this.state.email.length > 0 &&
							this.state.password.length >= 6
								? false
								: true
						}
						style={{
							marginTop: 34,
							height: 40,
							backgroundColor: 'black',
							justifyContent: 'center',
							alignItems: 'center',
						}}
						onPress={() => this.login()}>
						<IText
							style={{
								color: 'white',
								fontSize: 16,
								fontWeight: '500',
							}}>
							LOG IN
						</IText>
					</ITouch>
					<IView
						style={{
							flexDirection: 'row',
							// backgroundColor: "red",
							justifyContent: 'center',
							alignItems: 'center',
							marginTop: 13,
						}}>
						<IText
							style={{
								alignSelf: 'center',
								fontSize: 14,
							}}>
							Don’t have an account?{' '}
						</IText>
						<ITouch
							style={{
								borderBottomWidth: 1,
								borderBottomColor: 'black',
							}}
							onPress={() => {
								this.props.navigation.navigate('register')
							}}>
							<IText
								style={{
									fontWeight: '500',
									fontSize: 14,
								}}>
								Make a request
							</IText>
						</ITouch>
					</IView>
				</IView>
				<IFastImage
					source={images.banner_login}
					style={{
						marginTop: 32,
						flex: 1,
					}}></IFastImage>
			</IView>
		)
	}
}

export default LoginScreen
