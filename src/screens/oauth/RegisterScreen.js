import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  ScrollView,
  Alert
} from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch,
  ITextInput
} from "../../components";
import { colors, images } from "../../../assets";
import { APIService, UserService, NavigationService } from "../../services";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaseScreen from "../BaseScreen";
import AlertScreen from "../common/AlertScreen";
class RegisterScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      company_name: "",
      country: "",
      billing_address: "",
      person_contact: "",
      occupation: "",
      email: "",
      phone: ""
    };
  }

  eventButton = () => {
    if (
      this.state.company_name.length === 0 ||
      this.state.country.length === 0 ||
      this.state.billing_address.length === 0 ||
      this.state.person_contact.length === 0 ||
      this.state.occupation.length === 0 ||
      this.state.email.length === 0 ||
      this.state.phone.length === 0
    ) {
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance("Please enter all information")
      );
      return;
    }
    APIService.register(
      this.state.company_name,
      this.state.country,
      this.state.billing_address,
      this.state.person_contact,
      this.state.occupation,
      this.state.email,
      this.state.phone
    ).then(data => {
      if (data != null) {
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Register success!", null, () => {
            NavigationService.navigate("login");
          })
        );
      }
    });
  };
  render() {
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={""} notSearch="true" />
        <KeyboardAwareScrollView style={{}}>
          <IView
            style={{ paddingHorizontal: 18, marginBottom: 32, marginTop: 4 }}
          >
            <IText
              style={{
                fontSize: 24,
                fontWeight: "500",
                letterSpacing: 1.6,
                lineHeight: 28
              }}
            >
              Request for an account
            </IText>
            <IText
              style={{
                marginTop: 5,
                marginRight: 6,
                fontSize: 12,
                // fontWeight: "300",
                letterSpacing: 1.26,
                lineHeight: 16
              }}
            >
              Please fill in this form and our team will get back to you for
              further discussion & creating an account.
            </IText>

            <IView style={{ marginTop: 24 }}>
              <IText style={{ fontSize: 14 }}>Company name</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  placeholder={"Your company name"}
                  onChangeText={value => this.setState({ company_name: value })}
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Country</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  placeholder={""}
                  onChangeText={value => this.setState({ country: value })}
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Billing address</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  // placeholder={"Your poisition"}
                  onChangeText={value =>
                    this.setState({ billing_address: value })
                  }
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Person in contact</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value =>
                    this.setState({ person_contact: value })
                  }
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Occupation</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => this.setState({ occupation: value })}
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Email</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => this.setState({ email: value })}
                />
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Phone number</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    color: "black"
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => this.setState({ phone: value })}
                />
              </IView>
            </IView>
            <ITouch
              style={{
                marginTop: 36,
                height: 40,
                backgroundColor: "black",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={this.eventButton}
            >
              <IText
                style={{ color: "white", fontSize: 16, fontWeight: "500" }}
              >
                SEND REQUEST
              </IText>
            </ITouch>
            <IView
              style={{
                flexDirection: "row",
                // backgroundColor: "red",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 13
              }}
            >
              <IText style={{ alignSelf: "center", fontSize: 14 }}>
                Already have an account?{" "}
              </IText>
              <ITouch
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: "black"
                }}
                onPress={() => {
                  this.props.navigation.pop();
                }}
              >
                <IText style={{ fontWeight: "500", fontSize: 14 }}>
                  Log in
                </IText>
              </ITouch>
            </IView>
          </IView>
        </KeyboardAwareScrollView>
      </IView>
    );
  }
}

export default RegisterScreen;
