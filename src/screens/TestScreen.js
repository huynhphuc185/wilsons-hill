import React, { Component } from "react";
import { Text, View } from "react-native";
import { IView, IText, ITouch } from "../components";
import { colors, strings } from "../../assets";
import { IToolbar } from "../components";
import { NavigationService, APIService } from "../services";

class TestScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colors: {
        main: "yellow"
      }
    };
  }
  // componentDidMount() {
  //   colors.set(this.state.colors);
  //   this.setState({});
  // }

  render() {
    // {colors.set(this.state.colors)}
    return (
      <IView style={{ flex: 1, backgroundColor: colors.white }}>
        <IToolbar />
        <IText
          style={{
            fontFamily: "bold",
            fontSize: 32
          }}
        >
          {strings.get("lang")}
        </IText>
        <IText
          style={{
            fontFamily: "bold",
            fontSize: 32,
            color: colors.main
          }}
        >
          Text Bold
        </IText>

        <IText
          style={{
            fontFamily: "medium",
            fontSize: 32,
            color: colors.main
          }}
        >
          Text Medium (500)
        </IText>
        <IText
          style={{
            fontSize: 32,
            color: colors.main
          }}
        >
          Text Regular(default)
        </IText>
        {/* only use elevation for android & ios */}
        <IView
          style={{
            margin: 20,
            elevation: 12,
            backgroundColor: colors.white,
            padding: 20
          }}
        >
          <IText>elevation</IText>
        </IView>

        <ITouch
          style={{
            margin: 20,
            elevation: 12,
            backgroundColor: colors.main,
            padding: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
          onPress={() => {
            //like to this.props.navigation
            // NavigationService.reset("mainStack");
            NavigationService.navigate("login")
          }}
        >
          <IText style={{color: "white"}}>To Home</IText>
        </ITouch>
        <ITouch
          style={{
            margin: 20,
            elevation: 12,
            backgroundColor: colors.main,
            padding: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
          onPress={() => {
            //like to this.props.navigation
            // NavigationService.reset("mainStack");
            NavigationService.navigate("detailProduct")
          }}
        >
          <IText style={{color: "white"}}>To Home</IText>
        </ITouch>
      </IView>
    );
  } 
}

export default TestScreen;
