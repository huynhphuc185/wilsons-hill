import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import React, { Component } from 'react'
import LoginScreen from './oauth/LoginScreen'
import HomeScreen from './home/HomeScreen'
import AlertScreen from './common/AlertScreen'
import { Easing, Animated } from 'react-native'
import RegisterScreen from './oauth/RegisterScreen'
import DetailProductScreen from './product/DetailProductScreen'
import BestSellerScreen from './product/BestSellerScreen'
import CartScreen from './cart/CartScreen'
import CheckOutScreen from './cart/CheckOutScreen'
import AccountScreen from './account/AccountScreen'
import EditAccount from './account/EditAccount'
import DetailAccountScreen from './account/DetailAccountScreen'
import ChangePassword from './account/ChangePassword'
import OrderHistoryScreen from './account/OrderHistoryScreen'
import NotificationScreen from './noti/MyNotificationScreen'
import CatalogueScreen from './catalogue/MyCatalogueScreen'
import PreviousOrders from './catalogue/PreviousOrders'
import SavedCollections from './catalogue/SavedCollections'
import SaveItems from './catalogue/SaveItems'
import SplashComponent from './splash/SplashScreen'
import SplashLoginScreen from './splash/SplashLoginScreen'
import CollectionScreen from './product/CollectionScreen'
import CollectionDetailScreen from './product/CollectionDetailScreen'
import SearchScreen from './search/SearchScreen'
import SearchResultScreen from './search/SearchResultScreen'
import IProgress from '../components/IProgress'
import { colors, images, strings } from '../../assets'
import { IFastImage, IView, IText } from '../components'
import { NavigationService, UserService } from '../services'
import Global from '../constants/Global'
import EventEmitter from 'react-native-eventemitter'
import BottomBarCartComponent from './BottomBarCartComponent'
import ShippingDetaillScreen from '../screens/cart/ShippingDetaillScreen'
import OrderDetailScreen from './account/OrderDetailScreen'
import { constraintLayout } from '../components/ILayoutProps'
const HomeTab = createStackNavigator(
	{
		HomeScreen,
		CollectionScreen,
		CollectionDetailScreen,
		BestSellerScreen,
		DetailProductScreen,
		CartScreen,
		ShippingDetaillScreen,
		CheckOutScreen,
	},
	{
		navigationOptions: ({ navigation }) => ({
			tabBarVisible:
				navigation.state.routes[navigation.state.index].routeName ===
					'DetailProductScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'CartScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'CheckOutScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'ShippingDetaillScreen'
					? false
					: true,
		}),

		headerMode: 'none',
	},
)

const Catalogue = createStackNavigator(
	{
		CatalogueScreen,
		PreviousOrders,
		OrderDetailScreen,
		SavedCollections,
		SaveItems,
		DetailProductScreen,
		CollectionDetailScreen,
	},
	{
		headerMode: 'none',
	},
)
const MyOrderTab = createStackNavigator(
	{
		CartScreen,
		CheckOutScreen,
		ShippingDetaillScreen,
	},
	{
		headerMode: 'none',
	},
)
const NotificationTab = createStackNavigator(
	{
		NotificationScreen,
	},
	{
		headerMode: 'none',
	},
)
const AccountTab = createStackNavigator(
	{
		AccountScreen,
		OrderHistoryScreen,
		DetailAccountScreen,
		ChangePassword,
		EditAccount,
		OrderDetailScreen,
	},
	{
		headerMode: 'none',
	},
)
const MainApp = createBottomTabNavigator(
	{
		home: HomeTab,
		catalogue: Catalogue,
		order: MyOrderTab,
		notification: NotificationTab,
		account: AccountTab,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarOptions:
				navigation.state.routes[navigation.state.index].routeName ===
					'CollectionDetailScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'CartScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'CheckOutScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'ShippingDetaillScreen' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'SavedCollections' ||
				navigation.state.routes[navigation.state.index].routeName ===
					'SaveItems'
					? {
							showLabel: false,
							activeTintColor: '#FF6F00',
							inactiveTintColor: '#263238',
							style: {
								borderTopColor: 'transparent',
								height: constraintLayout(65),
							},
					  }
					: {
							showLabel: false,
							activeTintColor: '#FF6F00',
							inactiveTintColor: '#263238',
							style: {
								shadowOffset: { width: 0, height: -1 },
								shadowOpacity: 0.5,
								elevation: 20,
								borderTopColor: 'transparent',
								height: constraintLayout(65),
							},
					  },
			tabBarOnLongPress: () => {
				// return null
			},
			tabBarOnPress: async ({ defaultHandler }) => {
				let isLogin = await UserService.isLogin()
				if (navigation.state.routeName != 'order') {
					Global.previousRoute = navigation.state.routeName
				}

				//kiem tra login
				if (navigation.state.routeName === 'catalogue') {
					if (!isLogin) {
						NavigationService.navigate(
							'alert',
							AlertScreen.newInstance('Login required', -2),
						)
						return
					}
				}
				if (navigation.state.routeName === 'order') {
					if (!isLogin) {
						NavigationService.navigate(
							'alert',
							AlertScreen.newInstance('Login required', -2),
						)
						return
					}
				}
				if (navigation.state.routeName === 'notification') {
					if (!isLogin) {
						NavigationService.navigate(
							'alert',
							AlertScreen.newInstance('Login required', -2),
						)
						return
					}
				}
				if (navigation.state.routeName === 'account') {
					if (!isLogin) {
						NavigationService.navigate(
							'alert',
							AlertScreen.newInstance('Login required', -2),
						)
						return
					}
				}
				defaultHandler()
			},

			tabBarIcon: ({ focused }) => {
				const { routeName } = navigation.state
				if (routeName === 'home') {
					return focused ? (
						<IFastImage
							resizeMode='contain'
							source={images.home_black}
							style={{ width: 25, height: 25 }}
						/>
					) : (
						<IFastImage
							resizeMode='contain'
							source={images.home_white}
							ta
							style={{ width: 25, height: 25 }}
						/>
					)
				} else if (routeName === 'catalogue') {
					return focused ? (
						<IFastImage
							resizeMode='contain'
							source={images.catalogue_black}
							style={{ width: 25, height: 25 }}
						/>
					) : (
						<IFastImage
							resizeMode='contain'
							source={images.catalogue_white}
							style={{ width: 25, height: 25 }}
						/>
					)
				} else if (routeName === 'order') {
					return (
						<BottomBarCartComponent
							focused={focused}></BottomBarCartComponent>
					)
				} else if (routeName === 'notification') {
					return focused ? (
						<IFastImage
							resizeMode='contain'
							source={images.noti_black}
							style={{ width: 25, height: 25 }}
						/>
					) : (
						<IFastImage
							resizeMode='contain'
							source={images.noti_white}
							style={{ width: 25, height: 25 }}
						/>
					)
				} else if (routeName === 'account') {
					return focused ? (
						<IFastImage
							resizeMode='contain'
							source={images.account_black}
							style={{ width: 25, height: 25 }}
						/>
					) : (
						<IFastImage
							resizeMode='contain'
							source={images.account_white}
							style={{ width: 25, height: 25 }}
						/>
					)
				}
			},
		}),
	},
)

const OauthStack = createStackNavigator(
	{
		login: {
			screen: LoginScreen,
		},
		register: {
			screen: RegisterScreen,
		},
	},
	{
		headerMode: 'none',
	},
)
const SearchStack = createStackNavigator(
	{
		SearchScreen,
		SearchResultScreen,
		BestSellerScreen,
		DetailProductScreen,
		CollectionDetailScreen,
		CartScreen,
		ShippingDetaillScreen,
		CheckOutScreen,
	},
	{
		headerMode: 'none',
	},
)
const App = createStackNavigator(
	{
		splashStack: {
			screen: SplashComponent,
		},
		splashLogin: {
			screen: SplashLoginScreen,
		},
		oauthStack: {
			screen: OauthStack,
		},
		mainStack: {
			screen: MainApp,
		},
		alert: {
			screen: AlertScreen,
		},
		SearchStack,
		IProgress,
	},

	{
		initialRouteName: 'splashStack',
		headerMode: 'none',
		mode: 'card',
		transparentCard: true,
		transitionConfig: () => transitionConfig(),
	},
)

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 300,
			easing: Easing.out(Easing.poly(2)),
			timing: Animated.timing,
			useNativeDriver: true,
		},
		screenInterpolator: sceneProps => {
			const { position, layout, scene } = sceneProps
			const thisSceneIndex = scene.index
			const height = layout.initHeight
			const translateY = position.interpolate({
				inputRange: [
					thisSceneIndex - 1,
					thisSceneIndex,
					thisSceneIndex + 1,
				],
				outputRange: [100, 0, 0],
			})
			const opacity = position.interpolate({
				inputRange: [
					thisSceneIndex - 1,
					thisSceneIndex,
					thisSceneIndex + 1,
				],
				outputRange: [0, 1, 1],
			})
			const slideFromRight = {
				opacity,
				//transform: [{translateX: 0}, {translateY}],
			}
			return slideFromRight
		},
	}
}
export default createAppContainer(App)
