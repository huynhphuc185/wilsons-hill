import React, { Component } from 'react'
import { Text, View, StatusBar, Alert } from 'react-native'
import { colors, images, strings } from '../../../assets'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
} from '../../components'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { DrawerActions } from 'react-navigation-drawer'
import { NavigationService, APIService, UserService } from '../../services'
import ConfigServer from '../../utils/ConfigServer'
import DeviceInfo from 'react-native-device-info'
import EnvService from '../../utils/EnvService'
import { TouchableOpacity } from 'react-native-gesture-handler'
SIZE = 48
class HomeToolbar extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
		this.countConfig = 0
	}
	render() {
		const {isBack } = this.props
		return (
			<ITouch
				activeOpacity={1}
				onPress={() => {
					this.handleChangeServer()
				}}
				style={{ backgroundColor: colors.white }}>
				<StatusBar
					backgroundColor={'transparent'}
					translucent={true}
					animated
					barStyle='dark-content'
				/>
				<View style={{ height: getStatusBarHeight() }}></View>
				<IView
					style={{
						height: SIZE,
						flexDirection: 'row',
						justifyContent: 'space-between',
						alignItems: 'center',
						paddingHorizontal: 20,
					}}>
					<IFastImage
						source={images.home_logo}
						style={{ width: 207, height: 20 }}></IFastImage>
					<ITouch
						style={{
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							NavigationService.navigate('SearchScreen')
						}}>
						<IFastImage
							source={images.ic_search}
							style={{ width: 21, height: 21 }}></IFastImage>
					</ITouch>
				</IView>
			</ITouch>
		)
	}
	async handleChangeServer() {
		this.countConfig++
		console.log(`touch ${this.countConfig}`)

		clearTimeout(this.timer)
		this.timer = setTimeout(() => {
			this.countConfig = 0
			console.log(`reset. touch ${this.countConfig}`)
		}, 500)

		if (this.countConfig == 7) {
			let config = ConfigServer.getConfig()
			const currentEnv = await new EnvService().getCurrentEnv()
			this.countConfig = 0
			Alert.alert(
				'',

				'Current server: ' +
					currentEnv +
					' - ' +
					DeviceInfo.getVersion(),
				[
					{
						text: 'DEV',
						onPress: async () => {
							await new EnvService().setCurrentEnv(
								EnvService.ENV.DEV,
							)
							APIService.setBaseURL(config.restful_ip_dev)
							this.resetWhenChooseServer()
						},
					},
					{
						text: 'TEST',
						onPress: async () => {
							await new EnvService().setCurrentEnv(
								EnvService.ENV.TEST,
							)
							APIService.setBaseURL(config.restful_ip_test)
							this.resetWhenChooseServer()
						},
					},
					{
						text: 'REAL',
						onPress: async () => {
							await new EnvService().setCurrentEnv(
								EnvService.ENV.REAL,
							)
							APIService.setBaseURL(config.restful_ip)
							this.resetWhenChooseServer()
						},
					},
					{
						text: 'LOGOUT',
						onPress: () => {
							this.resetWhenChooseServer()
						},
					},
					{
						text: 'CANCEL',
					},
				],
				{ cancelable: true },
			)
		}
	}

	resetWhenChooseServer = async () => {
		await UserService.reset()
		NavigationService.resetBackHome('oauthStack')
	}


	
}
export default HomeToolbar
