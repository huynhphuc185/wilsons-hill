import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  FlatList,
  Image
} from "react-native";
import BaseScreen from "../BaseScreen";
import HomeToolbar from "./HomeToolbar";
import {
  IView,
  IFastImage,
  ITouch,
  INotity,
  ICart,
  IText,
  ITextInput,
  IProgress
} from "../../components";
import { colors, images, strings } from "../../../assets";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { ScrollView } from "react-native-gesture-handler";
import ICombobox from "../../components/ICombobox";
import RowCollection from "../row/RowCollection";
import { constraintLayout } from "../../components/ILayoutProps";
import DynamicFlatlistProduct from "./DynamicFlatlistProduct";
import IFlatList from "../../components/IFLatList";

import ContentLoader from "react-native-content-loader";
import { Circle, Rect } from "react-native-svg";
import ExploreComponent from "./ExploreComponent";
import { APIService } from "../../services";
import IBanner from "./IBanner";
import SplashScreen from "react-native-splash-screen";
import FantasyPushNotificationFirebase from "../../services/FantasyPushNotificationFirebase";
import { KeyboardAwareFlatList } from "react-native-keyboard-aware-scroll-view";
import IScrollView from "../../components/IScrollView";
class HomeScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      isRefresh: false
    };
    this.category = [];
    this.bestSeller = [];
    this.collection = [];
    this.listSort = [];
    this.listProduct = [];
    this.fbs = new FantasyPushNotificationFirebase();
  }

  componentDidMount() {
    this.fbs.registerFirebase(
      { handleDisplayNotify: data => {} },
      {
        handleOpenedNotify: data => {
          let url = data.notification.data.url;
          if (url !== null && url !== "undefined") {
            this.fbs.removeAllNotification();
            //this.deeplinkService.handleUrl({ url: url })
          }
        }
      },
      { handleDataNotify: data => {} }
    );

    const getListColletion = APIService.getListColletion(false);
    const getListCategory = APIService.getListCategory(false);
    const getBestSeller = APIService.getMainBestSeller(false);
    const getListSort = APIService.getListSort(false);
    Promise.all([
      getListColletion,
      getListCategory,
      getBestSeller,
      getListSort
    ]).then(result => {
      this.collection = result[0].collection;
      this.category = result[1].category;
      this.bestSeller = result[2].best_seller;
      this.listSort = result[3].sort_product_types;
      this._getListProduct();
    });
  }
  _getListProduct = async page => {
    this.listProduct = await APIService.getListProductByCate(
      this.category[0].id,
      this.listSort[0].id,
      1,
      false
    );
    this.setState({ isRefresh: true });
  };
  renderLoader = () => {
    let margin = 15;
    const width = (this.width - margin * 4) / 3;
    const widthItem = (this.width - margin * 4) / 3;
    return (
      <ContentLoader
        height={this.height}
        width={this.width}
        speed={1}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
      >
        <Rect x="0" y="0" rx="0" ry="0" width={this.width} height="207" />
        <Rect x={margin} y="222" rx="12" ry="12" width={width} height={width} />

        <Rect
          x={width + 2 * margin}
          y="222"
          rx="12"
          ry="12"
          width={width}
          height={width}
        />
        <Rect
          x={2 * width + 3 * margin}
          y="222"
          rx="12"
          ry="12"
          width={width}
          height={width}
        />

        <Rect
          x={margin}
          y={222 + margin + width}
          rx="12"
          ry="12"
          width={width}
          height={width}
        />
        <Rect
          x={width + 2 * margin}
          y={222 + margin + width}
          rx="12"
          ry="12"
          width={width}
          height={width}
        />
        <Rect
          x={2 * width + 3 * margin}
          y={222 + margin + width}
          rx="12"
          ry="12"
          width={width}
          height={width}
        />

        <Rect
          x={margin}
          y={222 + 2 * margin + 2 * width}
          rx="8"
          ry="8"
          width={(this.width - margin * 3) / 2}
          height="150"
        />
        <Rect
          x={margin}
          y={222 + 2 * margin + 2 * width + 155}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2}
          height="15"
        />
        <Rect
          x={margin}
          y={222 + 2 * margin + 2 * width + 175}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2 - 20}
          height="10"
        />
        <Rect
          x={margin}
          y={222 + 2 * margin + 2 * width + 190}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2 - 40}
          height="10"
        />

        <Rect
          x={(this.width - margin * 3) / 2 + 2 * margin}
          y={222 + 2 * margin + 2 * width}
          rx="8"
          ry="8"
          width={(this.width - margin * 3) / 2}
          height="150"
        />
        <Rect
          x={(this.width - margin * 3) / 2 + 2 * margin}
          y={222 + 2 * margin + 2 * width + 155}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2}
          height="15"
        />
        <Rect
          x={(this.width - margin * 3) / 2 + 2 * margin}
          y={222 + 2 * margin + 2 * width + 175}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2 - 20}
          height="10"
        />
        <Rect
          x={(this.width - margin * 3) / 2 + 2 * margin}
          y={222 + 2 * margin + 2 * width + 190}
          rx="4"
          ry="4"
          width={(this.width - margin * 3) / 2 - 40}
          height="10"
        />
      </ContentLoader>
    );
  };
  renderTitle = (text, callBack) => {
    return (
      <IView
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          paddingHorizontal: 18,
          marginVertical: 12,
          height: 23
        }}
      >
        <IText style={{ fontSize: 20, color: colors.text.black }}>{text}</IText>
        {callBack != null ? (
          <ITouch
            style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row"
            }}
            onPress={() => {
              callBack();
            }}
          >
            <IText
              style={{
                fontSize: 14,
                marginRight: 11,
                color: colors.text.black
              }}
            >
              View all
            </IText>
            <IFastImage
              source={images.vector}
              style={{ width: 16, height: 9 }}
            ></IFastImage>
          </ITouch>
        ) : null}
      </IView>
    );
  };
  // categoris
  renderBestSeller = () => {
    return (
      <ITouch
        onPress={() => {
          this.props.navigation.navigate("BestSellerScreen");
        }}
        style={{
          height: 226,

          marginHorizontal: 18,
          backgroundColor: "white",
          borderRadius: 10
        }}
      >
        <IFlatList
          style={{
            flex: 1,
            paddingVertical: 9,
            paddingHorizontal: 9
          }}
          data={this.bestSeller}
          renderItem={({ item, index }) => this.itemBestSeller(item, index)}
          extraData={this.bestSeller}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        />
      </ITouch>
    );
  };

  itemBestSeller = (item, index) => {
    return (
      <IView
        style={{
          height: (226 - 18) / 2,
          flex: 1
        }}
      >
        <IFastImage
          resizeMode="contain"
          style={{
            backgroundColor: "white",
            borderRadius: 10,
            flex: 1,
            margin: 9
          }}
          source={{ uri: item.image }}
        />
      </IView>
    );
  };

	render() {
		return this.state.isRefresh === true ? (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<HomeToolbar></HomeToolbar>
				<IScrollView
					contentContainerStyle={{ paddingBottom: 10 }}
					onBottom={() => {
						this.flatList.loadMore()
					}}>
					{this.collection.length !== 0 ? (
						<IView>
							{this.renderTitle('Collection', () => {
								this.props.navigation.navigate(
									'CollectionScreen',
								)
							})}
							<IBanner
								data={this.collection}
								height={constraintLayout(212)}
								isTouch={true}
								isCover = {true}
								></IBanner>
						</IView>
					) : null}

          {this.renderTitle("Best Sellers")}

          {this.renderBestSeller()}
          <ExploreComponent
            ref={ref => {
              this.flatList = ref;
            }}
            category={this.category}
            listSort={this.listSort}
            listProduct={this.listProduct.product}
          ></ExploreComponent>
        </IScrollView>
      </IView>
    ) : (
      <IView>
        <HomeToolbar></HomeToolbar>
        {this.renderLoader()}
      </IView>
    );
  }
}
export default HomeScreen;
