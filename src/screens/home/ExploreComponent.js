import React, { Component } from 'react'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
	IFLatList,
} from '../../components'
import { colors, images, strings } from '../../../assets'
import AutoHeightImage from 'react-native-scalable-image'
import { withNavigation } from 'react-navigation'
import BaseScreen from '../BaseScreen'
import { NavigationService, APIService } from '../../services'
import IFlatList from '../../components/IFLatList'
import ICombobox from '../../components/ICombobox'
import DynamicFlatlistProduct from './DynamicFlatlistProduct'
import { constraintLayout } from '../../components/ILayoutProps'
import EventEmitter from 'react-native-eventemitter'
class ExploreComponent extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isRefresh: false,
		}
		this.page = 1
		this.indexSort = 0
		this.indexExploreSelect = 0
		this.category = this.props.category
		this.listSort = this.props.listSort
		this.listProduct = this.props.listProduct
		
	}
	_getListProduct = async () => {
		const cate_id = this.category[this.indexExploreSelect].id
		const sort_id = this.listSort[this.indexSort].id
		const page = this.page
		const listProduct = await APIService.getListProductByCate(
			cate_id,
			sort_id,
			page,
		)
		this.listProduct = listProduct.product
		this.setState({ isRefresh: true })
		if (this._flatList !== null){
			this._flatList.refreshProductList(this.listProduct)
		}
		
	}
	loadMore = async () => {
		const cate_id = this.category[this.indexExploreSelect].id
		const sort_id = this.listSort[this.indexSort].id
		this.page++
		const listProduct = await APIService.getListProductByCate(
			cate_id,
			sort_id,
			this.page,
			false,
		)
		this.listProduct = this.listProduct.concat(listProduct.product)
		this.setState({ isRefresh: true })
		if (this._flatList !== null){
			this._flatList.refreshProductList(this.listProduct)
		}
	}
	getDataStringCombobox = listSort => {
		var listString = []
		listSort.map((item, index) => {
			listString.push(item.name)
		})
		return listString
	}
	renderTitle = (text, callBack) => {
		return (
			<IView
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					paddingHorizontal: 18,
					marginVertical: 12,
					height: 23,
				}}>
				<IText style={{ fontSize: 20, color: colors.text.black }}>
					{text}
				</IText>
				{callBack != null ? (
					<ITouch
						style={{
							alignItems: 'center',
							justifyContent: 'center',
							flexDirection: 'row',
						}}
						onPress={() => {
							callBack()
						}}>
						<IText
							style={{
								fontSize: 14,
								marginRight: 11,
								color: colors.text.black,
							}}>
							View all
						</IText>
						<IFastImage
							source={images.vector}
							style={{ width: 16, height: 9 }}></IFastImage>
					</ITouch>
				) : null}
			</IView>
		)
	}
	renderExplore = () => {
		return (
			<IView
				style={{
					height: 96,
					width: '100%',
				}}>
				<IFlatList
					style={{
						flex: 1,
					}}
					ref={ref => {
						this.flatListExplore = ref
					}}
					contentContainerStyle={{ paddingHorizontal: 18 }}
					data={this.category}
					renderItem={({ item, index }) =>
						this.itemExplore(item, index)
					}
					extraData={this.state.isRefresh}
					horizontal
					keyExtractor={(item, index) => index.toString()}
					ItemSeparatorComponent={() => (
						<IView style={{ width: 5 }} />
					)}
					showsHorizontalScrollIndicator={false}
				/>
			</IView>
		)
	}
	itemExplore = (item, index) => {
		return (
			<ITouch
				onPress={() => {
					this.page = 1
					this.indexExploreSelect = index
					this.flatListExplore._scrollToIndex(index)
					this._getListProduct()
				}}
				style={{
					height: 96,
					width: 96,
				}}>
				<IFastImage
					source={{
						uri:
							this.indexExploreSelect === index
								? item.image_active
								: item.image_deactive,
					}}
					style={{
						flex: 1,
						backgroundColor:
							this.indexExploreSelect == index
								? colors.white
								: colors.Transparent,
						borderRadius: 48,
					}}
				/>
			</ITouch>
		)
	}

	renderSortBy = callBack => {
		return (
			<IView
				style={{
					flexDirection: 'row',
					justifyContent: 'flex-end',
					alignItems: 'center',
					paddingHorizontal: 18,
					paddingVertical: 10,
				}}>
				<IText
					style={{
						fontSize: 14,
						color: colors.text.black,
						marginRight: 14,
					}}>
					Sort by
				</IText>

				<IView
					style={{
						height: 40,
						width: 178,
						// borderRadius: 10,
						// backgroundColor: colors.white,
					}}>
					<IView
						style={{
							flex: 1,
							justifyContent: 'center',
							alignItems: 'center',
						}}>
						<ICombobox
							widthItem={constraintLayout(178)}
							defaultValue={this.listSort[this.indexSort].name}
							listValue={this.getDataStringCombobox(
								this.listSort,
							)}
							callBack={index => {
								this.page = 1
								this.indexSort = index
								this._getListProduct()
							}}
						/>
					</IView>
				</IView>
			</IView>
		)
	}
	render() {
		return (
			<IView style={{ flex: 1 }}>
				{this.renderTitle('Explore')}
				{this.renderExplore()}
				{this.listProduct.length !== 0 ? (
					<IView>
						{this.renderSortBy()}
						<DynamicFlatlistProduct
							listProduct={this.listProduct}
							ref={ref => {
								this._flatList = ref
							}}></DynamicFlatlistProduct>
					</IView>
				) : null}
			</IView>
		)
	}
}

export default ExploreComponent
