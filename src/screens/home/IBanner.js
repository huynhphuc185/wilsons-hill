import React, { Component } from "react";
import { Text, View, StatusBar, ScrollView } from "react-native";
import { IView, IText, IFastImage, ITouch } from "../../components";
import { colors, images, strings } from "../../../assets";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { APIService, NavigationService, UserService } from "../../services";
import { DrawerActions } from "react-navigation-drawer";
import EventEmitter from "react-native-eventemitter";
import { Event } from "../../constants";
import Carousel, { Pagination } from "react-native-snap-carousel";
import RowCollection from "../row/RowCollection";
import BaseScreen from "../BaseScreen";
class IBanner extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			data: this.props.data,
			indexBanner: 0,
		}
	}
	render() {
		const { width, height, isTouch, isCover = false } = this.props
		return (
			<IView style={{ flex: 1, backgroundColor: colors.white }}>
				<IView
					style={{
						height: height,
						width: this.width,
						backgroundColor: colors.white,
					}}>
					<Carousel
						ref={c => {
							this._carousel = c
						}}
						data={this.state.data}
						renderItem={({ item, index }) =>
							isTouch ? (
								<ITouch
									onPress={() => {
										NavigationService.navigate(
											'CollectionDetailScreen',
											{
												collection_id: item.id,
											},
										)
									}}
									style={{
										alignItems: 'center',
										justifyContent: 'center',
									}}>
									<IFastImage
										source={{ uri: item.image || item }}
										resizeMode={
											isCover ? 'cover' : 'contain'
										}
										style={{
											width: '100%',
											height: '100%',
										}}></IFastImage>
								</ITouch>
							) : (
								<IView
									style={{
										alignItems: 'center',
										justifyContent: 'center',
									}}>
									<IFastImage
										source={{ uri: item.image || item }}
										resizeMode={
											isCover ? 'cover' : 'contain'
										}
										style={{
											height: '100%',
											width: '100%',
										}}></IFastImage>
								</IView>
							)
						}
						activeAnimationType={'timing'}
						sliderWidth={this.width}
						itemWidth={this.width}
						loop
						autoplay
						firstItem={this.state.indexBanner}
						onSnapToItem={index => {
							this.setState({ indexBanner: index })
						}}
					/>
				</IView>
				<IView>
					<Pagination
						dotsLength={this.state.data.length}
						activeDotIndex={this.state.indexBanner}
						containerStyle={{
							backgroundColor: 'white',
							paddingVertical: 13,
						}}
						dotStyle={{
							width: 24,
							height: 7,
							borderRadius: 3.5,
							backgroundColor: '#474647',
							marginHorizontal: -5,
						}}
						inactiveDotStyle={{
							width: 7,
							height: 7,
							borderRadius: 3.5,
							backgroundColor: '#909090',
							marginHorizontal: -5,
						}}
						// inactiveDotOpacity={0.4}
						inactiveDotScale={0.6}
					/>
				</IView>
			</IView>
		)
	}

	componentDidMount() {}
	componentWillUnmount() {}
}

export default IBanner;
