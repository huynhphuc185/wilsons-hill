import React, { Component } from "react";
import { Text, View, StatusBar, ScrollView } from "react-native";
import { IView, IText, IFastImage, ITouch } from "../../components";
import { colors, images, strings } from "../../../assets";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { APIService, NavigationService, UserService } from "../../services";
import { DrawerActions } from "react-navigation-drawer";
import EventEmitter from "react-native-eventemitter";
import { Event } from "../../constants";

class MenuScreen extends Component {
  ROUTE = {
    EVENT: "EVENT",
    HISTORY: "HISTORY",
    FOLLOW: "FOLLOW"
  };
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      isLogin: false
    };
    this.menus = [
      {
        route: this.ROUTE.EVENT,
        name: strings.get("event")
      },
      {
        route: this.ROUTE.HISTORY,
        name: strings.get("history_order")
      },
      {
        route: this.ROUTE.FOLLOW,
        name: strings.get("follow_order")
      }
    ];
  }
  render() {
    return (
      <IView style={{ backgroundColor: colors.main, flex: 1 }}>
        {this._renderHeader()}
        {this._renderContent()}
        {this._renderFooter()}
      </IView>
    );
  }

  componentDidMount() {
    this._getProfile();
    EventEmitter.on(Event.UPDATE_PROFILE, this._getProfile);
  }
  componentWillUnmount() {
    EventEmitter.removeListener(Event.UPDATE_PROFILE, this._getProfile);
  }

  _getProfile = async () => {
    try {
      const data = await APIService._getProfile();
      const isLogin = await UserService.isLogin();
      this.setState({
        profile: data,
        isLogin: isLogin
      });
    } catch (error) {}
  };

  _renderHeader = () => {
    const {
      avatar = "",
      fullname = "",
      address = "",
      ward_name = "",
      district_name = "",
      city_name = ""
    } = this.state.profile;
    var fullAddress = "";
    if (address) {
      fullAddress =
        address + ", " + ward_name + ", " + district_name + ", " + city_name;
    }
    return (
      <IView>
        <IView
          style={{
            alignItems: "center",
            overflow: "hidden"
          }}
        >
          <IView
            style={{
              position: "absolute",
              borderRadius: 204,
              backgroundColor: colors.mainDark,
              top: -205,
              left: -80,
              width: 408,
              height: 408
            }}
          />
          <View style={{ height: getStatusBarHeight() }}></View>

          <ITouch
            style={{
              width: 140,
              height: 140,
              backgroundColor: colors.white,
              borderRadius: 100,
              elevation: 8,
              margin: 24,
              alignItems: "center",
              justifyContent: "center"
            }}
            onPress={() => {
              NavigationService.navigate("profile");
            }}
          >
            <IFastImage
              source={{
                uri: avatar
              }}
              style={{
                width: 136,
                height: 136,
                overflow: "hidden",
                borderRadius: 100
              }}
              resizeMode="cover"
            />
          </ITouch>
        </IView>
        <IView style={{ padding: 12, alignItems: "center" }}>
          <IText
            style={{
              fontSize: 16,
              fontFamily: "bold",
              color: colors.white
            }}
          >
            {fullname}
          </IText>
          <IText
            style={{
              fontSize: 14,
              color: colors.white,
              marginTop: 4,
              textAlign: "center"
            }}
          >
            {fullAddress}
          </IText>
        </IView>
      </IView>
    );
  };
  _renderContent = () => {
    return (
      <ScrollView
        bounces={false}
        style={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1, paddingTop: 32 }}
      >
        {this.menus.map((item, index) => {
          return this._renderItem(item);
        })}
      </ScrollView>
    );
  };
  _renderFooter = () => {
    var oauthText = strings.get("login");
    if (this.state.isLogin) {
      oauthText = strings.get("logout");
    }
    return (
      <IView>
        <IView
          style={{
            alignItems: "center",
            overflow: "hidden"
          }}
        >
          <ITouch
            style={{
              backgroundColor: colors.mainDark,
              justifyContent: "center",
              flexDirection: "row"
            }}
            onPress={async () => {
              try {
                if (this.state.isLogin) {
                  await UserService.reset();
                  NavigationService.reset("oauthStack");
                  return;
                }
                NavigationService.navigate("login");
              } catch (error) {}
            }}
          >
            <IText
              style={{
                fontSize: 14,
                color: colors.white,
                padding: 12,
                fontFamily: "bold",
                flex: 1
              }}
            >
              {oauthText}
            </IText>
            <IText
              style={{
                fontSize: 14,
                color: colors.white,
                padding: 12,
                fontFamily: "bold"
              }}
            >
              Version 1.0.0
            </IText>
          </ITouch>
        </IView>
      </IView>
    );
  };

  _renderItem = item => {
    return (
      <ITouch
        style={{ paddingVertical: 16 }}
        onPress={() => {
          this._route(item.route);
        }}
      >
        <IView
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: 50,
            marginRight: 12
          }}
        >
          <IView
            style={{
              width: 4,
              height: 4,
              borderRadius: 4,
              backgroundColor: colors.white
            }}
          ></IView>

          <IText
            style={{
              color: colors.white,
              fontFamily: "medium",
              fontSize: 16,
              marginLeft: 10
            }}
          >
            {item.name}
          </IText>
        </IView>
      </ITouch>
    );
  };

  _route = route => {
    this.props.navigation.closeDrawer();
    switch (route) {
      case this.ROUTE.EVENT:
        break;
      case this.ROUTE.HISTORY:
        NavigationService.navigate("historyOrder");
        break;
      case this.ROUTE.FOLLOW:
        NavigationService.navigate("followOrder");
        break;
    }
  };
}

export default MenuScreen;
