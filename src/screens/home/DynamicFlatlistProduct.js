import React, { Component } from 'react'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
	IFLatList,
} from '../../components'
import { colors, images, strings } from '../../../assets'
import AutoHeightImage from 'react-native-scalable-image'
import { withNavigation } from 'react-navigation'
import BaseScreen from '../BaseScreen'
import { NavigationService, APIService, UserService } from '../../services'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import RowProduct from '../row/RowProduct'

class DynamicFlatlistProduct extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isRefresh: false,
			isLogin: null,
		}
		this.listProduct = this.props.listProduct || []
		this.isCheckSave = this.props.isCheckSave || false
		this.noLike = this.props.noLike || false
		this.list1 = []
		this.list2 = []
		this.handle2ListProduct()
		this.checkLogin()
	}

	checkLogin = async () => {
		try {
			const isLogin = await UserService.isLogin()
			this.setState({
				isLogin: isLogin,
			})
		} catch (error) {
			console.log(error)
		}
	}

	refreshSaveList(hidden) {
		this.isCheckSave = hidden
	}
	refreshProductList(list) {
		this.listProduct = list
		this.handle2ListProduct()
		this.setState({ isRefresh: true })
	}
	handle2ListProduct() {
		this.list1 = []
		this.list2 = []
		var ratio1 = 0.0
		var ratio2 = 0.0
		this.listProduct.map((item, index) => {
			var itemWidth = 1
			var itemHeight = 1
			if (item.image_width !== 0) {
				itemWidth = item.image_width
			}
			if (item.image_height !== 0) {
				itemHeight = item.image_height
			}

			if (ratio1 > ratio2) {
				this.list2.push(item)
				ratio2 = ratio2 + itemHeight / itemWidth
			} else {
				this.list1.push(item)
				ratio1 = ratio1 + itemHeight / itemWidth
			}
		})
	}
	render() {
		return this.state.isLogin != null ? (
			<IView style={{ flexDirection: 'row' }}>
				<IFLatList
					style={{
						flex: 1,
					}}
					contentContainerStyle={{ paddingLeft: 18 }}
					data={this.list1}
					renderItem={({ item, index }) => (
						<RowProduct
							item={item}
							index={index}
							isCheck={this.isCheckSave}
							navigation={this.props.navigation}
							isLogin={this.state.isLogin}
							isLike={!this.noLike}></RowProduct>
					)}
					extraData={this.state.isRefresh}
					keyExtractor={(item, index) => index.toString()}
					initialNumToRender={1}
					maxToRenderPerBatch={1}
					updateCellsBatchingPeriod={1}
					scrollEnabled={false}
				/>

				<IView style={{ width: 18 }} />
				<IFLatList
					contentContainerStyle={{ paddingRight: 18 }}
					style={{
						flex: 1,
					}}
					data={this.list2}
					renderItem={({ item, index }) => (
						<RowProduct
							item={item}
							index={index}
							isCheck={this.isCheckSave}
							navigation={this.props.navigation}
							isLogin={this.state.isLogin}
							isLike={!this.noLike}></RowProduct>
					)}
					extraData={this.state.isRefresh}
					keyExtractor={(item, index) => index.toString()}
					initialNumToRender={1}
					maxToRenderPerBatch={1}
					updateCellsBatchingPeriod={1}
					scrollEnabled={false}
				/>
			</IView>
		) : null
	}
}

export default DynamicFlatlistProduct
