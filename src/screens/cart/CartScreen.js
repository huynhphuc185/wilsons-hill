import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  ScrollView,
  FlatList,
  Keyboard,
  SafeAreaView,
  Modal
} from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch,
  ITextInput
} from "../../components";
import { colors, images } from "../../../assets";
import EventEmitter from "react-native-eventemitter";
import BaseScreen from "../BaseScreen";
import { APIService, NavigationService } from "../../services";
import { withNavigation } from "react-navigation";
import { priceFormat } from "../../utils";
import IFlatList from "../../components/IFLatList";
import AlertScreen from "../common/AlertScreen";

class CartScreen extends BaseScreen {
  constructor(props) {
    super(props);

    this.state = {
      dataCart: [],
      listItemCart: [],
      buttonBottom: true,
      isShowPopup: false,
      onOffBtnBottom: true,
      idItemChange: 0,
      quantityItemChange: 0,
      minQuantityItemChange: 0,
      oldQuantilyItemChange: 0,
      reloadItem: false,
      inCart: false,
      nameItemChange: ""
    };
    this.isRefresh = true;
  }
  componentDidMount() {
    EventEmitter.on("getCart", () => {
      this.isRefresh = true;
      this.getCart(false);
    });
    this.getCart();
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    // const { navigation } = this.props
    // this.focusListener = navigation.addListener('didFocus', () => {
    // 	if (this.isRefresh) {
    // 		this.getCart()
    // 		this.isRefresh = false
    // 	}
    // })
  }
  componentWillUnmount() {
    // this.focusListener.remove()
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    EventEmitter.removeListener("getCart", this.getCart);
  }

  _keyboardDidShow = () => {
    if (Platform.OS === "android") {
      this.setState({
        onOffBtnBottom: false
      });
    }
  };

  _keyboardDidHide = () => {
    if (Platform.OS === "android") {
      this.setState({
        onOffBtnBottom: true
      });
    }
    if (this.state.idItemChange == 0) {
      return;
    }
    if (this.state.minQuantityItemChange > this.state.quantityItemChange) {
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance(
          "Minimum required of " +
            this.state.nameItemChange +
            " is " +
            this.state.minQuantityItemChange,
          99999,
          () => {
            this.setState({
              quantityItemChange: this.state.oldQuantilyItemChange,
              reloadItem: !this.state.reloadItem
              // inCart: false
            });
            NavigationService.pop();
          }
        )
      );
      return;
    }
    this.updateProduct(this.state.idItemChange, this.state.quantityItemChange);
    console.log("CartScreen");
    // }
  };

  updateProduct(product_id, number) {
    APIService.updateProductCart(product_id, number).then(data => {
      if (data != null) {
        EventEmitter.emit("getCart");
        this.setState({
          idItemChange: 0
        });
        if (number === 0) {
          EventEmitter.emit("setNumberCart");
        }
      }
    });
  }

  getCart(isPro) {
    APIService.getCart(isPro).then(data => {
      if (data != null) {
        var data_ = data;
        data_.note = "";
        this.setState({
          dataCart: data_.cart,
          listItemCart: data_.cart.products,
          reloadItem: !this.state.reloadItem
        });
        if (data_.cart.products.length != 0) {
          this.setState({ buttonBottom: false });
        } else {
          this.setState({ buttonBottom: true });
        }
      }
    });
  }

  renderButtonBottom() {
    return (
      <IView
        style={{
          backgroundColor: "white",
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.5,
          elevation: 20,
          paddingHorizontal: 18
        }}
      >
        <ITouch
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            marginBottom: 29,
            marginTop: 13
          }}
          disabled={this.state.listItemCart.length === 0 ? true : false}
          onPress={() => {
            this.setState({ isShowPopup: true });
          }}
        >
          <IView
            style={{
              borderColor: "#1C1C1C",
              borderBottomWidth: 1,
              paddingBottom: 3
            }}
          >
            <IText
              style={{
                fontSize: 16,
                fontFamily: "normal",
                letterSpacing: 1.6,
                lineHeight: 18
              }}
            >
              Custom Request
            </IText>
          </IView>
        </ITouch>
        <IView
          style={{
            flexDirection: "row",
            marginBottom: 21,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <IText
            style={{
              fontSize: 14,
              letterSpacing: 1.6,
              fontWeight: "500"
            }}
          >
            Total Payment
          </IText>
          <IText
            style={{
              fontSize: 16,
              letterSpacing: 1.6
            }}
            isSo={true}
          >
            {priceFormat(this.state.dataCart.total_payment)}
          </IText>
        </IView>
        <ITouch
          style={{
            height: 40,
            width: "100%",
            backgroundColor: "black",
            justifyContent: "center",
			alignItems: "center",
			marginBottom:15
          }}
          disabled={this.state.buttonBottom}
          onPress={() => {
            this.props.navigation.push("ShippingDetaillScreen", {
              dataCart: this.state.dataCart
            });
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              letterSpacing: 1.6,
              color: "white"
            }}
          >
            PROCEED TO CHECKOUT
          </IText>
        </ITouch>
      </IView>
    );
  }

  renderPopUpRequest() {
    return (
      <Modal
        visible={this.state.isShowPopup}
        transparent={true}
        animationType="fade"
      >
        <IView
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "#00000080"
          }}
        >
          <IView
            style={{
              flex: 1,
              position: "absolute",
              width: 344,
              paddingTop: 26,
              paddingBottom: 33,
              backgroundColor: "white",
              borderRadius: 10,
              alignItems: "center"
            }}
          >
            <IText
              style={{
                fontSize: 16,
                fontWeight: "500",
                letterSpacing: 1.6,
                paddingBottom: 10,
                lineHeight: 19
              }}
            >
              Custom Request
            </IText>
            <IView style={{ paddingHorizontal: 20 }}>
              <IText
                style={{
                  fontSize: 12,
                  fontWeight: "300",
                  lineHeight: 18,
                  textAlign: "justify"
                }}
              >
                Please let us know if you would like to have any amendment in
                your order such as measurements or price. Our team will get back
                to you for further assistance. Thank you!
              </IText>

              <IView
                style={{
                  height: 100,
                  borderWidth: 0.5,
                  justifyContent: "center",
                  marginTop: 8
                }}
              >
                <ITextInput
                  multiline={true}
                  style={{
                    flex: 1,
                    fontSize: 14,
                    margin: 10,

                    color: "black",
                    textAlignVertical: "top"
                  }}
                  onChangeText={value => {
                    this.state.dataCart.note = value;
                  }}
                ></ITextInput>
              </IView>

              <IView
                style={{
                  flexDirection: "row",
                  paddingTop: 24
                }}
              >
                <ITouch
                  style={{
                    height: 40,
                    width: "50%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#9C9C9C",
                    flex: 1
                  }}
                  onPress={() => {
                    this.setState({ isShowPopup: false });
                  }}
                >
                  <IText
                    style={{
                      fontSize: 16,
                      color: colors.white,
                      fontWeight: "500"
                    }}
                  >
                    CANCEL
                  </IText>
                </ITouch>
                <IView style={{ width: 15 }}></IView>
                <ITouch
                  style={{
                    height: 40,
                    width: "50%",

                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: colors.black,

                    flex: 1
                  }}
                  onPress={() => {
                    this.setState({ isShowPopup: false });
                  }}
                >
                  <IText
                    style={{
                      fontSize: 16,
                      color: colors.white,
                      fontWeight: "500"
                    }}
                  >
                    CONFIRM
                  </IText>
                </ITouch>
              </IView>
            </IView>
          </IView>
        </IView>
      </Modal>
    );
  }
  renderPolicy = () => {
    return (
      <IView
        style={{
          paddingHorizontal: 35,
          paddingVertical: 4,
          backgroundColor: colors.white
        }}
      >
        <IText
          style={{
            fontSize: 10,
            lineHeight: 18,
            fontWeight: "500"
          }}
        >
          For your consideration:
        </IText>
        <IText
          style={{
            fontSize: 10,
            lineHeight: 18,
            fontWeight: "300"
          }}
        >
          Quantity discount for each item:{"\n"} 30-60 pcs: Original price
          {"\n"} 61-70 pcs: 5% discount{"\n"} 71-200 pcs: 8% discount
        </IText>
      </IView>
    );
  };

  renderListProduct = () => {
    return (
      <IFlatList
        style={{ paddingHorizontal: 18, backgroundColor: colors.bg }}
        data={this.state.listItemCart}
        renderItem={({ item, index }) => (
          <RowCartProduct
            data={item}
            index={index}
            reloadItem={this.state.reloadItem}
            callback={type => {
              if (type === "tru") {
                var num = item.quantity - 1;
                this.updateProduct(item.id, num);
              } else if (type === "cong") {
                var num = item.quantity + 1;
                this.updateProduct(item.id, num);
              } else if (type === "xoa") {
                this.updateProduct(item.id, 0);
              }
            }}
            numberQuantity={value => {
              this.setState({
                idItemChange: item.id,
                quantityItemChange: value,
                minQuantityItemChange: item.min_quantity,
                oldQuantilyItemChange: item.quantity,
                nameItemChange: item.name
              });
              // if (this.state.inCart == false) {
              //   this.setState({
              //     inCart: true
              //   });
              // }
            }}
          ></RowCartProduct>
        )}
        extraData={this.state.reloadItem}
        keyExtractor={(item, index) => index + ""}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => {
          return (
            <IView
              style={{
                height: 1,
                backgroundColor: "#C5C5C5"
              }}
            ></IView>
          );
        }}
        ListEmptyComponent={() => (
          <IView
            style={{
              marginVertical: 14
            }}
          >
            <IText
              style={{
                fontSize: 14,
                // fontFamily: 'bold',
                textAlign: "center"
              }}
            >
              No orders yet
            </IText>
          </IView>
        )}
      />
    );
  };
  render() {
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar
          title={"My Cart"}
          notSearch="true"
          isBackFromDetailScreen={this.props.navigation.getParam("isBack")}
        />
        <ScrollView style={{ backgroundColor: colors.bg }}>
          {this.renderPopUpRequest()}
          {this.renderPolicy()}
          {this.renderListProduct()}
        </ScrollView>
        {this.state.onOffBtnBottom ? this.renderButtonBottom() : null}
      </IView>
    );
  }
}
export default withNavigation(CartScreen);

class RowCartProduct extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      isSelect: false,
      isShow: false,
      disabledButton: false,
      reloadItem: false,
      number: this.props.data.quantity
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.reloadItem != this.state.reloadItem) {
      this.setState({
        reloadItem: newProps.reloadItem,
        number: newProps.data.quantity
      });
    }
  }

  render() {
    const { data } = this.props;
    return (
      <IView style={{ flex: 1, height: 157, paddingTop: 15 }}>
        <IView
          style={{
            flexDirection: "row",
            height: 86
          }}
        >
          <IView
            style={{
              padding: 5,
              width: 92,
              height: 86,
              backgroundColor: "white",
              borderRadius: 10
            }}
          >
            <IFastImage
              source={{ uri: data.thumb }}
              style={{ flex: 1 }}
              resizeMode="contain"
            ></IFastImage>
          </IView>
          <IView style={{ flex: 1, marginLeft: 10 }}>
            <IView
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              <IText
                numberOfLines={1}
                style={{
                  fontSize: 10,
                  // fontFamily: "medium",
                  fontWeight: "500",
                  letterSpacing: 1.6,
                  paddingRight: 10
                }}
              >
                [{data.collection_name}] Collection
              </IText>
              <ITouch
                onPress={() => {
                  this.props.callback("xoa");
                }}
              >
                <IFastImage
                  source={images.ic_delete}
                  style={{
                    width: 12,
                    height: 13
                  }}
                ></IFastImage>
              </ITouch>
            </IView>
            <IView
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 7
              }}
            >
              <IText
                style={{
                  fontSize: 14,
                  fontWeight: "500",
                  letterSpacing: 1.6
                }}
                numberOfLines={1}
              >
                {data.name}
              </IText>
            </IView>
            <IView
              style={{
                flexDirection: "row",
                flex: 1,
                justifyContent: "space-between",
                alignItems: "flex-end"
                // marginTop: 7
              }}
            >
              <IView>
                <IText
                  style={{
                    fontSize: 14,
                    letterSpacing: 1.6
                  }}
                  isSo={true}
                >
                  {priceFormat(data.price)}
                </IText>
              </IView>
              <IView
                style={{
                  borderWidth: 1,
                  borderColor: "#E0E1E2",
                  alignItems: "center",
                  flexDirection: "row",
                  width: 128,
                  height: 40
                }}
              >
                <ITouch
                  style={{ paddingHorizontal: 12 }}
                  disabled={data.quantity === data.min_quantity ? true : false}
                  onPress={() => {
                    this.props.callback("tru");
                  }}
                >
                  <IFastImage
                    source={images.p_tru}
                    style={{
                      width: 14,
                      height: 2
                    }}
                  ></IFastImage>
                </ITouch>
                <ITextInput
                  style={{
                    flex: 1,
                    textAlign: "center",
                    fontSize: 16,
                    fontWeight: "500"
                  }}
                  keyboardType="numeric"
                  isSo={true}
                  onChangeText={text => {
                    this.setState({
                      number: Number(text)
                    });
                    this.props.numberQuantity(Number(text));
                  }}
                >
                  {this.state.number}
                </ITextInput>
                <ITouch
                  style={{ paddingHorizontal: 12 }}
                  onPress={() => {
                    this.props.callback("cong");
                  }}
                >
                  <IFastImage
                    source={images.p_cong}
                    style={{
                      width: 14,
                      height: 14
                    }}
                  ></IFastImage>
                </ITouch>
              </IView>
            </IView>
          </IView>
        </IView>
        <IView
          style={{
            backgroundColor: "white",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 10,
            height: 25
          }}
        >
          <IText
            style={{
              fontSize: 12,
              letterSpacing: 1.6,
              fontWeight: "300"
            }}
          >
            Minimum order quantily:{" "}
            <IText style={{}} isSo={true}>
              {data.min_quantity}
            </IText>
          </IText>
        </IView>
      </IView>
    );
  }
}
