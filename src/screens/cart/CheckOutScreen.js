import React, { Component } from 'react'
import {
	View,
	Text,
	StyleSheet,
	AsyncStorage,
	Platform,
	ScrollView,
	Modal,
} from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
	ITextInput,
} from '../../components'
import { colors, images } from '../../../assets'
import { priceFormat } from '../../utils'
import { APIService, NavigationService } from '../../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BaseScreen from '../BaseScreen'
import SafeAreaView from 'react-native-safe-area-view'
import RowCheckOut from '../row/RowCheckOut'

class CheckOutScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			success: false,
		}
		this.isFillLater = this.props.navigation.getParam('isFillLater')
		this.dataCart = this.props.navigation.getParam('dataCart')
		this.listItemCart = this.dataCart.products
	}

	componentDidMount() {
		//this.getCart()
	}

	getCart() {
		APIService.getCart().then(data => {
			if (data != null) {
				this.setState({
					dataCart: data.cart,
					listItemCart: data.cart.products,
				})
				console.log(data.cart)
			}
		})
	}

	confirmCart() {
		APIService.confirmCart(
			this.dataCart.delivery_address_id,
			this.dataCart.forwarder,
			this.dataCart.port_of_discharge,
			this.dataCart.note,
		).then(data => {
			if (data != null) {
				this.setState({
					success: true,
				})
			}
		})
	}

	renderPopupSuccess() {
		return (
			<Modal
				visible={this.state.success}
				transparent={true}
				animationType='fade'>
				<IView
					style={{
						width: '100%',
						height: '100%',
						position: 'absolute',
						alignItems: 'center',
						justifyContent: 'center',
						flex: 1,
						backgroundColor: '#00000080',
						// backgroundColor: colors.white
					}}>
					<IView
						style={{
							flex: 1,
							position: 'absolute',
							width: 344,
							height: 185,
							backgroundColor: 'white',
							borderRadius: 10,
							alignItems: 'center',
							paddingTop: 26,
							//   justifyContent: "center"
						}}>
						<IText
							style={{
								fontSize: 16,
								fontWeight: '500',
								letterSpacing: 1.6,
							}}>
							Order Placed!
						</IText>
						<IText
							style={{
								fontSize: 14,
								fontWeight: '500',
								letterSpacing: 1.6,
								marginTop: 27,
							}}>
							Thank you for choosing
						</IText>
						<IFastImage
							source={images.home_logo}
							style={{
								width: 207,
								height: 20,
								marginTop: 10,
							}}></IFastImage>
						<IView
							style={{
								flex: 1,
								justifyContent: 'flex-end',
							}}>
							<ITouch
								style={{
									width: 166,
									height: 29,
									backgroundColor: 'black',
									borderTopLeftRadius: 11,
									borderTopRightRadius: 11,
									alignItems: 'center',
									justifyContent: 'center',
								}}
								onPress={() => {
									this.setState({
										success: false,
									})
									NavigationService.reset('mainStack')
								}}>
								<IText
									style={{
										fontSize: 10,
										letterSpacing: 1.6,
										color: 'white',
									}}>
									Go back to homepage
								</IText>
							</ITouch>
						</IView>
					</IView>
				</IView>
			</Modal>
		)
	}

	renderButtonBottom() {
		return (
			<IView
				style={{
					
					backgroundColor: 'white',
					shadowOffset: { width: 0, height: -1 },
					shadowOpacity: 0.5,
					elevation: 20,
					flexDirection: 'row',
					paddingVertical: 18,
					paddingHorizontal: 18,
				}}>
				<ITouch
					style={{
						height: 40,
						width: '100%',
						backgroundColor: 'black',
						justifyContent: 'center',
						alignItems: 'center',
					}}
					onPress={() => {
						this.confirmCart()
					}}>
					<IText
						style={{
							fontSize: 16,
							fontFamily: 'normal',
							letterSpacing: 1.6,
							color: 'white',
						}}>
						PLACE ORDER
					</IText>
				</ITouch>
			</IView>
		)
	}

	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar
					title={'Check Out'}
					notSearch='true'
					
				/>
				<KeyboardAwareScrollView style={{ paddingHorizontal: 18 }}>
					<IText
						style={{
							fontSize: 14,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						Order Summary
					</IText>
					<IFLatList
						style={{}}
						data={this.listItemCart}
						renderItem={({ item, index }) => (
							<RowCheckOut item={item}></RowCheckOut>
						)}
						// extraData={this.state.cartList}
						keyExtractor={(item, index) => item}
						showsVerticalScrollIndicator={false}
						scrollEnabled={false}
					/>
					<IView
						style={{
							flexDirection: 'row',
							paddingTop: 15,
							justifyContent: 'space-between',
							alignItems: 'center',
						}}>
						<IText
							style={{
								fontSize: 16,
								letterSpacing: 1.6,
								fontWeight: '500',
							}}>
							Total Payment
						</IText>
						<IText
							style={{
								fontSize: 20,
								letterSpacing: 1.6,
								fontWeight: '600',
							}}
							isSo={true}>
							{priceFormat(this.dataCart.total_payment)}
						</IText>
					</IView>
					<IView
						style={{
							flexDirection: 'row',

							justifyContent: 'space-between',
							alignItems: 'center',
						}}>
						<IText
							style={{
								fontSize: 14,
								letterSpacing: 1.6,
								fontWeight: '500',
								paddingVertical: 10,
							}}>
							First payment (deposit)
						</IText>
						<IText
							style={{
								fontSize: 14,
								letterSpacing: 1.6,
							}}
							isSo={true}>
							{priceFormat(this.dataCart.deposit)}
						</IText>
					</IView>

					<IView
						style={{
							flexDirection: 'row',
							paddingBottom: 15,
							justifyContent: 'space-between',
							alignItems: 'center',
						}}>
						<IText
							style={{
								fontSize: 14,
								letterSpacing: 1.6,
								fontWeight: '500',
							}}>
							Final payment
						</IText>
						<IText
							style={{
								fontSize: 14,
								letterSpacing: 1.6,
							}}
							isSo={true}>
							{priceFormat(this.dataCart.final_payment)}
						</IText>
					</IView>
					<IView
						style={{
							height: 1,
							backgroundColor: '#C5C5C5',
						}}></IView>
					<IView
						style={{
							flexDirection: 'row',
							paddingTop: 15,
							justifyContent: 'space-between',
							alignItems: 'center',
						}}>
						<IText
							style={{
								fontSize: 16,
								letterSpacing: 1.6,
								fontWeight: '500',
							}}>
							Total CBM
						</IText>
						<IText
							style={{
								fontSize: 16,
								letterSpacing: 1.6,
							}}
							isSo={true}>
							{this.dataCart.total_cubic_meter}
						</IText>
					</IView>
					<IView style={{ marginTop: 11, marginBottom: 15 }}>
						<IText
							style={{
								fontSize: 14,
								fontWeight: '500',
								letterSpacing: 1.6,
							}}>
							Number of containers:
						</IText>
						<IText
							style={{
								fontSize: 14,
								fontWeight: '300',
								letterSpacing: 1.6,
								marginTop: 5,
								marginLeft: 13,
							}}>
							{this.dataCart.quantily}
						</IText>
					</IView>
					{this.isFillLater === false ? (
						<IView>
							<IView
								style={{
									height: 1,
									backgroundColor: '#C5C5C5',
								}}></IView>

							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
									marginTop: 15,
								}}>
								Forwarder:
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '300',
									letterSpacing: 1.6,
									marginTop: 7,
									marginLeft: 13,
								}}>
								{this.dataCart.forwarder}
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
									marginTop: 7,
								}}>
								ETD:
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '300',
									letterSpacing: 1.6,
									marginTop: 7,
									marginLeft: 13,
								}}>
								{this.dataCart.etd}
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
									marginTop: 7,
								}}>
								Port of loading:
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '300',
									letterSpacing: 1.6,
									marginTop: 7,
									marginLeft: 13,
								}}>
								{this.dataCart.port_of_loading}
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
									marginTop: 7,
								}}>
								Port of discharge:
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '300',
									letterSpacing: 1.6,
									marginVertical: 7,
									marginLeft: 13,
								}}>
								{this.dataCart.port_of_discharge}
							</IText>
						</IView>
					) : null}
				</KeyboardAwareScrollView>
				{this.renderButtonBottom()}
				{this.renderPopupSuccess()}
			</IView>
		)
	}
}

export default CheckOutScreen
