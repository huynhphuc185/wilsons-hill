import React, { Component } from 'react'
import {
	View,
	Text,
	StyleSheet,
	AsyncStorage,
	Platform,
	ScrollView,
	Modal,
} from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
	ITextInput,
} from '../../components'
import { colors, images } from '../../../assets'
import { priceFormat } from '../../utils'
import { APIService, NavigationService } from '../../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BaseScreen from '../BaseScreen'
import SafeAreaView from 'react-native-safe-area-view'

class ShippingDetaillScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			buttonBottom: true,
		}
		this.dataCart = this.props.navigation.getParam('dataCart')
		this.dataCart.forwarder = ''
		this.dataCart.port_of_discharge = ''
	}

	componentDidMount() {}

	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar
					title={'Shipping Details'}
					notSearch='true'
				
				/>
				<KeyboardAwareScrollView style={{ paddingHorizontal: 18 }}>
					<IText
						style={{
							fontSize: 14,
							fontWeight: '300',
							letterSpacing: 1.6,
							marginVertical: 10,
						}}>
						Forwarder:
					</IText>

					<IView
						style={{
							height: 94,
							borderWidth: 1,
							backgroundColor: 'white',

							borderColor: '#1C1C1C',
						}}>
						<ITextInput
							multiline={true}
							style={{
								fontSize: 14,
								color: 'black',
								textAlignVertical: 'top',
								margin: 10,
								flex: 1,
							}}
							placeholder={
								'Please fill in forwarder’s infomation'
							}
							onChangeText={value => {
								this.dataCart.forwarder = value
								if (
									this.dataCart.forwarder.length !== 0 &&
									this.dataCart.port_of_discharge.length !== 0
								) {
									this.setState({
										buttonBottom: false,
									})
								} else {
									this.setState({
										buttonBottom: true,
									})
								}
							}}
						/>
					</IView>

					<IText
						style={{
							fontSize: 14,
							fontWeight: '300',
							letterSpacing: 1.6,
							marginVertical: 10,
						}}>
						Port of discharge
					</IText>
					<IView
						style={{
							height: 40,
							borderWidth: 1,
							backgroundColor: 'white',
							borderColor: '#1C1C1C',
							justifyContent: 'center',
						}}>
						<ITextInput
							style={{
								fontSize: 14,
								color: 'black',
								height: '100%',
								margin: 10,
							}}
							placeholder={''}
							onChangeText={value => {
								this.dataCart.port_of_discharge = value
								if (
									this.dataCart.forwarder.length !== 0 &&
									this.dataCart.port_of_discharge.length !== 0
								) {
									this.setState({
										buttonBottom: false,
									})
								} else {
									this.setState({
										buttonBottom: true,
									})
								}
							}}
						/>
					</IView>
				</KeyboardAwareScrollView>
				<IView
					style={{
						backgroundColor: colors.white,
						padding: 18,
						shadowOffset: { width: 0, height: -1 },
						shadowOpacity: 0.5,
						elevation: 20,
					}}>
					<ITouch
						style={{
							height: 40,
							marginBottom: 12,
							backgroundColor: 'black',
							justifyContent: 'center',
							alignItems: 'center',
						}}
						disabled={this.state.buttonBottom}
						onPress={() => {
							NavigationService.navigate('CheckOutScreen', {
								dataCart: this.dataCart,
								isFillLater: false,
							})
						}}>
						<IText
							style={{
								fontSize: 16,
								fontFamily: 'normal',
								letterSpacing: 1.6,
								color: 'white',
							}}>
							CHECK OUT
						</IText>
					</ITouch>
					<ITouch
						style={{
							width: '100%',
							justifyContent: 'center',
							alignItems: 'center',
							paddingBottom: 8,
						}}
						onPress={() => {
							NavigationService.navigate('CheckOutScreen', {
								dataCart: this.dataCart,
								isFillLater: true,
							})
						}}>
						<IText
							style={{
								fontSize: 16,
								fontFamily: 'normal',
								letterSpacing: 1.6,
								lineHeight: 18,
								textDecorationLine: 'underline',
								
							}}>
							Fill in later
						</IText>
					</ITouch>
				</IView>
			</IView>
		)
	}
}

export default ShippingDetaillScreen
