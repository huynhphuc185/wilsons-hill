import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  ScrollView,
  TouchableHighlight,
  Animated
} from "react-native";
import FastImage from "react-native-fast-image";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  ITouch
} from "../../components";
import { APIService, UserService, NavigationService } from "../../services";
import BaseScreen from "../BaseScreen";
import { throwStatement } from "@babel/types";
import { images, colors } from "../../../assets";
import ICombobox from "../../components/ICombobox";
import { constraintLayout } from "../../components/ILayoutProps";
import DynamicFlatlistProduct from "../home/DynamicFlatlistProduct";
import AlertScreen from "../common/AlertScreen";
import IScrollView from "../../components/IScrollView";
import EventEmitter from "react-native-eventemitter";
import AutoHeightImage from "react-native-scalable-image";
class CollectionDetailScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(2),
      bounceValueTop: new Animated.Value(72),
      textSelect: "Select",
      productList: null,
      dataCollection: {},
      scrollY: new Animated.Value(0),
      updateButton: true,
      marginBottomView: false
    };
    this.oneTimeLoadMore = true;
    this.indexSort = 0;
    this.page = 1;
    this.collection_id = this.props.navigation.getParam("collection_id");
    this.isHidden = true;
  }

  componentDidMount() {
    APIService.getListSortDetailCollection(false).then(data => {
      if (data != null) {
        this.setState({
          listSort: data.sort_collection_types
        });
        const getListProductCollection = APIService.getListProductCollection(
          this.collection_id,
          data.sort_collection_types[this.indexSort].id,
          1
        );
        const getDetailCollection = APIService.getDetailCollection(
          this.collection_id
        );
        Promise.all([getListProductCollection, getDetailCollection]).then(
          result => {
            result[0].product.map((item, index) => {
              item.chose = false;
            });
            this.setState({
              productList: result[0].product,
              dataCollection: result[1].collection
            });
          }
        );
      }
      console.log(data);
    });
    EventEmitter.on("onOffBtn", this.updateButton);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("onOffBtn", this.updateButton);
  }

  updateButton = () => {
    const result = this.state.productList.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    if (result.length != 0) {
      this.setState({
        updateButton: false
      });
    } else {
      this.setState({
        updateButton: true
      });
    }
  };
  addManyToCart = products => {
    APIService.addManyToCart(products).then(data => {
      if (data != null) {
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Add products successfully!", 9999, () => {
            this._toggleSubview();
            NavigationService.pop();
          })
        );
        EventEmitter.emit("setNumberCart");
        EventEmitter.emit("getCart");
      }
    });
  };

  filterChose() {
    const result = this.state.productList.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    var listChose = [];
    result.map((item, index) => {
      listChose.push({
        product_id: item.id,
        number: item.min_quantity
      });
    });
    console.log(listChose);
    this.addManyToCart(listChose);
  }

  _toggleSubview() {
    this.setState({
      textSelect: !this.isHidden ? "Select" : "Cancel",
      marginBottomView: !this.isHidden ? false : true
    });

    var toValue = 2;
    var toValue2 = 72;

    if (this.isHidden) {
      toValue = 72;
      toValue2 = 2;
    }

    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(this.state.bounceValue, {
      toValue: toValue,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    Animated.spring(this.state.bounceValueTop, {
      toValue: toValue2,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    this.isHidden = !this.isHidden;
    this._flatList.refreshSaveList(!this.isHidden);
  }

  addCollectionToCart = async () => {
    await APIService.addCollectionToCart([this.collection_id]);
    EventEmitter.emit("setNumberCart");
    EventEmitter.emit("getCart");
    NavigationService.navigate(
      "alert",
      AlertScreen.newInstance("Add collection successfully!")
    );
  };

  _getListProduct = async () => {
    const sort_id = this.state.listSort[this.indexSort].id;
    const page = this.page;
    const result = await APIService.getListProductCollection(
      this.collection_id,
      sort_id,
      page
    );
    this.setState({
      productList: result.product
    });
    this._flatList.refreshProductList(this.state.productList);
  };

  loadMore = async () => {
    const sort_id = this.state.listSort[this.indexSort].id;
    const page = this.page;
    const result = await APIService.getListProductCollection(
      this.collection_id,
      sort_id,
      page
    );

    this.state.productList = this.state.productList.concat(result.product);
    this.setState({
      productList: this.state.productList
    });
    this._flatList.refreshProductList(this.state.productList);
  };

  saveCollection = async () => {
    const saveCollection = await APIService.saveCollection(
      [this.collection_id],
      1
    );
    if (saveCollection.fail_list.length === 0) {
      EventEmitter.emit("update_catalogue");
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance("Save Collection successfully!")
      );
    }
  };

  renderButtonTop() {
    return (
      <Animated.View
        style={{
          height: this.state.bounceValueTop,
          backgroundColor: "#f2f1f1",
          flexDirection: "row"
        }}
      >
        <ITouch
          style={{
            height: 40,
            width: 94,
            backgroundColor: "black",
            marginLeft: 18,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 18
          }}
          onPress={() => {
            this.saveCollection();
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              letterSpacing: 1.6,
              color: "white"
            }}
          >
            SAVE
          </IText>
        </ITouch>
        <ITouch
          style={{
            height: 40,
            paddingHorizontal: 10,
            backgroundColor: "black",
            marginLeft: 18,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 18
          }}
          onPress={() => {
            this.addCollectionToCart();
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              color: "white"
            }}
          >
            ADD COLLECTION TO CART
          </IText>
        </ITouch>
      </Animated.View>
    );
  }

  renderButtonBottom() {
    return (
      <Animated.View
        style={{
          height: this.state.bounceValue,
          backgroundColor: "white",
          flexDirection: "row",
          bottom: 0,
          position: "absolute",
          justifyContent: "center",
          width: "100%",
          shadowOffset: { width: 0, height: -1 },
          shadowOpacity: 0.5,
          elevation: 20
        }}
      >
        <ITouch
          style={{
            backgroundColor: "black",
            justifyContent: "center",
            marginVertical: 16,
            height: 40
          }}
          disabled={this.state.updateButton}
          onPress={() => {
            this.filterChose();
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              letterSpacing: 1.6,
              color: "white",
              paddingHorizontal: 25
            }}
          >
            ADD SELECTED ITEMS TO CART
          </IText>
        </ITouch>
      </Animated.View>
    );
  }

  renderHeader() {
    return (
      <AutoHeightImage
        width={this.width}
        source={{ uri: this.state.dataCollection.image || "" }}
        style={{}}
      ></AutoHeightImage>
    );
  }

  getDataStringCombobox = listSort => {
    var listString = [];
    listSort.map((item, index) => {
      listString.push(item.name);
    });
    return listString;
  };

  renderSortBy = callBack => {
    return (
      <IView
        style={{
          flexDirection: "row",
          // justifyContent: "flex-end",
          alignItems: "center",
          paddingHorizontal: 18,
          paddingVertical: 16,
          backgroundColor: "#f2f1f1"
        }}
      >
        <IView
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <IText
            style={{
              fontSize: 14,
              color: colors.text.black,
              marginRight: 14
            }}
          >
            Sort by
          </IText>

          <IView
            style={{
              height: 40,
              width: 178,
              borderRadius: 10,
              backgroundColor: colors.white
            }}
          >
            <IView
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ICombobox
                widthItem={constraintLayout(178)}
                defaultValue={this.state.listSort[this.indexSort].name}
                listValue={this.getDataStringCombobox(this.state.listSort)}
                callBack={index => {
                  this.indexSort = index;
                  this.page = 1;
                  this._getListProduct();
                }}
              />
            </IView>
          </IView>
        </IView>
        <ITouch
          style={{ alignItems: "center" }}
          onPress={() => {
            this._toggleSubview();
          }}
        >
          <IText
            style={{
              fontSize: 14,
              color: colors.text.black
            }}
          >
            {this.state.textSelect}
          </IText>
        </ITouch>
      </IView>
    );
  };
  render() {
    // const scrollY = Animated.add(this.state.scrollY, 0)
    // const barTransparent = scrollY.interpolate({
    // 	inputRange: [0, constraintLayout(226)],
    // 	outputRange: [0, 1],
    // 	extrapolate: 'clamp',
    // })
    // const onScroll = Animated.event(
    // 	[
    // 		{
    // 			nativeEvent: {
    // 				contentOffset: { y: this.state.scrollY },
    // 			},
    // 		},
    // 	],
    // 	{ useNativeDriver: false },
    // )
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar
        //title={this.state.dataCollection.name}
        //animatedStyle={barTransparent}
        />
        {this.state.productList ? (
          <IView
            style={{
              flex: 1
              // position: 'absolute',
              // top: 0,
              // bottom: 0,
              // right: 0,
              // left: 0,
            }}
          >
            <IScrollView
              // onScroll={e => {
              // 	onScroll(e)
              // }}
              onBottom={() => {
                this.page++;
                this.loadMore();
              }}
            >
              {this.renderHeader()}
              {this.renderButtonTop()}
              {this.renderSortBy()}
              <IView style={{ backgroundColor: "#f2f1f1" }}>
                <DynamicFlatlistProduct
                  listProduct={this.state.productList}
                  isCheckSave={!this.isHidden}
                  ref={ref => {
                    this._flatList = ref;
                  }}
                ></DynamicFlatlistProduct>
              </IView>
              {this.state.marginBottomView ? (
                <IView style={{ height: 72 }}></IView>
              ) : null}
            </IScrollView>
            {this.renderButtonBottom()}
          </IView>
        ) : null}
      </IView>
    );
  }
}

export default CollectionDetailScreen;
