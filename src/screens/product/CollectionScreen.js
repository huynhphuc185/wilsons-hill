import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  FlatList
} from "react-native";
import BaseScreen from "../BaseScreen";
import { images, colors } from "../../../assets";
import ICombobox from "../../components/ICombobox";
import { IImage, IToolbar, IView, IFastImage, IText } from "../../components";
import { constraintLayout } from "../../components/ILayoutProps";
import ModalDropdown from "../../components/ModalDropdown";
import RowCollection from "../row/RowCollection";
import { APIService } from "../../services";
import IFlatList from "../../components/IFLatList";
import IScrollView from "../../components/IScrollView";

class CollectionScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      roomList: ["Room 1", "Room 2", "Room 3", "Room 4"],
      rangeList: ["Price Range", "A-Z Range", "Z-A Range"],
      colectionList: []
    };
    this.page = 1;
  }
  componentDidMount() {
    this.getAllCollection();
  }

  getAllCollection() {
    APIService.getAllCollection(this.page).then(data => {
      if (data != null) {
        this.setState({
          colectionList: data.collection
        });
      }
    });
  }
  loadMore() {
    APIService.getAllCollection(this.page, false).then(data => {
      if (data != null) {
        this.state.colectionList = this.state.colectionList.concat(
          data.collection
        );
        this.setState({ colectionList: this.state.colectionList });
      }
    });
  }
  renderDropDown(list) {
    return (
      <IView
        style={{
          height: 40,
          width: 164,
          borderRadius: 10,
          marginLeft: 18,
          backgroundColor: colors.white
        }}
      >
        <IView
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ICombobox
            widthItem={constraintLayout(164)}
            defaultValue={list[0]}
            listValue={list}
            callBack={index => {}}
          />
        </IView>
      </IView>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={"Collections"} />
        {/* <IView
					style={{
						backgroundColor: colors.bg,
						flexDirection: 'row',
					}}>
					{this.renderDropDown(this.state.roomList)}
					{this.renderDropDown(this.state.rangeList)}
				</IView> */}
        <IScrollView
          style={{ marginTop: 15 }}
          onBottom={() => {
            this.page++;
            this.loadMore();
          }}
        >
          <IFlatList
          style={{flex:1}}
            data={this.state.colectionList}
            renderItem={({ item, index }) => (
              <RowCollection data={item} index={index}></RowCollection>
            )}
            extraData={this.state.colectionList}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => (
              <IView
                style={{
                  padding: 32,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <IText
                  style={{
                    color: colors.gray._400,
                    fontSize: 16
                  }}
                >
                  No items
                </IText>
              </IView>
            )}
          />
        </IScrollView>
      </View>
    );
  }
}

export default CollectionScreen;
