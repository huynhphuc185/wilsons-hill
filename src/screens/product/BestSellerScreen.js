import React, { Component } from 'react'
import {
	View,
	Text,
	StyleSheet,
	AsyncStorage,
	Platform,
	ScrollView,
} from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	ITouch,
} from '../../components'
import { constraintLayout } from '../../components/ILayoutProps'
import { images, colors } from '../../../assets'
import ICombobox from '../../components/ICombobox'
import BaseScreen from '../BaseScreen'
import RowCollection from '../row/RowCollection'
import DynamicFlatlistProduct from '../home/DynamicFlatlistProduct'
import { APIService, NavigationService } from '../../services'
import AlertScreen from '../common/AlertScreen'
import IBanner from '../home/IBanner'
import Carousel, { Pagination } from 'react-native-snap-carousel'
class BestSellerScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isRefesh: false,
		}
		this.dataCollection = []
		this.listProducts = []
	}
	async componentDidMount() {
		let data = await APIService.getListBestSeller()
		console.log(data)

		this.dataCollection = data.best_collection
		this.listProducts = data.best_seller
		this.setState({ isRefesh: true })
	}
	renderDropDown(list) {
		return (
			<IView
				style={{
					height: 40,
					width: 164,
					borderRadius: 10,
					marginLeft: 18,
					backgroundColor: colors.white,
				}}>
				<IView
					style={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center',
					}}>
					<ICombobox
						widthItem={constraintLayout(164)}
						defaultValue={list[0]}
						listValue={list}
						callBack={index => {}}
					/>
				</IView>
			</IView>
		)
	}

	renderTitle = (text, callBack) => {
		return (
			<IView
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					paddingHorizontal: 18,
					marginVertical: 12,
					height: 23,
				}}>
				<IText
					style={{
						fontSize: 14,
						color: colors.text.black,
						fontWeight: '500',
					}}>
					{text}
				</IText>
				{callBack != null ? (
					<ITouch
						style={{
							alignItems: 'center',
							justifyContent: 'center',
							flexDirection: 'row',
						}}
						onPress={() => {
							callBack()
						}}>
						<IText
							style={{
								fontSize: 14,
								marginRight: 11,
								color: colors.text.black,
							}}>
							View all
						</IText>
						<IFastImage
							source={images.vector}
							style={{ width: 16, height: 9 }}></IFastImage>
					</ITouch>
				) : null}
			</IView>
		)
	}
	renderCollectionBanner = () => {
		return (
			<IView>
				{this.renderTitle('Best Sellers: Collection')}
				<RowCollection data={this.dataCollection[0]}></RowCollection>
			</IView>
		)
	}
	render() {
		return (
			<View style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar title={'Best Sellers'} />
				{this.state.isRefesh ? (
					<ScrollView style={{ backgroundColor: colors.bg }}>
						{/* <IView
              style={{
                backgroundColor: colors.bg,
                flexDirection: "row"
              }}
            >
              {this.renderDropDown(["Room 1", "Room 2", "Room 3", "Room 4"])}
              {this.renderDropDown(["Price Range", "A-Z Range", "Z-A Range"])}
            </IView> */}
            {this.dataCollection.length !== 0 ? this.renderCollectionBanner(): null}
						
						{this.renderTitle('Best Sellers : Item')}

						<IView style={{ backgroundColor: '#f2f1f1' }}>
							<DynamicFlatlistProduct
								ref={ref => {
									this.flatlist = ref
								}}
								listProduct={
									this.listProducts
								}></DynamicFlatlistProduct>
						</IView>
					</ScrollView>
				) : null}
			</View>
		)
	}
}

export default BestSellerScreen
