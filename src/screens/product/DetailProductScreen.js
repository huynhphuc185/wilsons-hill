import React, { Component } from 'react'
import {
	View,
	Text,
	StyleSheet,
	AsyncStorage,
	Platform,
	SafeAreaView,
	Animated,
	Keyboard,
	KeyboardAvoidingView,
} from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	ITouch,
	IFLatList,
	IProgress,
	ITextInput,
} from '../../components'
import BaseScreen from '../BaseScreen'
import { ScrollView } from 'react-native-gesture-handler'
import { colors, images, strings } from '../../../assets'
import DynamicFlatlistProduct from '../home/DynamicFlatlistProduct'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { APIService, NavigationService } from '../../services'
import AlertScreen from '../common/AlertScreen'
import EventEmitter from 'react-native-eventemitter'
import IBanner from '../home/IBanner'
import { constraintLayout } from '../../components/ILayoutProps'
import { priceFormat } from '../../utils'
import { getStatusBarHeight } from 'react-native-status-bar-height'
class DetailProductScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			indexBanner: 0,
			isRefresh: false,
			num: 1,
			isSeemore: false,
			scrollY: new Animated.Value(0),
		}
		this.product_id = this.props.navigation.getParam('product_id')
		this.productObj = null
		this.relateProduct = {}
	}

	componentDidMount() {
		EventEmitter.on('update_like', this.updateLike)
		const getDetailProduct = APIService.getDetailProduct(this.product_id)
		const getListProductRelate = APIService.getListProductRelate(
			this.product_id,
		)
		Promise.all([getDetailProduct, getListProductRelate]).then(result => {
			this.productObj = result[0].product
			this.relateProduct = result[1].product_related
			this.setState({
				isRefresh: true,
				num: this.productObj.min_quantity,
			})
		})

		this.keyboardDidHideListenerDetail = Keyboard.addListener(
			'keyboardDidHide',
			this._keyboardDidHideDetail,
		)
	}

	_keyboardDidHideDetail = () => {
		if (this.productObj.min_quantity > this.state.num) {
			NavigationService.navigate(
				'alert',
				AlertScreen.newInstance(
					'Minimum required of ' +
						this.productObj.name +
						' is ' +
						this.productObj.min_quantity,
					99999,
					() => {
						this.setState({
							num: this.productObj.min_quantity,
						})
						NavigationService.pop()
					},
				),
			)
			return
		}
		console.log('Detail')
	}

	componentWillUnmount() {
		this.keyboardDidHideListenerDetail.remove()
		EventEmitter.removeListener('update_like', this.updateLike)
	}

	updateLike = id => {
		if (this.productObj.like === 1) {
			this.productObj.like = 0
		} else {
			this.productObj.like = 1
		}
		this.setState({ isRefresh: true })
	}

	addToCart = async () => {
		await APIService.addToCart(this.productObj.id, this.state.num)
		EventEmitter.emit('setNumberCart')
		EventEmitter.emit('getCart')
		NavigationService.navigate(
			'alert',
			AlertScreen.newInstance('Add products successfully!'),
		)
	}
	renderPriceSection = () => {
		return (
			<IView
				style={{
					paddingVertical: 14,
					flexDirection: 'row',
					justifyContent: 'space-between',
				}}>
				<IView style={{ flex: 1 }}>
					<IText
						style={{
							fontSize: 20,
							lineHeight: 23,
							fontWeight: '500',
							letterSpacing: 1.6,
						}}>
						{this.productObj.name}
					</IText>
					<IText
						style={{
							fontSize: 20,
							lineHeight: 27,
						}}
						isSo={true}>
						{priceFormat(this.productObj.price)}
					</IText>
				</IView>

				<ITouch
					style={{ width: 16, height: 14 }}
					onPress={async () => {
						var dataLike = await APIService.productLike([
							this.product_id,
						])
						if (dataLike.fail_list.length === 0) {
							this.onEventUpdateWhenLikeProduct([
								this.productObj.id,
							])
						}
					}}>
					<IFastImage
						source={
							this.productObj.like === 1
								? images.p_timden
								: images.p_timtrang
						}
						resizeMode='contain'
						style={{ flex: 1 }}></IFastImage>
				</ITouch>
			</IView>
		)
	}
	renderLine = () => {
		return (
			<IView
				style={{
					width: '100%',
					height: 1,
					backgroundColor: '#C5C5C5',
				}}></IView>
		)
	}
	renderDescription = () => {
		return (
			<IView style={{ paddingVertical: 15 }}>
				<IText
					style={{
						fontSize: 16,
						lineHeight: 19,
						paddingBottom: 10,
						fontWeight: '500',
					}}>
					Description
				</IText>
				<IText
					style={{
						fontSize: 12,
						lineHeight: 18,
						color: '#474647',
						letterSpacing: 1.6,
						fontWeight: '300',
					}}>
					{this.productObj.description}
				</IText>
			</IView>
		)
	}
	renderDimension = callBack => {
		return (
			<IView style={{ paddingVertical: 15 }}>
				<IView
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					<IText
						style={{
							fontSize: 16,
							color: colors.text.black,
							fontWeight: '500',
						}}>
						Details
					</IText>
				</IView>
				<IText
					style={{
						fontSize: 12,
						lineHeight: 18,
						color: '#474647',
						letterSpacing: 0.8,
						fontWeight: '300',
						paddingVertical: 10,
					}}
					numberOfLines={this.state.isSeemore ? 0 : 3}>
					{this.productObj.detail || ''}
				</IText>
				<ITouch
					style={{
						alignItems: 'center',
						flexDirection: 'row',
					}}
					onPress={() => {
						this.setState({ isSeemore: !this.state.isSeemore })
					}}>
					<IText
						style={{
							fontSize: 14,
							marginRight: 8,
							color: colors.text.black,
						}}>
						{!this.state.isSeemore ? 'See more' : 'See less'}
					</IText>
					<IFastImage
						source={
							!this.state.isSeemore ? images.p_down : images.p_up
						}
						style={{ width: 13, height: 7 }}></IFastImage>
				</ITouch>
			</IView>
		)
	}
	renderCollection = () => {
		return (
			<IView style={{}}>
				<IView
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
						alignItems: 'center',
						paddingVertical: 14,
						paddingHorizontal: 18,
					}}>
					<IText
						style={{
							fontSize: 16,
							color: colors.text.black,
							fontWeight: '500',
						}}>
						{'More in ' + this.relateProduct.collection_name}
					</IText>
				</IView>
				<DynamicFlatlistProduct
					listProduct={this.relateProduct.products}
					ref={ref => {
						this._flatList = ref
					}}></DynamicFlatlistProduct>
			</IView>
		)
	}
	renderAddToCart = () => {
		return (
			<IView
				style={{
					height: 76,
					width: '100%',
					bottom: 0,
					position: 'absolute',
					backgroundColor: colors.white,
				}}>
				<IView
					style={{
						margin: 18,
						flex: 1,
						flexDirection: 'row',
					}}>
					<IView
						style={{
							flex: 1,
							borderWidth: 0.5,
							borderColor: colors.black,
							marginRight: 18,
							flexDirection: 'row',
							alignItems: 'center',
						}}>
						<ITouch
							style={{ flex: 1, alignItems: 'center' }}
							disabled={
								this.state.num === this.productObj.min_quantity
									? true
									: false
							}
							onPress={() => {
								let num = this.state.num - 1
								this.setState({ num: num })
							}}>
							<IFastImage
								source={images.p_tru}
								style={{ width: 14, height: 2 }}></IFastImage>
						</ITouch>
						<ITextInput
							style={{
								flex: 2,
								textAlign: 'center',
								fontSize: 16,
								fontWeight: '500',
							}}
							keyboardType='numeric'
							isSo={true}
							onChangeText={text => {
								this.setState({
									num: Number(text),
								})
							}}>
							{this.state.num}
						</ITextInput>
						<ITouch
							style={{ flex: 1, alignItems: 'center' }}
							onPress={() => {
								let num = this.state.num + 1

								this.setState({ num: num })
							}}>
							<IFastImage
								source={images.p_cong}
								style={{ width: 14, height: 14 }}></IFastImage>
						</ITouch>
					</IView>
					<ITouch
						style={{
							width: 198,
							backgroundColor: colors.black,
							justifyContent: 'center',
							alignItems: 'center',
						}}
						onPress={() => {
							this.addToCart()
						}}>
						<IText style={{ color: colors.white, fontSize: 16 }}>
							ADD TO CART
						</IText>
					</ITouch>
				</IView>
			</IView>
		)
	}
	render() {
		const scrollY = Animated.add(this.state.scrollY, 0)
		const barTransparent = scrollY.interpolate({
			inputRange: [0, constraintLayout(226)],
			outputRange: [0, 1],
			extrapolate: 'clamp',
		})
		return this.state.isRefresh === true ? (
			<KeyboardAvoidingView behavior='height' style={{ flex: 1 }} enabled>
				<IView style={{ flex: 1, backgroundColor: colors.bg }}>
					<IToolbar
						title={this.productObj.name}
						isCart={true}
						animatedStyle={barTransparent}
					/>
					<Animated.ScrollView
						onScroll={Animated.event(
							[
								{
									nativeEvent: {
										contentOffset: {
											y: this.state.scrollY,
										},
									},
								},
							],
							{ useNativeDriver: false },
						)}
						contentContainerStyle={{ paddingBottom: 86 }}
						style={{
							position: 'absolute',
							top: 0,
							bottom: 0,
							right: 0,
							left: 0,
						}}>
						<IBanner
							data={this.productObj.images}
							height={constraintLayout(312)}
							isTouch={false}></IBanner>
						<IView style={{ paddingHorizontal: 18 }}>
							{this.renderPriceSection()}
							{this.renderLine()}
							{this.renderDescription()}
							{this.renderLine()}
							{this.renderDimension(() => {
								//this.props.navigation.navigate('CollectionScreen')
							})}
							{this.renderLine()}
						</IView>
						{this.renderCollection()}
					</Animated.ScrollView>

					{this.renderAddToCart()}
				</IView>
			</KeyboardAvoidingView>
		) : (
			<SafeAreaView>
				<IToolbar title={''} isSafeArea={true} isCart={true} />
			</SafeAreaView>
		)
	}
}

export default DetailProductScreen
