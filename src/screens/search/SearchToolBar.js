import React, { Component } from 'react'
import { Text, View, StatusBar } from 'react-native'
import { colors, images, strings } from '../../../assets'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
} from '../../components'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { DrawerActions } from 'react-navigation-drawer'
import { NavigationService } from '../../services'
import IFlatList from '../../components/IFLatList'
SIZE = 56
class SearchToolBar extends Component {
	constructor(props) {
		super(props)
		this.state = {
			textSearch: '',
		}
	}
	render() {
		const { onSearch, isBack } = this.props
		return (
			<IView style={{ backgroundColor: colors.bg }}>
				<StatusBar
					backgroundColor={'transparent'}
					translucent={true}
					animated
					barStyle='dark-content'
				/>
				<IView style={{ height: getStatusBarHeight() }}></IView>
				<IView
					style={{
						height: SIZE,
						flexDirection: 'row',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					{this._renderSearch()}
				</IView>
			</IView>
		)
	}

	onChangeText = text => {
		clearTimeout(this.timer)
		this.setState({
			textSearch: text,
		})
		this.timer = setTimeout(() => {
			this.props.onSearch(text)
		}, 300)
	}

	_renderSearch = () => {
		const { isBack = false, keyword = '' } = this.props
		// console.log(keyword);
		return (
			<IView
				style={{
					paddingHorizontal: 0,
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'center',
					paddingRight: 16,
				}}>
				<ITouch
					style={{
						width: SIZE,
						height: SIZE,
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						NavigationService.pop()
					}}>
					<IFastImage
						style={{ width: 20, height: 16 }}
						resizeMode='contain'
						source={images.ic_back}
					/>
				</ITouch>
				{isBack ? (
					<ITouch
						style={{
							flex: 1,
							height: 40,
							borderWidth: 1,
							backgroundColor: colors.white,
							borderRadius: 100,
							borderColor: colors.white,
							flexDirection: 'row',
							alignItems: 'center',
							paddingLeft: 12,
						}}
						onPress={() => {
							NavigationService.pop()
						}}>
						<IFastImage
							source={images.ic_search}
							style={{ width: 18, height: 18 }}
							resizeMode='contain'
						/>
						<IText
							style={{
								flex: 1,
								color: colors.text.black,
								marginLeft: 8,
								padding: 0,
								fontSize: 16,
							}}>
							{keyword}
						</IText>
					</ITouch>
				) : (
					<IView
						style={{
							flex: 1,
							height: 40,
							borderWidth: 1,
							backgroundColor: colors.white,
							borderRadius: 100,
							borderColor: colors.white,
							flexDirection: 'row',
							alignItems: 'center',
							paddingLeft: 12,
						}}>
						<IFastImage
							source={images.ic_search}
							style={{ width: 18, height: 18 }}
							resizeMode='contain'
						/>

						<ITextInput
							placeholder={'Search'}
							style={{
								flex: 1,
								color: colors.text.black,
								marginLeft: 8,
								padding: 0,
								fontSize: 16,
							}}
							value={this.state.textSearch}
							onChangeText={text => {
								this.onChangeText(text)
							}}
							onSubmitEditing={event =>
								this.props.onSubmitEditing(
									event.nativeEvent.text,
								)
							}
						/>
						{this.state.textSearch != '' ? (
							<ITouch
								onPress={() => {
									this.props.onSearch('')
									this.setState({
										textSearch: '',
									})
								}}>
								<IFastImage
									style={{
										width: 22,
										height: 22,
										marginHorizontal: 8,
									}}
									source={images.p_close}
									resizeMode='contain'
								/>
							</ITouch>
						) : null}
					</IView>
				)}
			</IView>
		)
	}
}
export default SearchToolBar
