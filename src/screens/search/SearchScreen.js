import React, { Component } from 'react'
import BaseScreen from '../BaseScreen'
import { colors, images } from '../../../assets'
import { IToolbar } from '../../components'
import SearchToolBar from './SearchToolBar'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
} from '../../components'
import IFlatList from '../../components/IFLatList'
import { NavigationService, APIService, UserService } from '../../services'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import RowProduct from '../row/RowProduct'
import IBanner from '../home/IBanner'
import RowCollection from '../row/RowCollection'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import AutoHeightImage from 'react-native-scalable-image'
import { constraintLayout } from '../../components/ILayoutProps'
import { priceFormat } from '../../utils'
import EventEmitter from 'react-native-eventemitter'
import AlertScreen from '../common/AlertScreen'

class SearchScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			showSuggestScreen: false,
			isRefesh: false,
			isLogin: null,
		}
		this.collection = []
		this.product = []
		this.suggestionSearch = {}
	}

	componentDidMount() {
		const getTopSearchCollection = APIService.getTopSearchCollection()
		const getTopSearchProduct = APIService.getTopSearchProduct()
		const isLogin = UserService.isLogin()
		Promise.all([
			getTopSearchCollection,
			getTopSearchProduct,
			isLogin,
		]).then(result => {
			this.collection = result[0].collection
			this.product = result[1].product
			this.setState({ isRefesh: true, isLogin: result[2] })
		})
	}
	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<SearchToolBar
					onSearch={async text => {
						if (text.length !== 0) {
							this.suggestionSearch = await APIService.getSearchSuggestion(
								text,
							)
							this.setState({ showSuggestScreen: true })
						} else {
							this.setState({ showSuggestScreen: false })
						}
					}}
					onSubmitEditing={text => {
						this.props.navigation.navigate('SearchResultScreen', {
							keyword: text,
						})
					}}></SearchToolBar>
				{this.state.showSuggestScreen ? (
					this.renderListSuggestion()
				) : this.state.isRefesh ? (
					<ScrollView>
						{this.renderTitle('Top searched items')}
						{this.renderTopSearch()}
						{this.renderTitle('Top searched collection')}
						{this.renderTopCollection()}
					</ScrollView>
				) : null}
			</IView>
		)
	}

	renderListSuggestion = () => {
		return (
			<ScrollView style={{ flex: 1 }}>
				{this.suggestionSearch.suggestions_collection.length != 0 ? (
					<IView>
						<IText
							style={{
								marginVertical: 20,
								marginLeft: 16,
								fontSize: 14,
								fontWeight: '500',
							}}>
							Collection
						</IText>
						<IFlatList
							// style={{
							// 	flex: 1,
							// }}
							data={this.suggestionSearch.suggestions_collection}
							renderItem={({ item, index }) => (
								<ITouch
									onPress={() => {
										this.props.navigation.navigate(
											'SearchResultScreen',
											{
												keyword: item,
											},
										)
									}}>
									<IText
										style={{
											paddingVertical: 12,
											paddingHorizontal: 16,
											fontSize: 14,
											backgroundColor: 'white',
										}}>
										{item}
									</IText>
								</ITouch>
							)}
							scrollEnabled={false}
							extraData={
								this.suggestionSearch.suggestions_collection
							}
							ItemSeparatorComponent={() => (
								<IView
									style={{
										height: 1,
										backgroundColor: colors.gray._400,
										marginHorizontal: 16,
									}}
								/>
							)}
							keyExtractor={(item, index) => index.toString()}
						/>
					</IView>
				) : null}
				{this.suggestionSearch.suggestions_product.length != 0 ? (
					<IView>
						<IText
							style={{
								marginVertical: 20,
								marginLeft: 16,
								fontSize: 14,
								fontWeight: '500',
							}}>
							Item
						</IText>
						<IFlatList
							// style={{
							// 	flex: 1,
							// }}
							data={this.suggestionSearch.suggestions_product}
							renderItem={({ item, index }) => (
								<ITouch
									onPress={() => {
										this.props.navigation.navigate(
											'SearchResultScreen',
											{
												keyword: item,
											},
										)
									}}>
									<IText
										style={{
											paddingVertical: 12,
											paddingHorizontal: 16,
											fontSize: 14,
											backgroundColor: 'white',
										}}>
										{item}
									</IText>
								</ITouch>
							)}
							scrollEnabled={false}
							extraData={
								this.suggestionSearch.suggestions_product
							}
							ItemSeparatorComponent={() => (
								<IView
									style={{
										height: 1,
										backgroundColor: colors.gray._400,
										marginHorizontal: 16,
									}}
								/>
							)}
							keyExtractor={(item, index) => index.toString()}
						/>
					</IView>
				) : null}
				{this.suggestionSearch.suggestions_cate.length != 0 ? (
					<IView>
						<IText
							style={{
								marginVertical: 20,
								marginLeft: 16,
								fontSize: 14,
								fontWeight: '500',
							}}>
							Category
						</IText>
						<IFlatList
							// style={{
							// 	flex: 1,
							// }}
							data={this.suggestionSearch.suggestions_cate}
							renderItem={({ item, index }) => (
								<ITouch
									onPress={() => {
										this.props.navigation.navigate(
											'SearchResultScreen',
											{
												keyword: item,
											},
										)
									}}>
									<IText
										style={{
											paddingVertical: 12,
											paddingHorizontal: 16,
											fontSize: 14,
											backgroundColor: 'white',
										}}>
										{item}
									</IText>
								</ITouch>
							)}
							scrollEnabled={false}
							extraData={this.suggestionSearch.suggestions_cate}
							ItemSeparatorComponent={() => (
								<IView
									style={{
										height: 1,
										backgroundColor: colors.gray._400,
										marginHorizontal: 16,
									}}
								/>
							)}
							keyExtractor={(item, index) => index.toString()}
						/>
					</IView>
				) : null}
			</ScrollView>
		)
	}
	renderTopCollection = () => {
		return (
			// <IView
			// 	style={{
			// 		height: 212,
			// 		width: this.width,
			// 		backgroundColor: colors.bg,
			// 	}}>
			// 	<Carousel
			// 		ref={c => {
			// 			this._carousel = c
			// 		}}
			// 		data={this.collection}
			// 		renderItem={({ item, index }) => (
			// 			<ITouch
			// 				onPress={() => {
			// 					NavigationService.push(
			// 						'CollectionDetailScreen',
			// 						{
			// 							collection_id: item.id,
			// 						},
			// 					)
			// 				}}
			// 				style={{
			// 					alignItems: 'center',
			// 					justifyContent: 'center',
			// 				}}>
			// 				<IFastImage
			// 					source={{ uri: item.image || item }}
			// 					resizeMode='contain'
			// 					style={{
			// 						height: 214,
			// 						width: '100%',
			// 					}}></IFastImage>
			// 			</ITouch>
			// 		)}
			// 		activeAnimationType={'timing'}
			// 		sliderWidth={this.width}
			// 		itemWidth={this.width}
			// 		loop
			// 		autoplay
			// 	/>
			// </IView>
			<IFlatList
				style={{}}
				data={this.collection}
				renderItem={({ item, index }) => (
					<RowCollection data={item} index={index}></RowCollection>
				)}
				extraData={this.collection}
				keyExtractor={(item, index) => item}
				showsVerticalScrollIndicator={false}
				ListEmptyComponent={() => (
					<IView
						style={{
							padding: 32,
							alignItems: 'center',
							justifyContent: 'center',
						}}>
						<IText
							style={{ color: colors.gray._400, fontSize: 16 }}>
							No items
						</IText>
					</IView>
				)}
			/>
		)
	}

	renderSuggestion = () => {
		return (
			<IView
				style={{
					height: 164,
					marginHorizontal: 18,

					flexDirection: 'row',
				}}>
				<ITouch style={{ flex: 1 }}>
					<IFastImage
						style={{
							flex: 1,
							borderTopLeftRadius: 10,
							borderBottomLeftRadius: 10,
						}}
						source={images.sell5}></IFastImage>
				</ITouch>
				<IView
					style={{ width: 2, backgroundColor: colors.white }}></IView>
				<IView
					style={{
						width: 141,
					}}>
					<IView style={{ flex: 1, flexDirection: 'row' }}>
						<ITouch
							style={{
								flex: 1,
							}}>
							<IFastImage
								style={{
									flex: 1,
								}}
								source={images.collection2}></IFastImage>
						</ITouch>
						<IView
							style={{
								width: 2,
								backgroundColor: colors.white,
							}}></IView>
						<ITouch
							style={{
								flex: 1,
							}}>
							<IFastImage
								style={{
									flex: 1,
									borderTopRightRadius: 10,
								}}
								source={images.sell3}></IFastImage>
						</ITouch>
					</IView>
					<IView
						style={{
							height: 2,
							backgroundColor: colors.white,
						}}></IView>
					<IView style={{ flex: 1, flexDirection: 'row' }}>
						<ITouch
							style={{
								flex: 1,
							}}>
							<IFastImage
								style={{
									flex: 1,
								}}
								source={images.sell4}></IFastImage>
						</ITouch>
						<IView
							style={{
								width: 2,
								backgroundColor: colors.white,
							}}></IView>
						<ITouch
							style={{
								flex: 1,
							}}>
							<IFastImage
								style={{
									flex: 1,
									borderBottomRightRadius: 10,
								}}
								source={images.sell5}></IFastImage>
						</ITouch>
					</IView>
				</IView>
			</IView>
		)
	}
	renderTopSearch = () => {
		return (
			<IView
				style={{
					marginHorizontal: 18,
				}}>
				<IFlatList
					style={{
						flex: 1,
					}}
					horizontal
					data={this.product}
					renderItem={({ item, index }) => (
						<RowProductSearch
							item={item}
							index={index}
							width={(this.width - 54 - 80) / 2.5}
							isLogin={this.state.isLogin}
							isCheck={false}></RowProductSearch>
					)}
					extraData={this.product}
					keyExtractor={(item, index) => index.toString()}
					ItemSeparatorComponent={() => (
						<IView style={{ width: 18 }} />
					)}
					showsHorizontalScrollIndicator={false}
				/>
			</IView>
		)
	}

	renderTitle = (text, callBack) => {
		return (
			<IView
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					paddingHorizontal: 18,
					marginVertical: 15,
				}}>
				<IText
					style={{
						fontSize: 14,
						color: colors.text.black,
						letterSpacing: 1.6,
						fontWeight: '500',
					}}>
					{text}
				</IText>
				{callBack != null ? (
					<ITouch
						style={{
							alignItems: 'center',
							justifyContent: 'center',
							flexDirection: 'row',
						}}
						onPress={() => {
							callBack()
						}}>
						<IText
							style={{
								fontSize: 14,
								marginRight: 11,
								color: colors.text.black,
							}}>
							View all
						</IText>
						<IFastImage
							source={images.vector}
							style={{ width: 16, height: 9 }}></IFastImage>
					</ITouch>
				) : null}
			</IView>
		)
	}
}

class RowProductSearch extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isShow: false,
			checkSave: false,
			isRefreshRow: true,
		}
	}
	componentDidMount() {
		EventEmitter.on('update_like', this.updateLike)
	}
	componentWillUnmount() {
		EventEmitter.removeListener('update_like', this.updateLike)
	}
	updateLike = listID => {
		const { item } = this.props
		listID.forEach(id => {
			if (id == item.id) {
				if (item.like === 1) {
					item.like = 0
				} else {
					item.like = 1
				}
				this.setState({ isRefreshRow: true })
				return
			}
		})
	}
	render() {
		const { item, index, isCheck, isLogin } = this.props
		console.log(item.collection_name)

		return this.state.isRefreshRow ? (
			<ITouch
				style={{ flex: 1 }}
				onPress={index => {
					isLogin
						? NavigationService.push('DetailProductScreen', {
								product_id: item.id,
						  })
						: NavigationService.navigate(
								'alert',
								AlertScreen.newInstance('Login required', -2),
						  )
				}}>
				<IView>
					<IView
						style={{
							backgroundColor: colors.white,
							borderRadius: 10,
							padding: 15,
						}}>
						<AutoHeightImage
							width={constraintLayout(138)}
							source={{ uri: item.image }}
							style={{}}
							onLoad={() => {
								this.setState({ isShow: true })
							}}></AutoHeightImage>
					</IView>
					{isCheck === true ? (
						<IView
							style={{
								position: 'absolute',
								top: 8,
								right: 7,
								zIndex: 99,
							}}>
							<ITouch
								onPress={() => {
									this.setState({
										checkSave: !this.state.checkSave,
									})
								}}>
								<IFastImage
									source={
										this.state.checkSave
											? images.check_save
											: images.uncheck_save
									}
									resizeMode='contain'
									style={{
										width: 20,
										height: 20,
									}}></IFastImage>
							</ITouch>
						</IView>
					) : null}
					{this.state.isShow === true ? (
						<IView
							style={{
								position: 'absolute',
								bottom: 5,
								top: 5,
								left: 5,
								right: 5,
								justifyContent: 'space-between',
							}}>
							<IFastImage
								source={images.p_new}
								resizeMode='contain'
								style={{ width: 37, height: 19 }}></IFastImage>
							<IView style={{ alignItems: 'flex-end' }}>
								<ITouch
									style={{ width: 16, height: 14 }}
									onPress={async () => {
										var dataLike = await APIService.productLike(
											[item.id],
										)
										if (dataLike.fail_list.length === 0) {
											this.onEventUpdateWhenLikeProduct([
												item.id,
											])
										}
									}}>
									<IFastImage
										source={
											item.like === 1
												? images.p_timden
												: images.p_timtrang
										}
										resizeMode='contain'
										style={{ flex: 1 }}></IFastImage>
								</ITouch>
							</IView>
						</IView>
					) : null}
				</IView>
				{item.collection_name != '' ? (
					<IText
						style={{
							fontSize: 10,
							lineHeight: 12,
							paddingTop: 6,
							fontWeight: '500',
							letterSpacing: 1.6,
						}}>
						{/* {item.collection_name} */}[{item.collection_name}]
						Collection
					</IText>
				) : null}

				<IText
					style={{
						fontSize: 12,
						lineHeight: 14,
						paddingVertical: 6,
						fontWeight: '500',
						letterSpacing: 1.6,
					}}>
					{item.name}
				</IText>
				{/* <IText
					style={{
						fontSize: 14,
						lineHeight: 18,

						paddingBottom: 6,
					}}
					isSo={true}>
					{priceFormat(item.price)}
				</IText> */}
				{item.price === 0 ? null : (
					<IText
						style={{
							fontSize: 14,
							lineHeight: 18,
							paddingBottom: 6,
						}}
						isSo = {true}>
						{priceFormat(item.price)}
					</IText>
				)}
			</ITouch>
		) : null
	}
}

export default SearchScreen
