import React, { Component } from 'react'
import BaseScreen from '../BaseScreen'
import { colors, images } from '../../../assets'
import { IToolbar } from '../../components'
import SearchToolBar from './SearchToolBar'
import {
	IView,
	IFastImage,
	ITouch,
	INotity,
	ICart,
	IText,
	ITextInput,
} from '../../components'
import IFlatList from '../../components/IFLatList'
import { NavigationService, APIService } from '../../services'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import RowCollection from '../row/RowCollection'
import DynamicFlatlistProduct from '../home/DynamicFlatlistProduct'
class SearchResultScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isRefesh: false,
		}
		this.keyword = this.props.navigation.getParam('keyword')
		this.collection = []
		this.product = []
		this.category = []
	}

	componentDidMount() {
		const getSearchProductWithKey = APIService.getSearchProductWithKey(
			this.keyword,
		)
		const getSearchCategoryWithKey = APIService.getSearchCategoryWithKey(
			this.keyword,
		)
		const getSearchCollectionWithKey = APIService.getSearchCollectionWithKey(
			this.keyword,
		)
		Promise.all([
			getSearchProductWithKey,
			getSearchCategoryWithKey,
			getSearchCollectionWithKey,
		]).then(result => {
			this.product = result[0].product
			this.category = result[1].product_in_cate
			this.collection = result[2].collection
			this.setState({ isRefesh: true })
		})
	}
	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<SearchToolBar
					isBack={true}
					keyword={this.keyword}></SearchToolBar>
				{this.state.isRefesh ? (
					<ScrollView>
						{this.renderTitle('Collections', this.keyword)}
						{this.renderCollectionSearch()}
						{this.renderTitle('Items', this.keyword)}
						{this.renderProductSearch(this.product)}
						{this.renderTitle('Categories', this.keyword)}
						{this.renderProductSearch(this.category)}
					</ScrollView>
				) : null}
			</IView>
		)
	}
	renderProductSearch = list => {
		return (
			<IView style={{ backgroundColor: '#f2f1f1' }}>
				<DynamicFlatlistProduct
					listProduct={list}></DynamicFlatlistProduct>
			</IView>
		)
	}

	itemProductSearch = (item, index) => {
		return (
			<ITouch
				onPress={async () => {
					NavigationService.push('DetailProductStack')
				}}
				style={{
					height: (226 - 18) / 2,
					flex: 1,
				}}>
				<IFastImage
					resizeMode='contain'
					style={{
						backgroundColor: 'white',
						borderRadius: 10,
						flex: 1,
						margin: 9,
					}}
					source={item.image}
				/>
			</ITouch>
		)
	}
	renderCollectionSearch = () => {
		return (
			<IFlatList
				style={{}}
				data={this.collection}
				renderItem={({ item, index }) => (
					<RowCollection data={item} index={index}></RowCollection>
				)}
				extraData={this.collection}
				keyExtractor={(item, index) => item}
				showsVerticalScrollIndicator={false}
				ListEmptyComponent={() => (
					<IView
						style={{
							padding: 32,
							alignItems: 'center',
							justifyContent: 'center',
						}}>
						<IText
							style={{ color: colors.gray._400, fontSize: 16 }}>
							No items
						</IText>
					</IView>
				)}
			/>
		)
	}

	renderTitle = (text, keyword, callBack) => {
		return (
			<IView
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					paddingHorizontal: 18,
					marginVertical: 15,
				}}>
				<IText
					style={{
						fontSize: 14,
						color: '#1C1C1C',
						letterSpacing: 1.6,
						fontWeight: '300',
					}}>
					Results for
					<IText
						style={{
							fontSize: 14,
							color: colors.text.black,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						{' "' + keyword + '" '}
					</IText>
					<IText
						style={{
							fontSize: 14,
							color: '#1C1C1C',
							letterSpacing: 1.6,
							fontWeight: '300',
						}}>
						in
					</IText>
					<IText
						style={{
							fontSize: 14,
							color: colors.text.black,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						{' ' + text}
					</IText>
				</IText>
				{callBack != null ? (
					<ITouch
						style={{
							alignItems: 'center',
							justifyContent: 'center',
							flexDirection: 'row',
						}}
						onPress={() => {
							callBack()
						}}>
						<IText
							style={{
								fontSize: 14,
								marginRight: 11,
								color: colors.text.black,
							}}>
							View all
						</IText>
						<IFastImage
							source={images.vector}
							style={{ width: 16, height: 9 }}></IFastImage>
					</ITouch>
				) : null}
			</IView>
		)
	}
}

export default SearchResultScreen
