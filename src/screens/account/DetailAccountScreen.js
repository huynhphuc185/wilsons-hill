import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  ScrollView,
  Alert
} from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch,
  ITextInput
} from "../../components";
import { colors, images } from "../../../assets";
import { APIService, UserService, NavigationService } from "../../services";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaseScreen from "../BaseScreen";
import AlertScreen from "../common/AlertScreen";
import EventEmitter from "react-native-eventemitter";

class DetailAccountScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      dataProfile: {}
    };
  }
  componentDidMount() {
    this.getProfile();
    EventEmitter.on("update_profile", this.getProfile);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("update_profile", this.getProfile);
  }

  getProfile = () => {
    APIService.getProfile(false).then(data => {
      if (data != null) {
        this.setState({
          dataProfile: data
        });
        console.log(data);
      }
    });
  };

  render() {
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={"Details"} notSearch="true" editProfile="true" />
        <KeyboardAwareScrollView style={{}}>
          <IView style={{ paddingHorizontal: 18, marginBottom: 32 }}>
            <IView style={{}}>
              <IText style={{ fontSize: 14 }}>Company name</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.company_name}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Address</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.billing_address}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Country</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.country}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Person in contact</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.person_contact}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Occupation</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.occupation}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Email</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.email_contact}
                </IText>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Phone number</IText>
              <IView
                style={{
                  height: 40,
                  //   borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <IText style={{ fontSize: 14, fontWeight: "500" }}>
                  {this.state.dataProfile.phone}
                </IText>
              </IView>
            </IView>
            <IView
              style={{
                marginTop: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ITouch
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: "black"
                }}
                onPress={() => this.props.navigation.navigate("ChangePassword")}
              >
                <IText
                  style={{
                    fontSize: 16
                  }}
                >
                  Change password
                </IText>
              </ITouch>
            </IView>
          </IView>
        </KeyboardAwareScrollView>
      </IView>
    );
  }
}

export default DetailAccountScreen;
