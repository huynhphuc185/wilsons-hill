import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  ScrollView,
  Alert
} from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch,
  ITextInput
} from "../../components";
import { colors, images } from "../../../assets";
import { APIService, UserService, NavigationService } from "../../services";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaseScreen from "../BaseScreen";
import AlertScreen from "../common/AlertScreen";
import EventEmitter from "react-native-eventemitter";

class EditAccount extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      dataProfile: {}
    };
  }
  componentDidMount() {
    this.getProfile();
  }
  getProfile() {
    APIService.getProfile(false).then(data => {
      if (data != null) {
        this.setState({
          dataProfile: data
        });
        console.log(data);
      }
    });
  }
  updateProfile() {
    if (!this.state.dataProfile.company_name) {
      this.state.dataProfile.company_name = "";
    }
    if (!this.state.dataProfile.country) {
      this.state.dataProfile.country = "";
    }
    if (!this.state.dataProfile.billing_address) {
      this.state.dataProfile.billing_address = "";
    }
    if (!this.state.dataProfile.person_contact) {
      this.state.dataProfile.person_contact = "";
    }
    if (!this.state.dataProfile.occupation) {
      this.state.dataProfile.occupation = "";
    }
    if (!this.state.dataProfile.email_contact) {
      this.state.dataProfile.email_contact = "";
    }
    if (!this.state.dataProfile.phone) {
      this.state.dataProfile.phone = "";
    }

    if (
      this.state.dataProfile.company_name.length === 0 ||
      this.state.dataProfile.country.length === 0 ||
      this.state.dataProfile.billing_address.length === 0 ||
      this.state.dataProfile.person_contact.length === 0 ||
      this.state.dataProfile.occupation.length === 0 ||
      this.state.dataProfile.email_contact.length === 0 ||
      this.state.dataProfile.phone.length === 0
    ) {
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance("Please enter all information")
      );
      return;
    }
    APIService.updateProfile(
      this.state.dataProfile.company_name,
      this.state.dataProfile.country,
      this.state.dataProfile.billing_address,
      this.state.dataProfile.person_contact,
      this.state.dataProfile.occupation,
      this.state.dataProfile.email_contact,
      this.state.dataProfile.phone
    ).then(data => {
      if (data != null) {
        // console.log("cap nhat thanh cong");
        EventEmitter.emit("update_profile");
        EventEmitter.emit("update_detail_profile");
        NavigationService.pop();
      }
    });
  }

  render() {
    // {colors.set(this.state.colors)}
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar
          title={"Details"}
          notSearch="true"
          saveProfile="true"
          callBack={() => {
            this.updateProfile();
          }}
        />
        <KeyboardAwareScrollView style={{}}>
          <IView style={{ paddingHorizontal: 18, marginBottom: 32 }}>
            <IView style={{}}>
              <IText style={{ fontSize: 14 }}>Company name</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.company_name = value;
                  }}
                >
                  {this.state.dataProfile.company_name}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Address</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.billing_address = value;
                  }}
                >
                  {this.state.dataProfile.billing_address}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Country</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.country = value;
                  }}
                >
                  {this.state.dataProfile.country}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Person in contact</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.person_contact = value;
                  }}
                >
                  {this.state.dataProfile.person_contact}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Occupation</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.occupation = value;
                  }}
                >
                  {this.state.dataProfile.occupation}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Email</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.email_contact = value;
                  }}
                >
                  {this.state.dataProfile.email_contact}
                </ITextInput>
              </IView>
            </IView>
            <IView style={{ marginTop: 15 }}>
              <IText style={{ fontSize: 14 }}>Phone number</IText>
              <IView
                style={{
                  height: 40,
                  borderWidth: 0.5,
                  paddingHorizontal: 12,
                  justifyContent: "center",
                  marginTop: 7,
                  backgroundColor: "white"
                }}
              >
                <ITextInput
                  style={{
                    fontSize: 14,
                    color: "black",
                    flex: 1
                  }}
                  // placeholder={"Email address"}
                  onChangeText={value => {
                    this.state.dataProfile.phone = value;
                  }}
                >
                  {this.state.dataProfile.phone}
                </ITextInput>
              </IView>
            </IView>
            <IView
              style={{
                marginTop: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ITouch
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: "black"
                }}
                onPress={() => this.props.navigation.navigate("ChangePassword")}
              >
                <IText
                  style={{
                    fontSize: 16
                  }}
                >
                  Change password
                </IText>
              </ITouch>
            </IView>
          </IView>
        </KeyboardAwareScrollView>
      </IView>
    );
  }
}

export default EditAccount;
