import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage, Platform } from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch
} from "../../components";
import EventEmitter from "react-native-eventemitter";
import BaseScreen from "../BaseScreen";
import { images } from "../../../assets";
import { APIService, NavigationService, UserService } from "../../services";
import AlertScreen from "../common/AlertScreen";

class AccountScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      dataProfile: {}
    };
  }

  componentDidMount() {
    this.getProfile();
    EventEmitter.on("update_profile", this.getProfile);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("update_profile", this.getProfile);
  }

  getProfile = () => {
    APIService.getProfile().then(data => {
      if (data != null) {
        this.setState({
          dataProfile: data
        });
        console.log(data);
      }
    });
  };

  renderMembership() {
    if (this.state.dataProfile.membership_id === 1) {
      return (
        <IFastImage
          style={{
            marginTop: 7.86,
            width: 59,
            height: 20
          }}
          source={images.medal1}
          resizeMode="contain"
        />
      );
    } else if (this.state.dataProfile.membership_id === 2) {
      return (
        <IFastImage
          style={{
            marginTop: 7.86,
            width: 59,
            height: 20
          }}
          source={images.medal2}
          resizeMode="contain"
        />
      );
    } else if (this.state.dataProfile.membership_id === 3) {
      return (
        <IFastImage
          style={{
            marginTop: 7.86,
            width: 59,
            height: 20
          }}
          source={images.medal3}
          resizeMode="contain"
        />
      );
    }
  }

  renderHeader() {
    return (
      <IView style={{ paddingHorizontal: 18, backgroundColor: "white" }}>
        <IView
          style={{
            flexDirection: "row",
            paddingTop: 12,
            paddingBottom: 17

            // marginVertical: 18
          }}
        >
          <IView style={{ flex: 1 }}>
            <IText
              style={{ fontSize: 16, fontWeight: "700", letterSpacing: 1.6 }}
            >
              {this.state.dataProfile.company_name}
            </IText>
            <IView style={{ marginLeft: 11 }}>
              {this.renderMembership()}
              <IView style={{ marginLeft: 4 }}>
                <IView
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 6
                  }}
                >
                  <IFastImage
                    style={{
                      width: 8,
                      height: 8
                    }}
                    source={images.ic_phone}
                    resizeMode="contain"
                  />
                  <IText
                    style={{
                      fontSize: 12,
                      letterSpacing: 1.6,
                      
                      marginLeft: 10
                    }}
                    isSo = {true}
                  >
                    {this.state.dataProfile.phone}
                  </IText>
                </IView>
                <IView
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 4
                  }}
                >
                  <IFastImage
                    style={{
                      width: 6,
                      height: 8
                    }}
                    source={images.ic_location}
                    resizeMode="contain"
                  />
                  <IText
                    style={{
                      fontSize: 12,
                      letterSpacing: 1.8,
                      
                      marginLeft: 12
                    }}
                    isSo = {true}
                  >
                    {this.state.dataProfile.billing_address}
                  </IText>
                </IView>
              </IView>
            </IView>
          </IView>
          <IView>
            <ITouch
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "black"
              }}
              onPress={() =>
                this.props.navigation.navigate("DetailAccountScreen")
              }
            >
              <IText
                style={{
                  fontSize: 12,
                  letterSpacing: 0.9
                }}
              >
                Details
              </IText>
            </ITouch>
          </IView>
        </IView>
      </IView>
    );
  }
  line() {
    return (
      <IView
        style={{
          marginHorizontal: 18,
          backgroundColor: "#C5C5C5",
          height: 1
        }}
      ></IView>
    );
  }

  renderRowMenu(name, icon, callBack) {
    return (
      <ITouch
        style={{
          height: 56,
          alignItems: "center",
          flexDirection: "row",
          paddingHorizontal: 18
        }}
        onPress={() => {
          callBack();
        }}
      >
        <IView style={{ flex: 1, flexDirection: "row" }}>
          <IFastImage
            style={{
              width: 20,
              height: 20,
              marginRight: 14
            }}
            source={icon}
            resizeMode="contain"
          />
          <IText style={{ fontSize: 14, letterSpacing: 1.6 }}>{name}</IText>
        </IView>
        <IFastImage
          style={{
            width: 5,
            height: 8
          }}
          source={images.icmenu_next}
          resizeMode="contain"
        />
      </ITouch>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#f2f1f1" }}>
        <IToolbar title={"Account"} notBack="true" notSearch="true" />
        {/* <ITouch
          style={{
            height: 50,
            backgroundColor: "white",
            position: "absolute",
            bottom: 10,
            left: 30,
            right: 30,
            borderRadius: 12,
            justifyContent: "center",
            alignItems: "center"
          }}
          onPress={async () => {
            await UserService.reset();
            NavigationService.resetBackHome("oauthStack");
          }}
        >
          <IText style={{ fontSize: 18, fontWeight: "900" }}>LOGOUT</IText>
        </ITouch> */}
        {this.renderHeader()}
        {this.renderRowMenu("Order History", images.icmenu_orderhistory, () => {
          this.props.navigation.navigate("OrderHistoryScreen");
        })}
        {this.line()}
        {this.renderRowMenu(
          "App Preference",
          images.icmenu_apppreference,
          () => {
            NavigationService.navigate(
              "alert",
              AlertScreen.newInstance("Coming soon!")
            );
          }
        )}
        {this.line()}
        {this.renderRowMenu("About the app", images.icmenu_aboutapp, () => {
          NavigationService.navigate(
            "alert",
            AlertScreen.newInstance("Coming soon!")
          );
        })}
        {this.line()}
        {this.renderRowMenu("Log out", images.icon_logout, async () => {
          await UserService.reset();
          NavigationService.resetBackHome("oauthStack");
        })}
      </View>
    );
  }
}

export default AccountScreen;
