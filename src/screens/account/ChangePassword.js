import React, { Component } from 'react'
import {
	View,
	Text,
	StyleSheet,
	AsyncStorage,
	Platform,
	ScrollView,
	Alert,
} from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
	ITextInput,
} from '../../components'
import { colors, images } from '../../../assets'
import { APIService, UserService, NavigationService } from '../../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BaseScreen from '../BaseScreen'
import AlertScreen from '../common/AlertScreen'

class ChangePassword extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			current_password: '',
			new_password: '',
			retype_password: '',
			isCurrentPassword: false,
		}
	}

	changePassword() {
		if (this.state.new_password != this.state.retype_password) {
			this.setState({ isCurrentPassword: true })
			return
		}
		APIService.changePassword(
			this.state.current_password,
			this.state.new_password,
			this.state.retype_password,
		).then(data => {
			if (data != null) {
				NavigationService.navigate(
					'alert',
					AlertScreen.newInstance(
						'Change password success',
						666,
						() => {
							NavigationService.pop() //pop này tắt popup alert
							NavigationService.pop() //pop này để back ra màng hình trước
						},
					),
				)
			}
		})
	}

	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar title={'Change Password'} notSearch='true' />
				<IView style={{ paddingHorizontal: 18 }}>
					<IView style={{}}>
						<IText style={{ fontSize: 14 }}>Current password</IText>
						<IView
							style={{
								height: 40,
								borderWidth: 0.5,
								justifyContent: 'center',
								marginTop: 7,
								backgroundColor: 'white',
							}}>
							<ITextInput
								style={{
									fontSize: 14,
									marginLeft: 10,
									color: 'black',
								}}
								secureTextEntry={true}
								placeholder={''}
								onChangeText={value =>
									this.setState({ current_password: value })
								}
							/>
						</IView>
					</IView>
					<IText style={{ fontSize: 14, marginTop: 16 }}>
						Please enter your new password below
					</IText>
					<IText
						style={{
							fontSize: 12,
							marginTop: 4,
							fontWeight: '300',
							lineHeight: 12,
						}}>
						Minimum 6 characters with a number and a letter
					</IText>
					<IView style={{ marginTop: 10 }}>
						<IText style={{ fontSize: 14 }}>New password</IText>
						<IView
							style={{
								height: 40,
								borderWidth: 0.5,
								justifyContent: 'center',
								marginTop: 7,
								backgroundColor: 'white',
							}}>
							<ITextInput
								style={{
									fontSize: 14,
									marginLeft: 10,
									color: 'black',
								}}
								secureTextEntry={true}
								placeholder={''}
								onChangeText={value =>
									this.setState({ new_password: value })
								}
							/>
						</IView>
					</IView>
					<IView style={{ marginTop: 15 }}>
						<IText style={{ fontSize: 14 }}>Retype password</IText>
						<IView
							style={{
								height: 40,
								borderWidth: 0.5,
								justifyContent: 'center',
								marginTop: 7,
								backgroundColor: 'white',
							}}>
							<ITextInput
								style={{
									fontSize: 14,
									marginLeft: 10,
									color: 'black',
								}}
								secureTextEntry={true}
								placeholder={''}
								onChangeText={value =>
									this.setState({ retype_password: value })
								}
							/>
						</IView>
						{this.state.isCurrentPassword ? (
							<IText
								style={{
									fontSize: 12,
									marginTop: 8,
									color: '#E11C1C',
								}}
								isItalic = {true}>
								The two passwords don’t match.
							</IText>
						) : null}
					</IView>
					<ITouch
						style={{
							marginTop: 33,
							height: 40,
							justifyContent: 'center',
							alignItems: 'center',
							backgroundColor: 'black',
						}}

						disabled = {this.state.current_password.length >= 6 && this.state.new_password.length >= 6 && this.state.retype_password.length >= 6 ? false : true}
						onPress={() => this.changePassword()}>
						<IText
							style={{
								color: 'white',
								fontSize: 16,
								letterSpacing: 1.8,
							}}>
							SAVE
						</IText>
					</ITouch>
				</IView>
			</IView>
		)
	}
}

export default ChangePassword
