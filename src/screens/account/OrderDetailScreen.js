import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
} from '../../components'
import BaseScreen from '../BaseScreen'
import { priceFormat } from '../../utils'
import { images } from '../../../assets'
import { APIService, NavigationService, UserService } from '../../services'
import AlertScreen from '../common/AlertScreen'
import { ScrollView } from 'react-native-gesture-handler'
import RowCheckOut from '../row/RowCheckOut'
import EventEmitter from 'react-native-eventemitter'
class OrderDetailScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = { isRefresh: false }
		this.orderID = this.props.navigation.getParam('orderID')

		this.orderDetail = {}
	}

	async componentDidMount() {
		let data = await APIService.getOrderDetail(this.orderID)
		this.orderDetail = data.order
		this.setState({ isRefresh: true })
		console.log(this.orderDetail)
	}
	renderDeliveryInfo = () => {
		return (
			<IView style={{ paddingHorizontal: 18, paddingVertical: 10 }}>
				<IText
					style={{
						fontSize: 14,
						fontWeight: '500',
						letterSpacing: 1.6,
					}}>
					Delivery Infomation
				</IText>
				<IText
					style={{
						fontSize: 14,
						fontWeight: '500',
						letterSpacing: 1.6,
						paddingVertical: 8,
					}}>
					Forwarder:
				</IText>
				<IText
					style={{
						fontSize: 12,
						fontWeight: '300',
						letterSpacing: 1.6,
					}}>
					{this.orderDetail.forwarder}
				</IText>
				<IText
					style={{
						fontSize: 14,
						fontWeight: '500',
						letterSpacing: 1.6,
						paddingVertical: 8,
					}}>
					Port of discharge:
				</IText>
				<IText
					style={{
						fontSize: 12,
						fontWeight: '300',
						letterSpacing: 1.6,
					}}>
					{this.orderDetail.port_of_discharge}
				</IText>
			</IView>
		)
	}
	line = () => {
		return <IView style={{ height: 9, backgroundColor: '#f2f1f1' }}></IView>
	}
	renderBottomButton = () => {
		return (
			<IView
				style={{
					height: 69,
					backgroundColor: 'white',

					flexDirection: 'row',
					alignItems: 'center',
					paddingHorizontal: 18,
				}}>
				<ITouch
					style={{
						height: 40,
						width: '100%',
						backgroundColor: 'black',
						justifyContent: 'center',
						alignItems: 'center',
					}}
					onPress={() => {
						NavigationService.navigate(
							'alert',
							AlertScreen.newInstance(
								'Adding this item will clear your current basket. Proceed?',
								null,
								async () => {
									await APIService.reOrder(this.orderID)
									EventEmitter.emit('setNumberCart')
									EventEmitter.emit('getCart')
									NavigationService.navigate('order')
								},
								'Proceed to Start New Basket?',
							),
						)
					}}>
					<IText
						style={{
							fontSize: 16,
							fontFamily: 'normal',
							letterSpacing: 1.6,
							color: 'white',
						}}>
						RE-ORDER
					</IText>
				</ITouch>
			</IView>
		)
	}
	renderOrderSummary = () => {
		return (
			<IView
				style={{
					paddingHorizontal: 18,
					paddingVertical: 10,
					backgroundColor: '#f2f1f1',
				}}>
				<IText
					style={{
						fontSize: 14,
						letterSpacing: 1.6,
						fontWeight: '500',
					}}>
					Order Summary
				</IText>
				<IFLatList
					style={{}}
					data={this.orderDetail.products}
					renderItem={({ item, index }) => (
						<RowCheckOut item={item}></RowCheckOut>
					)}
					extraData={this.state.isRefresh}
					keyExtractor={(item, index) => index.toString()}
					showsVerticalScrollIndicator={false}
					scrollEnabled={false}
				/>
				<IView
					style={{
						flexDirection: 'row',
						paddingTop: 15,
						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					<IText
						style={{
							fontSize: 16,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						Total Payment
					</IText>
					<IText
						style={{
							fontSize: 20,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}
						isSo={true}>
						{priceFormat(this.orderDetail.total_payment)}
					</IText>
				</IView>
				<IView
					style={{
						flexDirection: 'row',

						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					<IText
						style={{
							fontSize: 14,
							letterSpacing: 1.6,
							fontWeight: '500',
							paddingVertical: 10,
						}}>
						First payment (deposit)
					</IText>
					<IText
						style={{
							fontSize: 14,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}
						isSo={true}>
						{priceFormat(this.orderDetail.deposit)}
					</IText>
				</IView>

				<IView
					style={{
						flexDirection: 'row',
						paddingBottom: 15,
						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					<IText
						style={{
							fontSize: 14,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						Final payment
					</IText>
					<IText
						style={{
							fontSize: 14,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}
						isSo={true}>
						{priceFormat(this.orderDetail.final_payment)}
					</IText>
				</IView>
				<IView
					style={{
						height: 1,
						backgroundColor: '#C5C5C5',
					}}></IView>
				<IView
					style={{
						flexDirection: 'row',
						paddingVertical: 15,
						justifyContent: 'space-between',
						alignItems: 'center',
					}}>
					<IText
						style={{
							fontSize: 16,
							letterSpacing: 1.6,
							fontWeight: '500',
						}}>
						Total CBM
					</IText>
					<IText
						style={{
							fontSize: 16,
							letterSpacing: 1.6,
						}}
						isSo={true}>
						{this.orderDetail.total_cbm}
					</IText>
				</IView>
			</IView>
		)
	}

	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: '#f2f1f1' }}>
				<IToolbar title={'Order Detail'} notSearch='true' />
				{this.state.isRefresh ? (
					<ScrollView style={{ backgroundColor: 'white' }}>
						{this.orderDetail.forwarder !== '' &&
						this.orderDetail.port_of_discharge !== ''
							? this.renderDeliveryInfo()
							: null}
						{this.line()}
						<IView
							style={{
								paddingHorizontal: 18,
								backgroundColor: 'white',
								height: 40,
								alignItems: 'center',
								justifyContent: 'space-between',
								flexDirection: 'row',
							}}>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
								}}>
								Order ID
							</IText>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '600',
									letterSpacing: 1.6,
								}}
								isSo={true}>
								{this.orderDetail.code}
							</IText>
						</IView>
						{this.line()}
						<IView
							style={{
								paddingHorizontal: 18,
								backgroundColor: 'white',
								height: 40,
								alignItems: 'center',
								justifyContent: 'space-between',
								flexDirection: 'row',
							}}>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
								}}>
								Parcel
							</IText>
							<IText
								style={{
									fontSize: 12,
									fontWeight: '600',
									letterSpacing: 1.6,
									color: '#3290BE',
								}}
								isSo={true}>
								{this.orderDetail.parcel}
							</IText>
						</IView>
						{this.line()}
						{this.renderOrderSummary()}
					</ScrollView>
				) : null}

				{this.renderBottomButton()}
			</IView>
		)
	}
}

export default OrderDetailScreen
