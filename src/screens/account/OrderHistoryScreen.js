import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	IFLatList,
	ITouch,
} from '../../components'
import { images } from '../../../assets'
import BaseScreen from '../BaseScreen'
import { APIService, NavigationService } from '../../services'
import { dateFormat } from '../../utils'
import AlertScreen from '../common/AlertScreen'
import { priceFormat } from '../../utils'
import FourImageThumbnails from '../row/FourImageThumbnails'
import IScrollView from '../../components/IScrollView'
class OrderHistoryScreen extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			isRefresh: false,
		}
		this.orderList = []
		this.page = 1
	}
	componentDidMount() {
		this.getOrderHistory()
	}

	getOrderHistory() {
		APIService.getOrderHistory(this.page).then(data => {
			if (data != null) {
				this.orderList = data.orders
				this.setState({ isRefresh: true })
			}
		})
	}
	loadMore() {
		APIService.getOrderHistory(this.page, false).then(data => {
			if (data != null) {
				this.orderList = this.orderList.concat(data.orders)
				this.setState({
					isRefresh: true,
				})
			}
		})
	}
	renderItem = data => {
		return (
			<IView
				style={{
					backgroundColor: 'white',
					height: 108,
					borderRadius: 8,
					paddingLeft: 13,
					paddingRight: 10,
					paddingVertical: 12,
					flexDirection: 'row',
					marginBottom: 18,
				}}>
				<IView
					style={{
						width: 80,
						height: 80,
					}}>
					<FourImageThumbnails
						thumbs={data.item.thumbs}></FourImageThumbnails>
				</IView>

				<IView style={{ flex: 1, marginLeft: 12 }}>
					<IView style={{ flex: 1 }}>
						<IView
							style={{
								flexDirection: 'row',
								justifyContent: 'space-between',
							}}>
							<IText
								style={{
									flex: 1,
									fontSize: 14,
									fontWeight: '500',
								}}>
								{data.item.collection_name}
							</IText>
							<IText
								style={{
									fontSize: 12,
									letterSpacing: 0.8,
								}}
								isSo={true}>
								{dateFormat(data.item.time_created, true)}
							</IText>
						</IView>
						<IView
							style={{
								flexDirection: 'row',
								justifyContent: 'space-between',
								marginTop: 1,
							}}>
							<IText
								style={{
									flex: 1,
									fontSize: 12,
									letterSpacing: 1.6,
								}}>
								<IText style={{}} isSo={true}>
									{data.item.total_Item + ' Items'}
								</IText>
							</IText>
							<IText
								style={{
									fontSize: 12,
									letterSpacing: 0.8,
								}}
								isSo={true}>
								{dateFormat(data.item.time_created)}
							</IText>
						</IView>
					</IView>
					<IView
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							alignItems: 'flex-end',
						}}>
						<IText
							style={{
								flex: 1,
								fontSize: 14,
								letterSpacing: 1.6,
								paddingRight: 20,
							}}
							isSo={true}
							numberOfLines={1}>
							{priceFormat(data.item.total_payment)}
						</IText>
						<ITouch
							style={{
								backgroundColor: 'black',
								height: 26,
								width: 95,
								alignItems: 'center',
								justifyContent: 'center',
								flexDirection: 'row',
							}}
							onPress={() => {
								NavigationService.navigate(
									'OrderDetailScreen',
									{
										orderID: data.item.id,
									},
								)
							}}>
							<IText
								style={{
									color: 'white',
									fontSize: 12,
									letterSpacing: 1.6,
								}}>
								Details
							</IText>
							<IFastImage
								style={{
									width: 16,
									height: 9,
									marginLeft: 9,
								}}
								source={images.ic_nextwhite}
								resizeMode='contain'
							/>
						</ITouch>
					</IView>
				</IView>
			</IView>
		)
	}

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: '#f2f1f1' }}>
				<IToolbar title={'Order History'} notSearch='true' />
				<IScrollView
					onBottom={() => {
						this.page++
						this.loadMore()
					}}>
					<IFLatList
						style={{ flex: 1 }}
						contentContainerStyle={{
							paddingHorizontal: 18,
						}}
						data={this.orderList}
						extraData={this.state.isRefresh}
						keyExtractor={(item, index) => index + ''}
						renderItem={this.renderItem}
						showsVerticalScrollIndicator={false}
					/>
				</IScrollView>
			</View>
		)
	}
}

export default OrderHistoryScreen
