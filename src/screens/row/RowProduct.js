import React, { Component } from "react";
import {
  IView,
  IFastImage,
  ITouch,
  INotity,
  ICart,
  IText,
  ITextInput,
  IFLatList
} from "../../components";
import { colors, images, strings } from "../../../assets";
import AutoHeightImage from "react-native-scalable-image";
import { withNavigation } from "react-navigation";
import BaseScreen from "../BaseScreen";
import { NavigationService, APIService } from "../../services";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import AlertScreen from "../common/AlertScreen";
import { priceFormat } from "../../utils";
import FastImage from "react-native-fast-image";
import EventEmitter from "react-native-eventemitter";
class RowProduct extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
      checkSave: false,
      isRefreshRow: true
    };
  }
  componentDidMount() {
    EventEmitter.on("update_like", this.updateLike);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("update_like", this.updateLike);
  }
  updateLike = listID => {
    const { item } = this.props;
    listID.forEach(id => {
      if (id == item.id) {
        if (item.like === 1) {
          item.like = 0;
        } else {
          item.like = 1;
        }
        this.setState({ isRefreshRow: true });
        return;
      }
    });
  };
  render() {
    const {
      item,
      index,
      isCheck,
      width = (this.width - 54 - 80) / 2,
      isCollection = false,
      isLike = true
    } = this.props;
    const { isLogin } = this.props;
    return this.state.isRefreshRow ? (
      <ITouch
        style={{ flex: 1 }}
        onPress={index => {
          isLogin
            ? NavigationService.push("DetailProductScreen", {
                product_id: item.id
              })
            : NavigationService.navigate(
                "alert",
                AlertScreen.newInstance("Login required", -2)
              );
        }}
      >
        <IView>
          <IView
            style={{
              backgroundColor: colors.white,
              padding: 20,
              borderRadius: 10
            }}
          >
            <AutoHeightImage
              width={width}
              source={{ uri: item.image || "" }}
              style={{}}
              onLoad={() => {
                this.setState({ isShow: true });
              }}
            ></AutoHeightImage>
          </IView>
          {isCheck === true ? (
            <IView
              style={{
                position: "absolute",
                top: 8,
                right: 7,
                zIndex: 99
              }}
            >
              <ITouch
                onPress={() => {
                  item.chose = !item.chose;
                  EventEmitter.emit("onOffBtn");
                  this.setState({});
                  console.log(item.chose);
                }}
              >
                <IFastImage
                  source={item.chose ? images.check_save : images.uncheck_save}
                  resizeMode="contain"
                  style={{
                    width: 20,
                    height: 20
                  }}
                ></IFastImage>
              </ITouch>
            </IView>
          ) : null}
          {this.state.isShow === true ? (
            <IView
              style={{
                position: "absolute",
                bottom: 5,
                top: 5,
                left: 5,
                right: 5,
                justifyContent: "space-between"
              }}
            >
              <IFastImage
                source={images.p_new}
                resizeMode="contain"
                style={{ width: 37, height: 19 }}
              ></IFastImage>
              {isLike ? (
                <IView style={{ alignItems: "flex-end" }}>
                  <ITouch
                    style={{ width: 16, height: 14 }}
                    onPress={async () => {
                      var dataLike = await APIService.productLike([item.id]);
                      if (dataLike.fail_list.length === 0) {
                        this.onEventUpdateWhenLikeProduct([item.id]);
                      }
                    }}
                  >
                    <IFastImage
                      source={
                        item.like === 1 ? images.p_timden : images.p_timtrang
                      }
                      resizeMode="contain"
                      style={{ flex: 1 }}
                    ></IFastImage>
                  </ITouch>
                </IView>
              ) : null}
            </IView>
          ) : null}
        </IView>

        <IText
          style={{
            fontSize: 14,
            lineHeight: 16,
            paddingVertical: 10,
            fontWeight: "500",
            letterSpacing: 1.6
          }}
        >
          {item.name}
        </IText>
        {item.price === 0 ? null : (
          <IText
            style={{
              fontSize: 14,
              lineHeight: 18,
              paddingBottom: 10
            }}
            isSo={true}
          >
            {priceFormat(item.price)}
          </IText>
        )}
      </ITouch>
    ) : null;
  }
}
export default RowProduct;
