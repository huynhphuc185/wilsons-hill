import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage, Platform } from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  ITouch
} from "../../components";
import { images } from "../../../assets";
import { withNavigation } from "react-navigation";
import { NavigationService } from "../../services";
import { dateFormat } from "../../utils";
class RowPreviousOrder extends Component {
  constructor(props) {
    super(props);
    this.state = { isLeft: true };
  }
  render() {
    const { data } = this.props;
  
    return (
      <ITouch
        style={{
          height: 114,
          width: "100%",
          backgroundColor: "white",
          borderRadius: 10,
          flexDirection: "row",
          marginBottom: 18
        }}
        onPress={() => {
          NavigationService.navigate("OrderDetailScreen", {
            orderID: data.id
          });
        }}
      >
        <IView style={{ flex: 3, paddingVertical: 10 }}>
          <IFastImage
            source={{ uri: data.image[0] }}
            resizeMode="contain"
            style={{ flex: 1 }}
          ></IFastImage>
        </IView>
        <IView
          style={{
            flex: 2,
            alignItems: "flex-end",
            marginRight: 17
            // justifyContent: "flex-end"
          }}
        >
          <IText
            style={{
              marginTop: 13,
              fontSize: 24,
              fontWeight: "500",
              letterSpacing: 1.6,
              lineHeight: 28
            }}
          >
            {data.name}
          </IText>
          <IText
            style={{
              fontSize: 14,
              letterSpacing: 1.6,
              lineHeight: 16
            }}
          >
            COLLECTION
          </IText>
          <IView
            style={{
              marginTop: 15,
              borderWidth: 0.5,
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              paddingHorizontal: 8,
              paddingVertical: 2
            }}
          >
            <IText style={{ fontSize: 12 }}>
              {dateFormat(data.create_time)}
            </IText>
          </IView>
        </IView>
      </ITouch>
    );
  }
}

export default withNavigation(RowPreviousOrder);
