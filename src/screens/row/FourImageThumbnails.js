import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	ITouch,
} from '../../components'
import { images } from '../../../assets'
import { withNavigation } from 'react-navigation'
import { NavigationService } from '../../services'
import { priceFormat } from '../../utils'
import IFlatList from '../../components/IFLatList'
class FourImageThumbnails extends Component {
	constructor(props) {
		super(props)
	}
	render() {
		const { thumbs } = this.props
		return (
			<IView style={{ flex: 1, borderRadius: 10, overflow: 'hidden' }}>
				<IView style={{ flex: 1, flexDirection: 'row' }}>
					<IFastImage
						style={{ flex: 1 }}
						source={{ uri: thumbs[0] }}></IFastImage>
                        <IView style={{width:1}}></IView>
					<IFastImage
						style={{ flex: 1 }}
						source={{ uri: thumbs[1] }}></IFastImage>
				</IView>
                <IView style={{height:1}}></IView>
				<IView style={{ flex: 1, flexDirection: 'row' }}>
					<IFastImage
						style={{ flex: 1 }}
						source={{ uri: thumbs[2] }}></IFastImage>
                        <IView style={{width:1}}></IView>
					<IFastImage
						style={{ flex: 1 }}
						source={{ uri: thumbs[3] }}></IFastImage>
				</IView>
			</IView>
		)
	}
}

export default withNavigation(FourImageThumbnails)
