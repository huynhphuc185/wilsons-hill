import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage, Platform } from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  ITouch
} from "../../components";
import { images, colors } from "../../../assets";
import { withNavigation } from "react-navigation";
import { NavigationService } from "../../services";
import AutoHeightImage from "react-native-scalable-image";
import BaseScreen from "../BaseScreen";
import EventEmitter from "react-native-eventemitter";
class RowCollection extends BaseScreen {
  constructor(props) {
    super(props);
    this.data = this.props.data || {};
  }
  render() {
    const { isCheckSave } = this.props;
    return (
      <ITouch
        style={{
          flex: 1,
          backgroundColor: colors.white
        }}
        onPress={() => {
          NavigationService.navigate("CollectionDetailScreen", {
            collection_id: this.data.id
          });
        }}
      >
        {isCheckSave === true ? (
          <IView
            style={{
              position: "absolute",
              top: 3,
              right: 18,
              zIndex: 99
            }}
          >
            <ITouch
              onPress={() => {
                this.data.chose = !this.data.chose;
                EventEmitter.emit("onOffBtnCollection");
                this.setState({});
              }}
            >
              <IFastImage
                source={
                  this.data.chose ? images.check_save : images.uncheck_save
                }
                resizeMode="contain"
                style={{
                  width: 20,
                  height: 20
                }}
              ></IFastImage>
            </ITouch>
          </IView>
        ) : null}
        {/* <IFastImage
					source={{ uri: this.data.image }}
					resizeMode='contain'
					style={{
						height: 200,
						backgroundColor: 'red',
					}}></IFastImage> */}

        <AutoHeightImage
          width={this.width}
          source={{ uri: this.data.image || "" }}
          style={{}}
        ></AutoHeightImage>
      </ITouch>
    );
    isch;
  }
}

export default withNavigation(RowCollection);
