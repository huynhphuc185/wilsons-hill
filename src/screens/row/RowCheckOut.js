import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import {
	IImage,
	IToolbar,
	IView,
	IFastImage,
	IText,
	ITouch,
} from '../../components'
import { images } from '../../../assets'
import { withNavigation } from 'react-navigation'
import { NavigationService } from '../../services'
import { priceFormat } from '../../utils'
class RowCheckOut extends Component {
	constructor(props) {
		super(props)
	}
	render() {
		const { item } = this.props
		var totalTong = item.price * item.quantity
		return (
			<IView
				style={{
					paddingVertical: 18,
					borderBottomWidth: 1,
					borderColor: '#C5C5C5',
				}}>
				<IView
					style={{
						flexDirection: 'row',
						flex:1
					}}>
					<IView
						style={{
							padding: 5,
							width: 92,
							height: 86,
							backgroundColor: 'white',
							borderRadius: 10,
						}}>
						<IFastImage
							source={{ uri: item.thumb }}
							style={{ flex: 1 }}
							resizeMode='contain'></IFastImage>
					</IView>
					<IView style={{ flex: 1, marginLeft: 10 }}>
						<IText
							style={{
								fontSize: 14,
								letterSpacing: 1.6,
								fontWeight: '500',
							}}>
							{item.collection_name}
						</IText>
						<IView
							style={{
								flexDirection: 'row',
								justifyContent: 'space-between',
                                alignItems: 'center',
                                marginTop: 8,
							}}>
							<IText
								numberOfLines={1}
								style={{
									fontSize: 14,
									letterSpacing: 1.6,
									fontWeight: '500',
									flex: 1,
								}}>
								{item.name}
							</IText>
							<IText
								numberOfLines={1}
								style={{
									paddingLeft: 10,
									fontSize: 14,
									letterSpacing: 1.6,
									maxWidth: 90,
								
								}}
								isSo = {true}>
								{priceFormat(item.price)}
							</IText>
						</IView>
						<IView
							style={{
								flexDirection: 'row',
								justifyContent: 'space-between',
								alignItems: 'center',
								marginTop: 8,
							}}>
							<IText
								style={{
									fontSize: 12,
									fontWeight: '500',
									letterSpacing: 1.6,
								}}>
								Quantily:{' '}
								<IText style={{ }}isSo = {true}>
									{item.quantity}
								</IText>
							</IText>
							<IText
								style={{
									fontSize: 12,
									fontWeight: '500',
									letterSpacing: 1.6,
								}}>
								CBM:{' '}
								<IText style={{  }}isSo = {true}>
									{item.cubic_meter}
								</IText>
							</IText>
						</IView>
						<IView
							style={{
								flex: 1,
								justifyContent: 'flex-end',
								alignItems: 'flex-end',
								 marginTop: 8
							}}>
							<IText
								style={{
									fontSize: 14,
									fontWeight: '500',
									letterSpacing: 1.6,
									
								}}isSo = {true}>
								{priceFormat(totalTong)}
							</IText>
						</IView>
					</IView>
				</IView>
			</IView>
		)
	}
}

export default withNavigation(RowCheckOut)
