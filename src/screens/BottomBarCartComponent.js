import React, { Component } from "react";
import { ActivityIndicator, View, StyleSheet, Platform } from "react-native";
import { IView, IText, IFastImage } from "../components/";
import { colors, images } from "../../assets";
import EventEmitter from "react-native-eventemitter";
import { APIService } from "../services";
class BottomBarCartComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0
    };
  }
  setNumberCart = async () => {
    const data = await APIService.getCartNumber();
    this.setState({ number: data.number });
  };
  componentDidMount() {
    this.setNumberCart();

    EventEmitter.on("setNumberCart", this.setNumberCart);
  }

  componentWillUnmount() {
    EventEmitter.removeListener("setNumberCart", this.setNumberCart);
  }
  render() {
    const focused = this.props.focused;
    return (
      <IView>
        <IFastImage
          resizeMode="contain"
          source={focused ? images.cartblack : images.cartwhite}
          style={{ width: 25, height: 25 }}
        />
        {this.state.number === 0 ? null : (
          <IView
            style={{
              position: "absolute",
              backgroundColor: "red",
              width: 16,
              height: 16,
              borderRadius: 16 / 2,
              top: 0,
              right: -8,
              borderColor: colors.white,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <IText
              style={{
                color: colors.white,
                fontSize: 9,
                fontFamily: "bold"
              }}
              fontWeight="900"
            >
              {this.state.number}
            </IText>
          </IView>
        )}
      </IView>
    );
  }
}

export default BottomBarCartComponent;
