import React, { Component } from 'react'
import { View, Text, StyleSheet, AsyncStorage, Platform } from 'react-native'
import BaseScreen from '../BaseScreen'
import { IToolbar, IView, IText, ITouch, IFastImage } from '../../components'
import { colors, images } from '../../../assets'
import RowPreviousOrder from '../row/RowPreviousOrder'
import DynamicFlatlistProduct from '../home/DynamicFlatlistProduct'
import { APIService, NavigationService } from '../../services'
import { ScrollView } from 'react-native-gesture-handler'
import IFlatList from '../../components/IFLatList'
import IScrollView from '../../components/IScrollView'
class PreviousOrders extends BaseScreen {
	constructor(props) {
		super(props)
		this.state = {
			dataPreviousOrders: null,
		}
		this.page = 1
	}

	componentDidMount() {
		APIService.getPreviousOrders(this.page).then(data => {
			if (data != null) {
				this.setState({
					dataPreviousOrders: data.orders,
				})
			}
		})
	}
	loadMore() {
		APIService.getPreviousOrders(this.page).then(data => {
			if (data != null) {
				this.state.dataPreviousOrders = this.state.dataPreviousOrders.concat(
					data.orders,
				)
				this.setState({
					dataPreviousOrders: this.state.dataPreviousOrders,
				})
			}
		})
	}
	render() {
		return (
			<IView style={{ flex: 1, backgroundColor: colors.bg }}>
				<IToolbar title={'Previous Orders'} />
				{this.state.dataPreviousOrders ? (
					<IScrollView
						style={{ paddingHorizontal: 18 }}
						onBottom={() => {
							this.page++
							this.loadMore()
						}}>
						<IFlatList
							data={this.state.dataPreviousOrders}
							renderItem={({ item, index }) => (
								<RowPreviousOrder
									data={item}
									index={index}></RowPreviousOrder>
							)}
							extraData={this.state.dataPreviousOrders}
							keyExtractor={(item, index) => index.toString()}
							showsVerticalScrollIndicator={false}
							ListEmptyComponent={() => (
								<IView
									style={{
										padding: 32,
										alignItems: 'center',
										justifyContent: 'center',
									}}>
									<IText
										style={{
											//   color: colors.gray._400,
											fontSize: 16,
										}}>
										No items
									</IText>
								</IView>
							)}
						/>
					</IScrollView>
				) : null}
			</IView>
		)
	}
}

export default PreviousOrders
