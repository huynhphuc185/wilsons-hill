import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage, Platform } from "react-native";
import BaseScreen from "../BaseScreen";
import { IToolbar, IView, IText, ITouch, IFastImage } from "../../components";
import { colors, images } from "../../../assets";
import RowCollection from "../row/RowCollection";
import DynamicFlatlistProduct from "../home/DynamicFlatlistProduct";
import { APIService, NavigationService } from "../../services";
import { ScrollView } from "react-native-gesture-handler";
import EventEmitter from "react-native-eventemitter";
import AutoHeightImage from "react-native-scalable-image";
class MyCatalogueScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      dataMyCatalogue: null
    };
  }

  componentDidMount() {
    this.getMyCatalogue();
    EventEmitter.on("update_catalogue", this.getMyCatalogue);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("update_catalogue", this.getMyCatalogue);
  }

  getMyCatalogue = () => {
    APIService.getMyCatalogue().then(data => {
      if (data != null) {
        this.setState({
          dataMyCatalogue: data.catalogue
        });
        this._flatList.refreshProductList(this.state.dataMyCatalogue.products);
      }
    });
  };

  renderImage(listOrders) {
    return (
      <IView
        style={{
          backgroundColor: "white",
          height: 164,
          flexDirection: "row",
          borderRadius: 10
        }}
      >
        {/* item lon */}
        <IView style={{ flex: 1 }}>
          <IFastImage
            source={{ uri: listOrders[0].image }}
            resizeMode="contain"
            style={{ flex: 1 }}
          ></IFastImage>
        </IView>
        {/* 4 item nho */}
        <IView style={{ flex: 1 }}>
          <IView style={{ flex: 1, flexDirection: "row" }}>
            <IView style={{ flex: 1 }}>
              <IFastImage
                source={{ uri: listOrders[1].image }}
                resizeMode="contain"
                style={{ flex: 1 }}
              ></IFastImage>
            </IView>
            <IView style={{ flex: 1 }}>
              <IFastImage
                source={{ uri: listOrders[2].image }}
                resizeMode="contain"
                style={{ flex: 1 }}
              ></IFastImage>
            </IView>
          </IView>
          <IView style={{ flex: 1, flexDirection: "row" }}>
            <IView style={{ flex: 1 }}>
              <IFastImage
                source={{ uri: listOrders[3].image }}
                resizeMode="contain"
                style={{ flex: 1 }}
              ></IFastImage>
            </IView>
            <IView style={{ flex: 1 }}>
              <IFastImage
                source={{ uri: listOrders[4].image }}
                resizeMode="contain"
                style={{ flex: 1 }}
              ></IFastImage>
            </IView>
          </IView>
        </IView>
      </IView>
    );
  }

  renderTitle = (text, callBack) => {
    return (
      <IView
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          // marginVertical: 12,
          height: 23
        }}
      >
        <IText
          style={{
            fontSize: 14,
            fontWeight: "500",
            letterSpacing: 1.6,
            color: colors.text.black
          }}
        >
          {text}
        </IText>
        {callBack != null ? (
          <ITouch
            style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row"
            }}
            onPress={() => {
              callBack();
            }}
          >
            <IText
              style={{
                fontSize: 14,
                marginRight: 11,
                color: colors.text.black
              }}
            >
              View all
            </IText>
            <IFastImage
              source={images.vector}
              style={{ width: 16, height: 9 }}
            ></IFastImage>
          </ITouch>
        ) : null}
      </IView>
    );
  };
  line() {
    return (
      <IView
        style={{
          backgroundColor: "#C5C5C5",
          height: 1
        }}
      ></IView>
    );
  }

  renderPreviousOrser() {
    console.log(this.state.dataMyCatalogue.orders);

    return this.state.dataMyCatalogue.orders.length != 0 ? (
      <ITouch
        style={{ paddingHorizontal: 18 }}
        onPress={() => {
          NavigationService.navigate("PreviousOrders");
        }}
      >
        {this.renderTitle("Previous Orders", () => {
          NavigationService.navigate("PreviousOrders");
        })}
        <IView style={{ marginTop: 12, marginBottom: 16 }}>
          {this.renderImage(this.state.dataMyCatalogue.orders)}
        </IView>
        {this.line()}
      </ITouch>
    ) : (
      <IView style={{ paddingHorizontal: 18 }}>
        {this.renderTitle("Previous Orders")}
        <IText
          style={{
            marginTop: 12,
            fontSize: 12,
            fontWeight: "300",
            marginBottom: 42
          }}
        >
          All of your orders ever made can be found here, makingg it very easy &
          quick to re-order.
        </IText>
        {this.line()}
      </IView>
    );
  }
  renderSavedCollections() {
    return this.state.dataMyCatalogue.collection ? (
      <ITouch
        style={{ marginTop: 15 }}
        onPress={() => {
          NavigationService.navigate("SavedCollections");
        }}
      >
        <IView style={{ paddingHorizontal: 18 }}>
          {this.renderTitle("Saved Collections", () => {
            NavigationService.navigate("SavedCollections");
          })}
        </IView>
        <IView style={{ marginTop: 8, marginBottom: 17 }}>
          <AutoHeightImage
            width={this.width}
            source={{ uri: this.state.dataMyCatalogue.collection.image || "" }}
            style={{}}
          ></AutoHeightImage>
        </IView>
        <IView style={{ paddingHorizontal: 18 }}>{this.line()}</IView>
      </ITouch>
    ) : (
      <IView style={{ marginTop: 15, paddingHorizontal: 18 }}>
        {this.renderTitle("Saved Collections")}
        <IText
          style={{
            marginTop: 12,
            fontSize: 12,
            fontWeight: "300",
            marginBottom: 40,
            letterSpacing: 1.6
          }}
        >
          All ofthe collections that you saved for later can be found here
        </IText>
        {this.line()}
      </IView>
    );
  }

  renderSavedItems() {
    console.log(this.state.dataMyCatalogue.products.length);
    console.log(this.state.dataMyCatalogue);

    return this.state.dataMyCatalogue.products.length != 0 ? (
      <IView style={{ marginTop: 14 }}>
        <IView style={{ paddingHorizontal: 18, marginBottom: 15 }}>
          {this.renderTitle("Saved Items", () => {
            NavigationService.navigate("SaveItems");
          })}
        </IView>
        <DynamicFlatlistProduct
          listProduct={this.state.dataMyCatalogue.products}
          noLike={true}
          ref={ref => {
            this._flatList = ref;
          }}
        ></DynamicFlatlistProduct>
        <IView style={{ paddingHorizontal: 18 }}>{this.line()}</IView>
      </IView>
    ) : (
      <IView style={{ paddingHorizontal: 18, marginTop: 15 }}>
        {this.renderTitle("Saved Items")}
        <IText
          style={{
            marginTop: 12,
            fontSize: 12,
            fontWeight: "300",
            marginBottom: 40,
            letterSpacing: 1.6
          }}
        >
          All ofthe items that you saved for later can be found here
        </IText>
        {this.line()}
      </IView>
    );
  }

  suggestCollection() {
    return this.state.dataMyCatalogue.suggest_collection ? (
      <IView style={{ marginTop: 14 }}>
        <IText
          style={{
            fontWeight: "300",
            letterSpacing: 1.6,
            paddingHorizontal: 18,
            lineHeight: 14,
            fontSize: 12,
            marginBottom: 15
          }}
        >
          Because you bought{" "}
          <IText style={{ fontWeight: "500" }}>
            “{this.state.dataMyCatalogue.suggest_collection.name}”
          </IText>
          {"\n"}you should check these:
        </IText>
        <RowCollection
          data={this.state.dataMyCatalogue.suggest_collection}
        ></RowCollection>
        <IView style={{ paddingHorizontal: 18, marginTop: 18 }}>
          {this.line()}
        </IView>
      </IView>
    ) : null;
  }
  newCollection() {
    return this.state.dataMyCatalogue.collection_new ? (
      <IView style={{ marginTop: 14 }}>
        <IText
          style={{
            paddingHorizontal: 18,
            marginBottom: 14,
            fontWeight: "500",
            fontSize: 14,
            lineHeight: 16,
            letterSpacing: 1.6
          }}
        >
          New Collection
        </IText>
        <RowCollection
          data={this.state.dataMyCatalogue.collection_new}
        ></RowCollection>
      </IView>
    ) : null;
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={"My Catalogue"} notBack={true} />
        {this.state.dataMyCatalogue != null ? (
          <ScrollView>
            <IView>
              {this.renderPreviousOrser()}
              {this.renderSavedCollections()}
              {this.renderSavedItems()}
              {this.suggestCollection()}
              {this.newCollection()}
            </IView>
          </ScrollView>
        ) : null}
      </View>
    );
  }
}

export default MyCatalogueScreen;
