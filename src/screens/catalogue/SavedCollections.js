import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  Animated
} from "react-native";
import BaseScreen from "../BaseScreen";
import { IToolbar, IView, IText, ITouch, IFastImage } from "../../components";
import { colors, images } from "../../../assets";
import RowCollection from "../row/RowCollection";
import { constraintLayout } from "../../components/ILayoutProps";
import DynamicFlatlistProduct from "../home/DynamicFlatlistProduct";
import { APIService, NavigationService } from "../../services";
import { ScrollView } from "react-native-gesture-handler";
import IFlatList from "../../components/IFLatList";
import ICombobox from "../../components/ICombobox";
import AlertScreen from "../common/AlertScreen";
import EventEmitter from "react-native-eventemitter";
import IScrollView from "../../components/IScrollView";

class SavedCollections extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(2),
      bounceValueTop: new Animated.Value(90),
      dataSaveCollections: null,
      textSelect: "Select",
      listSort: ["Room 1", "Room 2", "Room 3", "Room 4"],
      isRefresh: false,
      listChose: [],
      updateButton: true
    };
    this.indexSort = 0;
    this.isHidden = true;
    this.page = 1;
  }

  componentDidMount() {
    this.getSaveCollections();
    EventEmitter.on("update_catalogue", this.getSaveCollections);
    EventEmitter.on("onOffBtnCollection", this.updateButton);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("update_catalogue", this.getSaveCollections);
    EventEmitter.removeListener("onOffBtnCollection", this.updateButton);
  }

  updateButton = () => {
    const result = this.state.dataSaveCollections.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    if (result.length != 0) {
      this.setState({
        updateButton: false
      });
    } else {
      this.setState({
        updateButton: true
      });
    }
  };

  getSaveCollections = () => {
    APIService.getSaveCollections().then(data => {
      if (data != null) {
        data.collection.map((item, index) => {
          item.chose = false;
        });
        this.setState({
          dataSaveCollections: data.collection
        });
      }
    });
  };
  loadMore() {
    APIService.getSaveCollections(this.page).then(data => {
      if (data != null) {
        data.collection.map((item, index) => {
          item.chose = false;
        });
        this.state.dataSaveCollections = this.state.dataSaveCollections.concat(
          data.collection
        );
        this.setState({
          dataSaveCollections: this.state.dataSaveCollections
        });
      }
    });
  }
  addCollectionToCart = async () => {
    var listChose = this.filterChose();
    await APIService.addCollectionToCart(listChose);
    EventEmitter.emit("setNumberCart");
    EventEmitter.emit("getCart");
    NavigationService.navigate(
      "alert",
      AlertScreen.newInstance("Add collection successfully!", 9999, () => {
        this._toggleSubview();
        NavigationService.pop();
      })
    );
  };

  unsaveCollection = async () => {
    var listChose = this.filterChose();
    console.log(listChose);
    const saveCollection = await APIService.saveCollection(listChose, 0);
    console.log(saveCollection.fail_list.length);
    if (saveCollection.fail_list.length === 0) {
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance(
          "Remove selected items from saved successfully!",
          9999,
          () => {
            EventEmitter.emit("update_catalogue");
            this._toggleSubview();
            NavigationService.pop();
          }
        )
      );
    }
  };

  filterChose() {
    const result = this.state.dataSaveCollections.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    var listChose = [];
    result.map((item, index) => {
      listChose.push(item.id);
    });
    // this.setState({ listChose: listChose });
    return listChose;
  }

  renderButtonBottom() {
    return (
      <Animated.View
        style={{
          height: this.state.bounceValue,
          backgroundColor: "white",
          shadowOffset: { width: 0, height: -1 },
          shadowOpacity: 0.5,
          elevation: 20,
  
        }}
      >
        <ITouch
          style={{
            height: 40,
            width: 343,
            backgroundColor: "black",
            marginLeft: 18,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 18
          }}
          disabled={this.state.updateButton}
          onPress={() => {
            this.filterChose();
            NavigationService.navigate(
              "alert",
              AlertScreen.newInstance("Coming soon!")
            );
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              letterSpacing: 1.6,
              color: "white"
            }}
          >
            ADD SELECTED ITEMS TO CART
          </IText>
        </ITouch>
        <IView
          style={{
            height: 32,
            alignItems: "center",
            marginTop: 9
            // justifyContent: "flex-end"
          }}
        >
          <ITouch
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "black"
            }}
            disabled={this.state.updateButton}
            onPress={() => {
              this.unsaveCollection();
            }}
          >
            <IText style={{ fontSize: 12 }}>
              Remove selected items from saved
            </IText>
          </ITouch>
        </IView>
      </Animated.View>
    );
  }

  _toggleSubview() {
    this.setState({
      textSelect: !this.isHidden ? "Select" : "Cancel"
    });

    var toValue = 2;
    var toValue2 = 90;

    if (this.isHidden) {
      toValue = 90;
      toValue2 = 2;
    }
    Animated.spring(this.state.bounceValue, {
      toValue: toValue,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    Animated.spring(this.state.bounceValueTop, {
      toValue: toValue2,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    this.isHidden = !this.isHidden;
  }

  //   getDataStringCombobox = listSort => {
  //     var listString = [];
  //     listSort.map((item, index) => {
  //       listString.push(item.name);
  //     });
  //     return listString;
  //   };

  renderSortBy = callBack => {
    return (
      <IView
        style={{
          flexDirection: "row",
          // justifyContent: "flex-end",
          alignItems: "center",
          paddingHorizontal: 18,
          //   paddingVertical: 16,
          marginBottom: 10,
          backgroundColor: "#f2f1f1"
        }}
      >
        <IView style={{ flex: 1 }}></IView>
        {/* <IView
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <IView
            style={{
              height: 40,
              width: 270,
              borderRadius: 10,
              backgroundColor: colors.white
            }}
          >
            <IView
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ICombobox
                widthItem={constraintLayout(270)}
                defaultValue={this.state.listSort[this.indexSort]}
                // listValue={this.getDataStringCombobox(this.state.listSort)}
                listValue={this.state.listSort}
                callBack={index => {
                  this.indexSort = index;
                  //   this._getListProduct();
                }}
              />
            </IView>
          </IView>
        </IView> */}
        <ITouch
          style={{ alignItems: "center" }}
          onPress={() => {
            this._toggleSubview();
            this.setState({ isRefresh: true });
          }}
        >
          <IText
            style={{
              fontSize: 14,
              color: colors.text.black
            }}
          >
            {this.state.textSelect}
          </IText>
        </ITouch>
      </IView>
    );
  };

  render() {
    return this.state.dataSaveCollections ? (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={"Saved Collections"} />
        {this.renderSortBy()}
        <IScrollView
          style={{ marginTop: 15 }}
          onBottom={() => {
            this.page++;
            this.loadMore();
          }}
        >
          <IFlatList
            data={this.state.dataSaveCollections}
            renderItem={({ item, index }) => (
              <RowCollection
                data={item}
                isCheckSave={!this.isHidden}
              ></RowCollection>
            )}
            extraData={this.state.dataSaveCollections}
            keyExtractor={(item, index) => item}
            showsVerticalScrollIndicator={false}
            ref={ref => {
              this._flatList = ref;
            }}
            ListEmptyComponent={() => (
              <IView
                style={{
                  padding: 32,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <IText
                  style={{
                    color: colors.gray._400,
                    fontSize: 16
                  }}
                >
                  No items
                </IText>
              </IView>
            )}
          />
        </IScrollView>

        {this.renderButtonBottom()}
      </IView>
    ) : null;
  }
}

export default SavedCollections;
