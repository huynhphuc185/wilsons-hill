import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  Animated
} from "react-native";
import BaseScreen from "../BaseScreen";
import { IToolbar, IView, IText, ITouch, IFastImage } from "../../components";
import { colors, images } from "../../../assets";
import RowCollection from "../row/RowCollection";
import { constraintLayout } from "../../components/ILayoutProps";
import DynamicFlatlistProduct from "../home/DynamicFlatlistProduct";
import { APIService, NavigationService } from "../../services";
import { ScrollView } from "react-native-gesture-handler";
import IFlatList from "../../components/IFLatList";
import ICombobox from "../../components/ICombobox";
import AlertScreen from "../common/AlertScreen";
import IScrollView from "../../components/IScrollView";
import EventEmitter from "react-native-eventemitter";

class SaveItems extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(2),
      bounceValueTop: new Animated.Value(90),
      dataSaveItems: null,
      textSelect: "Select",
      listSort: ["Room 1", "Room 2", "Room 3", "Room 4"],
      isRefresh: false,
      listChose: [],
      updateButton: true,
      marginBottomView: false
    };
    this.indexSort = 0;
    this.isHidden = true;
    this.page = 1;
  }

  componentDidMount() {
    this.getSaveItems();
    EventEmitter.on("update_catalogue", this.getSaveItems);
    EventEmitter.on("onOffBtn", this.updateButton);
  }
  componentWillUnmount() {
    EventEmitter.removeListener("onOffBtn", this.updateButton);
    EventEmitter.removeListener("update_catalogue", this.getSaveItems);
  }

  updateButton = () => {
    const result = this.state.dataSaveItems.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    if (result.length != 0) {
      this.setState({
        updateButton: false
      });
    } else {
      this.setState({
        updateButton: true
      });
    }
  };

  getSaveItems = () => {
    APIService.getSaveItems(this.page).then(data => {
      if (data != null) {
        data.product.map((item, index) => {
          item.chose = false;
        });

        this.setState({
          dataSaveItems: data.product
        });
        this._flatList.refreshProductList(data.product);
      }
    });
  };
  addManyToCart = () => {
    var listChose = this.filterChoseAddToCart();
    APIService.addManyToCart(listChose).then(data => {
      if (data != null) {
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Add products successfully!", 9999, () => {
            this._toggleSubview();
            NavigationService.pop();
          })
        );
        EventEmitter.emit("setNumberCart");
        EventEmitter.emit("getCart");
      }
    });
  };

  filterChoseAddToCart() {
    const result = this.state.dataSaveItems.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    var listChose = [];
    result.map((item, index) => {
      console.log(item);

      listChose.push({
        product_id: item.id,
        number: item.min_quantity
      });
    });
    return listChose;
  }

  loadMore() {
    APIService.getSaveItems(this.page).then(data => {
      if (data != null) {
        data.product.map((item, index) => {
          item.chose = false;
        });

        this.state.dataSaveItems = this.state.dataSaveItems.concat(
          data.product
        );
        this.setState({
          dataSaveItems: this.state.dataSaveItems
        });

        this._flatList.refreshProductList(this.state.dataSaveItems);
      }
    });
  }

  filterChose() {
    const result = this.state.dataSaveItems.filter(item => {
      if (item.chose) {
        return item;
      }
    });
    var listChose = [];
    result.map((item, index) => {
      listChose.push(item.id);
    });
    // this.setState({ listChose: listChose });
    return listChose;
  }

  unsaveItem = async () => {
    var listChose = this.filterChose();
    const saveCollection = await APIService.productLike(listChose);
    if (saveCollection.fail_list.length === 0) {
      NavigationService.navigate(
        "alert",
        AlertScreen.newInstance(
          "Remove selected items from saved successfully!",
          9999,
          () => {
            this.onEventUpdateWhenLikeProduct(listChose);
            this._toggleSubview();
            NavigationService.pop();
          }
        )
      );
    }
  };

  renderButtonBottom() {
    return (
      <Animated.View
        style={{
          height: this.state.bounceValue,
          backgroundColor: "white",
          borderColor: "#909090",
          shadowOffset: { width: 0, height: -1 },
          shadowOpacity: 0.5,
          elevation: 20
        }}
      >
        <ITouch
          style={{
            height: 40,
            width: 343,
            backgroundColor: "black",
            marginLeft: 18,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 18
          }}
          disabled={this.state.updateButton}
          onPress={() => {
            this.addManyToCart();
          }}
        >
          <IText
            style={{
              fontSize: 16,
              fontFamily: "normal",
              letterSpacing: 1.6,
              color: "white"
            }}
          >
            ADD SELECTED ITEMS TO CART
          </IText>
        </ITouch>
        <IView
          style={{
            height: 32,
            alignItems: "center",
            marginTop: 9
          }}
        >
          <ITouch
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "black"
            }}
            disabled={this.state.updateButton}
            onPress={() => {
              this.unsaveItem();
            }}
          >
            <IText style={{ fontSize: 12 }}>
              Remove selected items from saved
            </IText>
          </ITouch>
        </IView>
      </Animated.View>
    );
  }

  _toggleSubview() {
    this.setState({
      textSelect: !this.isHidden ? "Select" : "Cancel",
      marginBottomView: !this.isHidden ? false : true
    });

    var toValue = 2;
    var toValue2 = 90;

    if (this.isHidden) {
      toValue = 90;
      toValue2 = 2;
    }

    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(this.state.bounceValue, {
      toValue: toValue,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    Animated.spring(this.state.bounceValueTop, {
      toValue: toValue2,
      velocity: 3,
      tension: 1,
      friction: 8
    }).start();

    this.isHidden = !this.isHidden;
    this._flatList.refreshSaveList(!this.isHidden);
  }

  renderSortBy = callBack => {
    return (
      <IView>
        {/* <IText
					style={{ marginBottom: 7, fontSize: 14, marginLeft: 19 }}>
					Sort by
				</IText> */}
        <IView
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            paddingHorizontal: 18,
            //   paddingVertical: 16,
            marginBottom: 10,
            backgroundColor: "#f2f1f1"
          }}
        >
          {/* <IView
						style={{
							flex: 1,
							flexDirection: 'row',
							alignItems: 'center',
						}}>
						<IView
							style={{
								height: 40,
								width: 270,
								borderRadius: 10,
								backgroundColor: colors.white,
							}}>
							<IView
								style={{
									flex: 1,
									justifyContent: 'center',
									alignItems: 'center',
								}}>
								<ICombobox
									widthItem={constraintLayout(270)}
									defaultValue={
										this.state.listSort[this.indexSort]
									}
									// listValue={this.getDataStringCombobox(this.state.listSort)}
									listValue={this.state.listSort}
									callBack={index => {
										this.indexSort = index
										//   this._getListProduct();
									}}
								/>
							</IView>
						</IView>
					</IView> */}
          <ITouch
            style={{ alignItems: "center" }}
            onPress={() => {
              this._toggleSubview();
              // this.setState({ isRefresh: true });
            }}
          >
            <IText
              style={{
                fontSize: 14,
                color: colors.text.black
              }}
            >
              {this.state.textSelect}
            </IText>
          </ITouch>
        </IView>
      </IView>
    );
  };

  render() {
    return (
      <IView style={{ flex: 1, backgroundColor: colors.bg }}>
        <IToolbar title={"Saved Items"} />
        <IScrollView
          style={{ backgroundColor: colors.bg }}
          onBottom={() => {
            this.page++;
            this.loadMore();
          }}
        >
          {this.renderSortBy()}

          <IView style={{ flex: 1 }}>
            <DynamicFlatlistProduct
              listProduct={this.state.dataSaveItems}
              isCheckSave={!this.isHidden}
              noLike={true}
              ref={ref => {
                this._flatList = ref;
              }}
            ></DynamicFlatlistProduct>
            {this.state.marginBottomView ? (
              <IView style={{ height: 90 }}></IView>
            ) : null}
          </IView>
        </IScrollView>
        {this.renderButtonBottom()}
      </IView>
    );
  }
}

export default SaveItems;
