import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  Image
} from "react-native";
import {
  IImage,
  IToolbar,
  IView,
  IFastImage,
  IText,
  IFLatList,
  ITouch
} from "../../components";
import BaseScreen from "../BaseScreen";
import { images } from "../../../assets";
import { APIService, NavigationService, UserService } from "../../services";
import AlertScreen from "../common/AlertScreen";
class SplashLoginScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      dataProfile: {}
    };
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Image
          resizeMode="contain"
          style={{ flex: 1 }}
          source={images.t_splash_login}
        ></Image>
        <IView
          style={{
            // padding: 60,
            width: "100%",
            top: "62%",
            right: "11%",
            left: "4%",
            position: "absolute",
            paddingRight: 60
          }}
        >
          <IText
            style={{
              fontSize: 30,
              fontWeight: "500",
              letterSpacing: 0.8,
              lineHeight: 40,
              color: "white"
            }}
          >
            Exclusive access to the best deals & designs daily
          </IText>
        </IView>
        <IView
          style={{
            // padding: 60,
            width: "100%",
            top: "84%",
            right: "4%",
            left: "4%",
            position: "absolute",
            paddingRight: 60,
            flexDirection: "row"
          }}
        >
          <ITouch
            style={{
              height: 40,
              width: 204,
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center"
            }}
            onPress={() => NavigationService.push("oauthStack")}
          >
            <IText
              style={{
                fontSize: 16,
                letterSpacing: 1.6,
                lineHeight: 19
              }}
            >
              LOG IN
            </IText>
          </ITouch>
          <ITouch
            style={{
              height: 40,
              width: 123,
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              marginLeft: 16
            }}
            onPress={async () => {
              await UserService.reset();

              NavigationService.reset("mainStack");
            }}
          >
            <IText
              style={{
                fontSize: 16,
                letterSpacing: 1.6,
                lineHeight: 19
              }}
            >
              PREVIEW
            </IText>
          </ITouch>
        </IView>
      </View>
    );
  }
}

export default SplashLoginScreen;
