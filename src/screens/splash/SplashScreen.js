import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Platform,
  Image,
  Animated,
  Linking,
  BackHandler
} from "react-native";
import { CONFIG, VERSION, KeyStorage } from "constant";
import SplashScreen from "react-native-splash-screen";
// import APIServices from "services/APIService";
import { NavigationActions, StackActions } from "react-navigation";
//import firebase from 'react-native-firebase'
import { APIService, UserService, NavigationService } from "../../services";
import { colors, images, strings } from "../../../assets";
import { IView, IFastImage, IText } from "../../components";
import ConfigServer from "../../utils/ConfigServer";
import EnvService from "../../utils/EnvService";
import AlertScreen from "../common/AlertScreen";
// import UserService from "../services/UserService";

const envService = new EnvService();
class SplashComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opacity: new Animated.Value(0)
    };
    this.checkForceUpdate();
  }
  componentWillMount() {}
  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide();
    }, 3000);
  }
  render() {
    return (
      <IView
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Image
          resizeMode="contain"
          style={{ flex: 1 }}
          source={images.p_splash}
        ></Image>

        <IView
          style={{
            padding: 60,
            width: "100%",
            position: "absolute"
          }}
        >
          <ImageLoader
            resizeMode="contain"
            style={{ width: "100%" }}
            source={images.ic_splash}
          />
        </IView>

        <IView
          style={{
            top: "84.32%",
            right: "11.74%",
            left: "4.21%",
            bottom: "2.66%",
            position: "absolute"
          }}
        >
          <IText
            style={{
              fontSize: 20,
              fontWeight: "500",
              letterSpacing: 0.8,
              lineHeight: 30,
              color: "white"
            }}
          >
            Exclusive access to the best deals & designs daily
          </IText>
        </IView>
      </IView>
    );
  }
  async configBaseURL(config) {
    const currentEnv = await envService.getCurrentEnv();
    switch (currentEnv) {
      case EnvService.ENV.REAL:
        APIService.setBaseURL(config.restful_ip);
        break;
      case EnvService.ENV.DEV:
        APIService.setBaseURL(config.restful_ip_dev);
        break;
      case EnvService.ENV.TEST:
        APIService.setBaseURL(config.restful_ip_test);
        break;
      default:
        APIService.setBaseURL(config.restful_ip);
        break;
    }
  }
  async checkForceUpdate() {
    try {
      const response = await fetch(CONFIG);
      const json = await response.json();
      var configs = json.config;

      const config = this.getConfigWithVersion(VERSION, configs);
      ConfigServer.setConfig(config);
      // Change baseURL base on current version
      await this.configBaseURL(config);
      // Force update check
      const shouldMaintain = this.handleMaintain(config);
      if (!shouldMaintain) {
        this.handleUpdate(config);
      }
    } catch (err) {
      console.log(err);
    }
  }
  getConfigWithVersion(currentVersion, forceUpdateConfig) {
    var result = null;
    forceUpdateConfig.forEach(_config => {
      if (_config.version === currentVersion) result = _config;
    });
    return result;
  }
  openLink(link) {
    Linking.canOpenURL(link).then(
      supported => {
        supported && Linking.openURL(link);
      },
      err => console.log(err)
    );
  }
  moveToAppOnStore(config) {
    const platform = Platform.OS;
    let link;
    if (platform === "ios") {
      link = config.ios_url;
    } else {
      link = config.android_url;
    }

    this.openLink(link);
  }

  handleMaintain(config) {
    const { maintenance_status } = config;
    switch (maintenance_status) {
      case 0:
        return false;
        break;
      case 1:
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Server maintenance", 555)
        );
        return true;
        break;
      default:
        break;
    }
  }
  handleUpdate(config) {
    const { update_status } = config;
    switch (update_status) {
      case 0:
        this.goHomePage();
        break;
      case 1:
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Force Update", 666, () => {
            this.moveToAppOnStore();
          })
        );
        break;
      case 2:
        NavigationService.navigate(
          "alert",
          AlertScreen.newInstance("Force Update", 777, () => {
            this.moveToAppOnStore();
          })
        );
        break;
      default:
        break;
    }
  }
  goHomePage = async () => {
    let isLogin = await UserService.isLogin();
    if (isLogin) {
      setTimeout(() => {
        NavigationService.reset("mainStack");
      }, 3000);
    } else {
      const data = await APIService.getTokenDefaut();
      await UserService.save(data.token);
      setTimeout(() => {
        NavigationService.reset("mainStack");
      }, 3000);
    }
  };
}

export default SplashComponent;

class ImageLoader extends Component {
  state = {
    opacity: new Animated.Value(0.1)
  };

  onLoad = () => {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 2500,
      useNativeDriver: true
    }).start();
  };

  render() {
    return (
      <Animated.Image
        onLoad={this.onLoad}
        {...this.props}
        style={[
          {
            opacity: this.state.opacity
          },
          this.props.style
        ]}
      />
    );
  }
}
