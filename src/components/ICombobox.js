import React, { Component } from 'react'
import { ActivityIndicator, View, StyleSheet, Platform } from 'react-native'
import { colors, images } from '../../assets'
import ModalDropdown from './ModalDropdown'
import { ITouch, IFastImage } from '.'
import { constraintLayout } from '../components/ILayoutProps'
class ICombobox extends Component {
	render() {
		let {
			widthItem,
			defaultValue,
			listValue,
			disabled = false,
			callBack,
		} = this.props
		return (
			<View
				style={{
					flexDirection: 'row',
					width: '100%',
					alignItems: 'center',
					height: '100%',
				}}>
				<ModalDropdown
					disabled={disabled}
					adjustFrame={style => {
						style.top =
							Platform.OS == 'ios' ? style.top : style.top - 29
						return style
					}}
					style={{ flex: 1 }}
					dropdownStyle={{ width: widthItem }}
					textStyle={{
						paddingLeft: 11,
						paddingRight: 25,
						fontSize: constraintLayout(14),
						fontFamily: 'Raleway',
						color: '#171717',
						fontWeight: '500',
					}}
					dropdownTextStyle={{
						fontSize: constraintLayout(14),
						fontFamily: 'Raleway',
						color: '#171717',
					}}
					defaultIndex={0}
					defaultValue={defaultValue}
					options={listValue}
					onSelect={index => {
						callBack(index)
					}}
				/>

				<View
					pointerEvents='none'
					style={{
						width: 19,
						height: 16.3,
						justifyContent: 'center',
						alignItems: 'center',
						right: 8,
						position: 'absolute',
					}}>
					<IFastImage
						style={{ height: 10, width: 10 }}
						source={images.ic_combobox}
						resizeMode='contain'
					/>
				</View>
			</View>
		)
	}
}

export default ICombobox
