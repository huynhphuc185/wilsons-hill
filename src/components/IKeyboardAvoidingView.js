import React, { Component } from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { IView } from ".";

class IKeyboardAvoidingView extends Component {
  render() {
    return (
      <IView {...this.props}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          <TouchableWithoutFeedback
            style={{ flex: 1 }}
            onPress={Keyboard.dismiss}
          >
            {this.props.children}
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </IView>
    );
  }
}

export default IKeyboardAvoidingView;
