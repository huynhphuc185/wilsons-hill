import React from "react";
import {
  check,
  request,
  openSettings,
  PERMISSIONS,
  RESULTS
} from "react-native-permissions";
import { Alert, Platform } from "react-native";
import ActionSheet from "react-native-actionsheet";
import ImagePicker from "react-native-image-crop-picker";

const imagePickerOptions = {
  title: "Chọn nguồn hình",
  takePhotoButtonTitle: "Chụp ảnh mới",
  chooseFromLibraryButtonTitle: "Chọn từ album ảnh",
  cancelButtonTitle: "Hủy bỏ",
  quality: 0.01,
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

const ImageSources = {
  camera: "Chụp ảnh mới",
  photo: "Chọn từ album ảnh"
};

const IOS_PERMISSIONS = PERMISSIONS.IOS;
const ANDROID_PERMISSIONS = PERMISSIONS.ANDROID;

class PhotoService {
  constructor() {
    this.platform = Platform.OS;
  }

  async getPermissions() {
    if (this.platform == "ios") {
      const camera = await check(IOS_PERMISSIONS.CAMERA);
      const photo = await check(IOS_PERMISSIONS.PHOTO_LIBRARY);

      return {
        photo,
        camera
      };
    } else {
      const photo = await check(ANDROID_PERMISSIONS.READ_EXTERNAL_STORAGE);
      const camera = await check(ANDROID_PERMISSIONS.CAMERA);
      return {
        photo,
        camera
      };
    }
  }

  // Photo
  async requestPhotoPermission() {
    if (this.platform == "ios") {
      let response = await request(IOS_PERMISSIONS.PHOTO_LIBRARY);
      this.photoPermission = response;

      if (this.photoPermission == RESULTS.GRANTED) {
        ImagePicker.openPicker({
          multiple: this.multichoice,
          compressImageQuality: 0.3,
          includeBase64: true,
          maxFiles: 10
        }).then(images => {
          console.log(images);

          // Luôn trả về mảng
          if (this.multichoice) {
            this.callback(images);
          } else {
            this.callback([images]);
          }
        });
      }
    } else {
      let response = await request(ANDROID_PERMISSIONS.READ_EXTERNAL_STORAGE);
      this.photoPermission = response;

      if (this.photoPermission == RESULTS.GRANTED) {
        ImagePicker.openPicker({
          multiple: this.multichoice,
          compressImageQuality: 0.3,
          includeBase64: true,
          maxFiles: 10
        }).then(images => {
          console.log(images);

          // Luôn trả về mảng
          if (this.multichoice) {
            this.callback(images);
          } else {
            this.callback([images]);
          }
        });
      }
    }
  }

  alertForPhotosPermission() {
    Alert.alert(
      "Vui lòng cấp quyền truy cập album",
      "Vinh Hoàng Business cần quyền truy cập album để có thể update hình ảnh cho bạn",
      [
        {
          text: "Để sau",
          onPress: () => console.log("Permission denied"),
          style: "cancel"
        },
        this.photoPermission == RESULTS.DENIED
          ? {
              text: "OK",
              onPress: this.requestPhotoPermission.bind(this)
            }
          : {
              text: "Mở quyền truy cập",
              onPress: openSettings
            }
      ]
    );
  }

  // Camera
  async requestCameraPermission() {
    if (this.platform == "ios") {
      let response = await request(IOS_PERMISSIONS.CAMERA);
      this.cameraPermission = response;

      if (this.cameraPermission == RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          // cropping: true,
          compressImageQuality: 0.3,
          includeBase64: true
        }).then(image => {
          console.log(image);
          this.callback([image]);
        });
      }
    } else {
      let response = await request(ANDROID_PERMISSIONS.CAMERA);
      this.cameraPermission = response;

      if (this.cameraPermission == RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          // cropping: true,
          compressImageQuality: 0.3,
          includeBase64: true
        }).then(image => {
          console.log(image);
          this.callback([image]);
        });
      }
    }
  }

  alertForCameraPermission() {
    Alert.alert(
      "Vui lòng cấp quyền truy cập camera",
      "Vinh Hoàng Business cần quyền truy cập camera để có thể update hình ảnh cho bạn",
      [
        {
          text: "Để sau",
          onPress: () => console.log("Permission denied"),
          style: "cancel"
        },
        this.cameraPermission == RESULTS.DENIED
          ? {
              text: "OK",
              onPress: this.requestCameraPermission.bind(this)
            }
          : {
              text: "Mở quyền truy cập",
              onPress: openSettings
            }
      ]
    );
  }

  async showImagePicker(callback) {
    this.ActionSheet.show();
    this.callback = callback;
  }

  async handleActionSheetChoose(index) {
    // Lấy quyền
    let permissions = await this.getPermissions();
    this.photoPermission = permissions.photo;
    this.cameraPermission = permissions.camera;

    // User chọn photo hay camera
    let imageSource = this.imageSources[index];

    if (imageSource == ImageSources.photo) {
      if (this.photoPermission != RESULTS.GRANTED) {
        this.alertForPhotosPermission(); // Show popup xin quyền và thoát
        return;
      }

      ImagePicker.openPicker({
        multiple: this.multichoice,
        compressImageQuality: 0.3,
        includeBase64: true,
        maxFiles: 10
      })
        .then(images => {
          console.log(images);

          // Luôn trả về mảng
          if (this.multichoice) {
            this.callback(images);
          } else {
            this.callback([images]);
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else if (imageSource == ImageSources.camera) {
      if (this.cameraPermission != RESULTS.GRANTED) {
        this.alertForCameraPermission(); // Show popup xin quyền và thoát
        return;
      }

      ImagePicker.openCamera({
        width: 300,
        height: 400,
        compressImageQuality: 0.3,
        // cropping: true,
        includeBase64: true
      })
        .then(image => {
          console.log(image);
          this.callback([image]);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  renderImagePicker({ photo = false, camera = false, multichoice = false }) {
    var imageSources = [];
    if (camera) {
      imageSources.push(ImageSources.camera);
    }
    if (photo) {
      imageSources.push(ImageSources.photo);
    }

    imageSources.push("Hủy");

    this.imageSources = imageSources;
    this.multichoice = multichoice;

    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        title={"Chọn nguồn hình"}
        options={imageSources}
        cancelButtonIndex={this.imageSources.length - 1}
        onPress={index => this.handleActionSheetChoose(index)}
      />
    );
  }
}

export default PhotoService;
