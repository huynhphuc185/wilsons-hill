import React, { Component } from "react";
import { Text, View, FlatList } from "react-native";
import { IView, IText } from ".";
import { colors } from "../../assets";

class IFlatList extends Component {
  constructor(props) {
    super(props);
    this.isLoading = false;
  }
  render() {
    return (
      <FlatList
        {...this.props}
        ref={ref => {
          this.flatList = ref;
        }}
        onEndReachedThreshold={0.5}
        onEndReached={this._onEndReached}
        ListEmptyComponent={() => {
          if (this.props.refreshing) {
            return null;
          }
          if (this.props.ListEmptyComponent) {
            return this.props.ListEmptyComponent();
          }
          return (
            <IView
              style={{
                padding: 32,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              {/* <IText style={{ color: colors.gray._400, fontSize: 16 }}>
                No items
              </IText> */}
            </IView>
          );
        }}
      />
    );
  }
  _scrollToIndex = index => {
    this.flatList.scrollToIndex({
      animated: true,
      index: index
    });
  };
  _onEndReached = async () => {
    try {
      if (!this.props.onEndReached) {
        return;
      }
      if (this.isLoading) {
        return;
      }
      this.isLoading = true;
      await this.props.onEndReached();
      this.isLoading = false;
    } catch (error) {}
  };
}

export default IFlatList;
