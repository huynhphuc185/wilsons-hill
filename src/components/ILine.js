import React, { Component } from "react";
import { Text, View } from "react-native";
import { IView } from ".";

class ILine extends Component {
  render() {
    const {
      width = "100%",
      height = "100%",
      color = "transparent"
    } = this.props;
    return (
      <IView
        style={{
          ...this.props.style,
          width: width,
          height: height,
          backgroundColor: color
        }}
      ></IView>
    );
  }
}

export default ILine;
