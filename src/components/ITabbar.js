import React, { Component } from "react";
import { Text, View } from "react-native";
import { createAppContainer } from "react-navigation";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import { colors } from "../../assets";
import { constraintLayout } from "./ILayoutProps";
import { IView } from ".";

class ITabbar extends Component {
  render() {
    const { tabs, scrollEnabled = false } = this.props;
    const Tab = createAppContainer(
      createMaterialTopTabNavigator(tabs, {
        lazy: true,

        tabBarOptions: {
          showLabel: true,
          showIcon: false,
          borderTopWidth: 0,
          scrollEnabled: scrollEnabled,
          zIndex: 1,
          inactiveTintColor: "black",
          activeTintColor: "black",
          indicatorStyle: {
            backgroundColor: "black",
            width: constraintLayout(40),
            marginLeft: constraintLayout((380 / 3 - 40) / 2)
          },

          upperCaseLabel: false,
          labelStyle: {
            fontSize: constraintLayout(16),
            fontFamily: "Raleway-Regular",
            width: constraintLayout(105),
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          },
          // activeBackgroundColor: colors.mainDark,
          style: {
            height: constraintLayout(44),
            borderTopColor: "transparent",
            backgroundColor: "white",
            // shadowColor: "#000",
            // shadowOffset: {
            //   width: 0,
            //   height: 4
            // },
            // shadowOpacity: 0.3,
            // shadowRadius: 4.65,
            // elevation: 8,
            zIndex: 1
          }
        }
      })
    );
    return <Tab />;
  }
}

export default ITabbar;
