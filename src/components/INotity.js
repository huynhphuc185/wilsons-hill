import React, { Component } from "react";
import { Text, View } from "react-native";
import { ITouch, IFastImage } from ".";
import { images, colors, strings } from "../../assets";
import IView from "./IView";
import IText from "./IText";
import { NavigationService } from "../services";
import AlertScreen from "../screens/common/AlertScreen";
SIZE = 48;
class INotity extends Component {
  render() {
    return (
      <ITouch
        style={{
          width: SIZE,
          height: SIZE,
          alignItems: "center",
          justifyContent: "center"
        }}
        onPress={() => {
         NavigationService.navigate('notify');
        }}
      >
        <IFastImage
          source={images.icNotify}
          style={{ width: 18, height: 19 }}
          resizeMode="contain"
        />

        {/* <IView
          style={{
            backgroundColor: colors.main,
            paddingHorizontal: 4,
            paddingVertical: 2,
            position: "absolute",
            borderRadius: 4,
            left: "50%",
            bottom: "50%",
            minWidth: 16,
            alignItems: "center",
            justifyContent: "center",
            elevation: 4
          }}
        >
          <IText
            style={{ color: colors.white, fontSize: 10, fontFamily: "bold" }}
          >
            12
          </IText>
        </IView> */}
      </ITouch>
    );
  }
}

export default INotity;
