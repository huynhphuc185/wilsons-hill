import React from 'react'
import { Dimensions, Platform, PixelRatio } from 'react-native'
import { Text } from 'react-native'
import ILayoutProps from './ILayoutProps'

const IText = props => {
	var typeFont = 'Raleway-'
	var fontFamily = typeFont + 'Regular'
	var color = '#1C1C1C'
	if (props.isSo) {
		typeFont = 'SourceSansPro-'
		fontFamily = typeFont + 'Regular'
	}
	if(props.isItalic){
		fontFamily = typeFont + 'Italic'
	}
	if (typeof props.style !== 'undefined') {
		if (props.style.color) {
			color = props.style.color
		}

		if (props.style.fontWeight) {
			switch (props.style.fontWeight) {
				case '300':
					fontFamily = typeFont + 'Regular'
					break
				case '500':
					fontFamily = typeFont + 'Semibold'
					break
				case '900':
					fontFamily = typeFont + 'Bold'
					break
				default:
					fontFamily = typeFont + 'Regular'
					break
			}
		}
	}

	return (
		<Text
			{...props}
			style={[
				ILayoutProps(props.style),
				{ fontFamily: fontFamily, color: color },
			]}>
			{props.children}
		</Text>
	)
}

export default IText
