import React, { Component } from 'react'
import { Text, View, StatusBar, Animated, StyleSheet } from 'react-native'
import { IView, ICart, ITouch, IFastImage } from '.'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { colors, images } from '../../assets'
import IText from './IText'
import { NavigationService, APIService } from '../services'
import { currentTabSelect } from '../constants'
import Global from '../constants/Global'
import EventEmitter from 'react-native-eventemitter'
HEIGHT = 56
export class IToolbar extends Component {
	constructor(props) {
		super(props)
		this.state = {
			number: 0,
		}
	}

	setNumberCart = async () => {
		const data = await APIService.getCartNumber()
		this.setState({ number: data.number })
	}
	componentDidMount() {
		if (this.props.isCart) {
			this.setNumberCart()
			EventEmitter.on('setNumberCart', this.setNumberCart)
		}
	}
	componentWillUnmount() {
		if (this.props.isCart) {
			EventEmitter.removeListener('setNumberCart', this.setNumberCart)
		}
	}

	render() {
		const {
			title = '',
			isCart = false,
			zIndex = 5,
			notBack = false,
			notSearch = false,
			isSafeArea = false,
			isBackFromDetailScreen = false,
			editProfile = false,
			saveProfile = false,
			callBack,
			animatedStyle,
		} = this.props
		// shadow = {
		// 	shadowOffset: { width: 0, height: 1 },
		// 	shadowOpacity: 0.4,
		// 	elevation: 2,
		// 	width: '100%',
		// 	height: '100%',
		// 	position: 'absolute',
		// 	alignItems: 'center',
		// 	justifyContent: 'center',
		// 	backgroundColor: colors.bg,
		// 	opacity: animatedStyle,
		// }
		// nonshadow = {
		// 	width: '100%',
		// 	height: '100%',
		// 	position: 'absolute',
		// 	alignItems: 'center',
		// 	justifyContent: 'center',
		// 	backgroundColor: colors.bg,
		// 	opacity: animatedStyle,
		// }

		return (
			<IView style={{ zIndex: zIndex }}>
				{Platform.OS === 'ios' && isSafeArea === true ? null : (
					<IView style={{}}>
						<StatusBar
							translucent
							barStyle='dark-content'
							animated
						/>
						<Animated.View
							style={{
								height: getStatusBarHeight(),
								opacity: animatedStyle,
								backgroundColor: colors.bg,
							}}
						/>
					</IView>
				)}
				<View
					style={{
						height: HEIGHT,
						overflow: 'hidden',
						// paddingBottom: 5,
					}}>
					<Animated.View
						style={{
							width: '100%',
							height: '100%',
							position: 'absolute',
							alignItems: 'center',
							justifyContent: 'center',
							backgroundColor: colors.bg,
							opacity: animatedStyle,
							// shadowOffset: { width: 0, height: 1 },
							// shadowOpacity: 0.4,
							// elevation: 2,
						}}>
						<IText
							style={{
								fontSize: 16,
								color: colors.text.black,
								fontWeight: '500',
								paddingHorizontal: 50,
							}}
							numberOfLines={1}>
							{title}
						</IText>
					</Animated.View>

					<Animated.View
						style={{
							width: '100%',
							height: '100%',
							position: 'absolute',
							alignItems: 'center',
							justifyContent: 'center',
							flexDirection: 'row',
							elevation: 2,
						}}>
						{notBack ? null : (
							<ITouch
								style={{
									width: HEIGHT,
									height: HEIGHT,
									alignItems: 'center',
									justifyContent: 'center',
								}}
								onPress={() => {
									if (title === 'My Cart') {
										if (isBackFromDetailScreen === false) {
											NavigationService.navigate(
												Global.previousRoute,
											)
										} else {
											NavigationService.pop()
										}
									} else {
										NavigationService.pop()
									}
								}}>
								<IFastImage
									style={{ width: 20, height: 16 }}
									resizeMode='contain'
									source={images.ic_back}
								/>
							</ITouch>
						)}

						<IView style={{ flex: 1 }}></IView>
						{editProfile ? (
							<ITouch
								style={{
									marginRight: 20,
									// borderBottomWidth: 1,
									borderBottomColor: 'black',
								}}
								onPress={() =>
									NavigationService.navigate('EditAccount')
								}>
								<IText
									style={{
										fontSize: 12,
									}}>
									Edit
								</IText>
							</ITouch>
						) : null}
						{saveProfile ? (
							<ITouch
								style={{
									marginRight: 20,
									// borderBottomWidth: 1,
									borderBottomColor: 'black',
								}}
								onPress={() => callBack()}>
								<IText
									style={{
										fontSize: 12,
									}}>
									Save
								</IText>
							</ITouch>
						) : null}
						{notSearch ? null : isCart ? (
							<ITouch
								style={{
									width: HEIGHT,
									height: HEIGHT,

									alignItems: 'center',
									justifyContent: 'center',
								}}
								onPress={() => {
									NavigationService.push('CartScreen', {
										isBack: true,
									})
								}}>
								<IView>
									<IFastImage
										resizeMode='contain'
										source={images.cartblack}
										style={{ width: 25, height: 25 }}
									/>
									{this.state.number === 0 ? null : (
										<IView
											style={{
												position: 'absolute',
												backgroundColor: 'red',
												width: 16,
												height: 16,
												borderRadius: 16 / 2,
												top: 0,
												right: -8,
												borderColor: colors.white,
												alignItems: 'center',
												justifyContent: 'center',
											}}>
											<IText
												style={{
													color: colors.white,
													fontSize: 9,
													fontWeight: '900',
												}}>
												{this.state.number}
											</IText>
										</IView>
									)}
								</IView>
							</ITouch>
						) : (
							<ITouch
								style={{
									width: HEIGHT,
									height: HEIGHT,

									alignItems: 'center',
									justifyContent: 'center',
								}}
								onPress={() => {
									NavigationService.navigate('SearchScreen')
								}}>
								<IFastImage
									style={{ width: 21, height: 21 }}
									resizeMode='contain'
									source={images.ic_search}
								/>
							</ITouch>
						)}
					</Animated.View>
				</View>
			</IView>
		)
	}
}

export default IToolbar
