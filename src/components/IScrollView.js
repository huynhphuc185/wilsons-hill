import React, { Component } from 'react'
import { Text, View, FlatList, Animated } from 'react-native'
import { IView, IText } from '.'
import { colors } from '../../assets'
import { ScrollView } from 'react-native-gesture-handler'
class IScrollView extends Component {
	constructor(props) {
		super(props)
		this.oneTimeLoadMore = true
		this.maxContentSize = 0.0
	}
	resetMaxContent = () => {
		this.maxContentSize = 0.0
	}
	_isBottom({ layoutMeasurement, contentOffset, contentSize }) {
		// if (this.maxContentSize < contentSize.height) {
		// 	var isLoadmore =
		// 		layoutMeasurement.height + contentOffset.y >=
		// 		contentSize.height - 1
		// 	if (isLoadmore) {
		// 		this.maxContentSize = contentSize.height
		// 	}
		// 	return isLoadmore
		// } else {
		// 	return false
		// }
		return (
			layoutMeasurement.height + contentOffset.y >= contentSize.height - 1
		)
	}

	_isTop({ layoutMeasurement, contentOffset, contentSize }) {
		return contentOffset.y == 0
	}
	render() {
		return (
			<Animated.ScrollView
				{...this.props}
				scrollEventThrottle={400}
				onScroll={({ nativeEvent }) => {
					if (this.props.onScroll) {
						this.props.onScroll({ nativeEvent })
					}

					if (this._isTop(nativeEvent)) {
						if (this.props.onTop) {
							this.props.onTop()
						}
					}
					if (this._isBottom(nativeEvent)) {
						{
							if (this.props.onBottom) {
								clearTimeout(this.timer)
								if (this.oneTimeLoadMore) {
									this.oneTimeLoadMore = false
									this.props.onBottom()
								}
								this.timer = setTimeout(() => {
									this.oneTimeLoadMore = true
								}, 500)
							}
						}
					}
				}}
			/>
		)
	}
}

export default IScrollView
