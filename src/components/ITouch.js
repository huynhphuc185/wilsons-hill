import React from "react";
import { TouchableOpacity, View } from "react-native";
import debounce from "lodash.debounce";
import ILayoutProps, { createShaddowFromElevation } from "./ILayoutProps";

const withPreventDoubleClick = WrappedComponent => {
  class PreventDoubleClick extends React.PureComponent {
    debouncedOnPress = () => {
      this.props.onPress && this.props.onPress();
    };

    onPress = debounce(this.debouncedOnPress, 300, {
      leading: true,
      trailing: false
    });

    render() {
      return <WrappedComponent {...this.props} onPress={this.onPress} />;
    }
  }

  PreventDoubleClick.displayName = `withPreventDoubleClick(${WrappedComponent.displayName ||
    WrappedComponent.name})`;
  return PreventDoubleClick;
};
const Touch = props => {
  if (props.disabled) {
    props.style.opacity = 0.3;
  }
  var newStyle = {
    ...props.style
  };

  if (typeof props.style === "object") {
    const elevation = props.style.elevation;

    if (typeof elevation === "number") {
      newStyle = createShaddowFromElevation(props.style);
    }
  }
  return (
    <TouchableOpacity
      {...props}
      style={[ILayoutProps(newStyle)]}
      activeOpacity={props.activeOpacity ? props.activeOpacity : 0.5}
      //onLongPress={() => { }}
      hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
    >
      {props.children}
    </TouchableOpacity>
  );
};
const ITouch = withPreventDoubleClick(Touch);
export default ITouch;
