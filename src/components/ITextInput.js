import React from 'react'
import { Dimensions, Platform, PixelRatio } from 'react-native'
import { TextInput } from 'react-native'
import ILayoutProps from './ILayoutProps'

const ITextInput = props => {
	var typeFont = 'Raleway-'
	var fontFamily = typeFont + 'Regular'
	var color = '#1C1C1C'
	if (typeof props.style !== 'undefined') {
		if (props.style.color) {
			color = props.style.color
		}

		if (props.isSo) {
			typeFont = 'SourceSansPro-'
			fontFamily = typeFont + 'Regular'
		}
		if (props.style.fontFamily) {
			switch (props.style.fontFamily) {
				case 'normal':
					fontFamily = typeFont + 'Regular'
					break
				case 'medium':
					fontFamily = typeFont + 'Medium'
					break
				case 'bold':
					fontFamily = typeFont + 'Bold'
					break
				default:
					fontFamily = typeFont + 'Regular'
					break
			}
		}
	}

	return (
		<TextInput
			{...props}
			style={[
				ILayoutProps(props.style),
				{ fontFamily: fontFamily, color: color },
			]}>
			{props.children}
		</TextInput>
	)
}

export default ITextInput
