import React, { Component } from "react";
import { Text, View } from "react-native";
import { ITouch, IFastImage } from ".";
import { images, colors } from "../../assets";
import IView from "./IView";
import IText from "./IText";
import { APIService, DataService, NavigationService } from "../services";
import EventEmitter from "react-native-eventemitter";
import { Event } from "../constants";
import AlertScreen from "../screens/common/AlertScreen";
SIZE = 48;
class ICart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0
    };
  }
  render() {
    return (
      <ITouch
        style={{
          width: SIZE,
          height: SIZE,
          alignItems: "center",
          justifyContent: "center"
        }}
        onPress={() => {
          if (!this.state.number) {
            NavigationService.navigate(
              "alert",
              AlertScreen.newInstance(
                "Chưa có hàng trong giỏ. Vui lòng chọn sản phẩm để vào giỏ hàng"
              )
            );
            return;
          }
          NavigationService.navigate("cart");
        }}
      >
        <IFastImage
          source={
            this.props.isDetailProduct ? images.ic_cart_white : images.icCart
          }
          style={{ width: 18, height: 19 }}
          resizeMode="contain"
        />
        {this.state.number ? (
          <IView
            style={{
              backgroundColor: colors.main,
              paddingHorizontal: 4,
              paddingVertical: 2,
              position: "absolute",
              borderRadius: 4,
              left: "50%",
              bottom: "50%",
              minWidth: 16,
              alignItems: "center",
              justifyContent: "center",
              elevation: 4
            }}
          >
            <IText
              style={{ color: colors.white, fontSize: 10, fontFamily: "bold" }}
            >
              {this.state.number}
            </IText>
          </IView>
        ) : null}
      </ITouch>
    );
  }

  eventData = data => {
    this.setState({
      number: data.length ? data.length : 0
    });
  };

  componentDidMount() {
    this._getNumberCart();
    EventEmitter.on(Event.UPDATE_CART, this.eventData);
  }
  componentWillUnmount() {
    EventEmitter.removeListener(Event.UPDATE_CART, this.eventData);
  }

  _getNumberCart = async () => {
    try {
      const data = await DataService._getNumberCart();
      if (data && data.length) {
        this.setState({
          number: data.length
        });
      } else {
        this.setState({
          number: 0
        });
      }
    } catch (error) {}
  };
}

export default ICart;
