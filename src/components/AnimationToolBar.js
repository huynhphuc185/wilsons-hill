import React, { Component } from "react";
import { View, StyleSheet, Image, Alert } from "react-native";
import { IText, ITouch, IFastImage, IView, ICart } from ".";
import EventEmitter from "react-native-eventemitter";
import { Event } from "../constants";
import { APIService, DataService } from "../services";
import { colors, images } from "../../assets";
class AnimationToolBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0
    };
  }
  static defaultProps = {
    title: "",
    navigation: null,
    isCart: true
  };
  _getNumberCart = async () => {
    try {
      const data = await DataService._getNumberCart();
      if (data && data.length) {
        this.setState({
          number: data.length
        });
      } else {
        this.setState({
          number: 0
        });
      }
    } catch (error) {}
  };

  eventData = data => {
    this.setState({
      number: data.length ? data.length : 0
    });
  };

  componentDidMount() {
    this._getNumberCart();
    EventEmitter.on(Event.UPDATE_CART, this.eventData);
  }

  componentWillUnmount() {
    EventEmitter.removeListener(Event.UPDATE_CART, this.eventData);
  }

  render() {
    let alignSelfCartIcon = this.state.number == 0 ? "flex-end" : "center";
    const { onError } = this.props;

    return (
      <View
        style={{
          position: "absolute",
          width: "100%",
          height: "100%",
          justifyContent: "center"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            paddingHorizontal: 14,
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ITouch
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              backgroundColor: "rgba(31, 31, 31, 0.5)",
              alignItems: "center",
              justifyContent: "center"
            }}
            onPress={() => {
              this.props.navigation.pop();
            }}
          >
            <Image
              style={{ flex: 1, width: 10, height: 10 }}
              source={images.ic_back_white}
              resizeMode="contain"
            />
          </ITouch>
          <IText
            style={{
              color: colors.main,
              flex: 1,
              marginLeft: 20,
              marginRight: 20,
              textAlign: "center",
              alignSelf: "center"
            }}
            fontSize={16}
            fontWeight="bold"
          >
            {this.props.title}
          </IText>
          {this.props.isCart ? (
            <IView
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: "rgba(31, 31, 31, 0.5)",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ICart isDetailProduct={true} />
            </IView>
          ) : null}
        </View>
        {/* <View style={{height:1,backgroundColor:colors.Main}}> */}

        {/* </View> */}
      </View>
    );
  }
}
export default AnimationToolBar;
