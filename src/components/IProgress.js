import React, { Component } from 'react'
import { ActivityIndicator, View, StyleSheet, Platform } from 'react-native'
import { IView, IText } from '.'
import { colors, images } from '../../assets'
import FastImage from 'react-native-fast-image'
import {
	BallIndicator,
	BarIndicator,
	DotIndicator,
	MaterialIndicator,
	PacmanIndicator,
	PulseIndicator,
	SkypeIndicator,
	UIActivityIndicator,
	WaveIndicator,
} from 'react-native-indicators'
class IProgress extends Component {
	componentWillMount() {
		console.log(this.constructor.name + ' init')
	}

	componentWillUnmount() {
		console.log(this.constructor.name + ' deinit')
	}
	render() {
		return (
			<View
				style={{
					width: '100%',
					height: '100%',
					position: 'absolute',
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: colors.whiteBlack,
				}}>
				<View
					style={{
						flex: 1,
						backgroundColor: '#00000010',
						width: '100%',
						alignItems: 'center',
						justifyContent: 'center',
					}}>
					<View
						style={{
							width: 100,
							height: 100,
							backgroundColor: colors.white,
							borderRadius: 16,
						}}>
						<BallIndicator color={colors.black} />
					</View>
				</View>
			</View>
		)
	}
}

export default IProgress
