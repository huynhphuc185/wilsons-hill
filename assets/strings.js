import * as vi from "./strings/vi.json";
import * as en from "./strings/en.json";
class strings {
  constructor() {
    if (!strings.instance) {
      strings.instance = this;
    }
    return strings.instance;
  }

  //AsyncStorage next version
  currentLang = "vi";

  get = key => {
    try {
      switch (this.currentLang) {
        case "vi":
          return vi[key];
        case "en":
          return en[key];
        default:
          return vi[key];
      }
    } catch (error) {
      return key;
    }
  };
}

const instance = new strings();
//Object.freeze(instance);
export default instance;
