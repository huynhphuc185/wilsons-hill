class images {
  constructor() {
    if (!images.instance) {
      images.instance = this;
    }
    return images.instance;
  }

  set(images) {
    try {
      this.logo = images.logo;
    } catch (error) {}
  }

  ic_splash = require("assets/images/logo__00071.png");
  bg_splash = require("assets/images/bg_splash.png");
  account_black = require("assets/images/account_black.png");
  account_white = require("assets/images/account_white.png");
  cartblack = require("assets/images/cartblack.png");
  cartwhite = require("assets/images/cartwhite.png");
  catalogue_black = require("assets/images/catalogue_black.png");
  catalogue_white = require("assets/images/catalogue_white.png");
  home_black = require("assets/images/home_black.png");
  home_white = require("assets/images/home_white.png");
  noti_black = require("assets/images/noti_black.png");
  noti_white = require("assets/images/noti_white.png");
  home_logo = require("assets/images/home_logo.png");
  login_logo = require("assets/images/login_logo.png");
  ic_search = require("assets/images/ic_search.png");
  vector = require("assets/images/Vector.png");
  ic_back = require("assets/images/ic_back.png");
  icDropdown = require("assets/images/icDropdown.png");
  collection1 = require("assets/images/itemcheat/collection1.png");
  collection2 = require("assets/images/itemcheat/collection2.png");
  ic_combobox = require("assets/images/ic_combobox.png");
  p_new = require("assets/images/p_new.png");
  p_timden = require("assets/images/p_timden.png");
  p_timtrang = require("assets/images/p_timtrang.png");
  p_cong = require("assets/images/p_cong.png");
  p_tru = require("assets/images/p_tru.png");
  collectionDetail = require("assets/images/itemcheat/collectionDetail.png");
  prd1 = require("assets/images/itemcheat/prd1.png");
  prd2 = require("assets/images/itemcheat/prd2.png");
  prd3 = require("assets/images/itemcheat/prd3.png");
  prd4 = require("assets/images/itemcheat/prd4.png");
  prd5 = require("assets/images/itemcheat/prd5.png");
  prd6 = require("assets/images/itemcheat/prd6.png");
  sell1 = require("assets/images/itemcheat/sell1.png");
  sell2 = require("assets/images/itemcheat/sell2.png");
  sell3 = require("assets/images/itemcheat/sell3.png");
  sell4 = require("assets/images/itemcheat/sell4.png");
  sell5 = require("assets/images/itemcheat/sell5.png");
  sell6 = require("assets/images/itemcheat/sell6.png");
  cartcheat = require("assets/images/itemcheat/cartcheat.png");

  orderhistory = require("assets/images/itemcheat/orderhistory.png");
  iclike = require("assets/images/iclike.png");
  icnotlike = require("assets/images/icnotlike.png");
  ic_noti = require("assets/images/ic_noti.png");
  dot = require("assets/images/dot.png");
  avt = require("assets/images/avtcheat.png");
  ic_location = require("assets/images/ic_location.png");
  ic_phone = require("assets/images/ic_phone.png");
  rankmember = require("assets/images/rankmember.png");
  icmenu_orderhistory = require("assets/images/icmenu_orderhistory.png");
  icmenu_apppreference = require("assets/images/icmenu_apppreference.png");
  icmenu_aboutapp = require("assets/images/icmenu_aboutapp.png");
  icmenu_next = require("assets/images/icmenu_next.png");
  ic_nextwhite = require("assets/images/ic_nextwhite.png");
  banner_login = require("assets/images/banner_login.png");
  p_close = require("assets/images/p_close.png");

  bed_off = require("assets/images/itemcheat/bed_off.png");
  bed_on = require("assets/images/itemcheat/bed_on.png");
  deco_off = require("assets/images/itemcheat/deco_off.png");
  deco_on = require("assets/images/itemcheat/deco_on.png");
  dini_off = require("assets/images/itemcheat/dini_off.png");
  dini_on = require("assets/images/itemcheat/dini_on.png");
  living_off = require("assets/images/itemcheat/living_off.png");
  living_on = require("assets/images/itemcheat/living_on.png");
  ic_delete = require("assets/images/ic_delete.png");
  uncheck_save = require("assets/images/uncheck_save.png");
  check_save = require("assets/images/check_save.png");
  p_down = require("assets/images/p_down.png");
  p_up = require("assets/images/p_up.png");
  medal1 = require("assets/images/medal1.png");
  medal2 = require("assets/images/medal2.png");
  medal3 = require("assets/images/medal3.png");
  icon_logout = require("assets/images/icon_logout.png");

  t_splash_login = require("assets/images/splash_login.png");
  p_splash = require("assets/images/Splash.jpg");
  
}

const instance = new images();
//Object.freeze(instance);
export default instance;
