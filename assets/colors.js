class colors {
  constructor() {
    if (!colors.instance) {
      colors.instance = this;
    }
    return colors.instance;
  }

  set(colors) {
    try {
      this.main = colors.main;
    } catch (error) {}
  }

  main = "#2ebd6a";
  mainDark = "#13a751";
  text = {
    black: "#1C1C1C",
    gray: "#939495",
    white: "#FFFFFF",
    green: "#369E69",
    green1: "#13A751"
  };
  line = {
    black: "#E9F2F4",
    gray: "#CDD0D9",
    red: "#369E69"
  };
  White = "#FFFFFF";
  Transparent = "#00000000";
  white = "#fff";
  black = "#000";
  gray= {
    _50: '#FAFAFA',
    _100: '#F5F5F5',
    _200: '#EEEEEE',
    _300: '#E0E0E0',
    _400: '#BDBDBD',
    _500: '#9E9E9E',
    _600: '#757575',
    _700: '#616161',
    _800: '#424242',
    _900: '#212121',
  }
  green = "#13A751";
  gray_white = "#D1D8D8";
  whiteBlack = "#00000080";
  bg= '#F4F3F3'
}

const instance = new colors();
//Object.freeze(instance);
export default instance;
